var searchData=
[
  ['languagechanged_718',['languageChanged',['../classSettings.html#a130fcb4d32379104628686cc96f127c1',1,'Settings']]],
  ['left_5farea_5fresize_719',['left_area_resize',['../classResizeArea.html#ae2393f05162be0d3db830ff8fcfaf25a',1,'ResizeArea']]],
  ['levenshtein_720',['levenshtein',['../levenshtein_8h.html#a54dfc3d113fd974bff89634280d45172',1,'levenshtein(QString string1, QString string2, int swap_penalty, int substition_penalty, int insertion_penalty, int deletion_penalty):&#160;levenshtein.cpp'],['../levenshtein_8cpp.html#a31e9065c8a8738ec1515a4631de9eace',1,'levenshtein(QString string1, QString string2, int w, int s, int a, int d):&#160;levenshtein.cpp']]],
  ['loadsettings_721',['loadSettings',['../classSettings.html#a2d965ef0a054b61050811b416c896ed4',1,'Settings']]]
];
