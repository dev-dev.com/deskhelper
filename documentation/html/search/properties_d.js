var searchData=
[
  ['placeholdertextcolor_944',['placeholderTextColor',['../classTheme.html#ab7a2b8912e8fdeb662a6027c4bebf4d9',1,'Theme']]],
  ['previousx_945',['previousX',['../classCustomSystemPanel.html#ac6ab46cf03c51a4c24fa5d88f9445a68',1,'CustomSystemPanel::previousX()'],['../classMain.html#a23c4420e27caa2aa0652bbac6b73f701',1,'Main::previousX()'],['../classResizeArea.html#a8ca0903f218c2f34c00b6bc3eedb2821',1,'ResizeArea::previousX()']]],
  ['previousy_946',['previousY',['../classCustomSystemPanel.html#a666db324353044cfcd4b74fafde9aabb',1,'CustomSystemPanel::previousY()'],['../classMain.html#a96ac1ad1d3fb92766227f6d97675bd5a',1,'Main::previousY()'],['../classResizeArea.html#ac5e2c390095efba26d787c5d7ceb6931',1,'ResizeArea::previousY()']]],
  ['prevx_947',['prevX',['../classRemindersDialog.html#af37baf5d5f768b3fa211c9051d66260e',1,'RemindersDialog::prevX()'],['../classStartWindow.html#a976bc593870290ac46982982c4892b44',1,'StartWindow::prevX()']]],
  ['prevy_948',['prevY',['../classRemindersDialog.html#ab77faee2e63cc7434191b5c6a49bf072',1,'RemindersDialog::prevY()'],['../classStartWindow.html#a08d74c4e8038a32dade9b1f24656d6ee',1,'StartWindow::prevY()']]],
  ['prompt_5ftext_949',['prompt_text',['../classGuideRectangle.html#ad74dd277cae2dc62c408bfbe9c76e7ef',1,'GuideRectangle']]],
  ['proportion_950',['proportion',['../classMain.html#afa6227246f8b374acfeb553caf692644',1,'Main']]]
];
