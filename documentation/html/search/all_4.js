var searchData=
[
  ['darkmode_78',['darkMode',['../classSettings.html#a4d77974e0735f708db0ff831be7ccfa2',1,'Settings']]],
  ['database_79',['Database',['../classDatabase.html',1,'Database'],['../classDatabase.html#a4703c80e6969d33565ea340f768fdadf',1,'Database::Database()']]],
  ['database_2ecpp_80',['database.cpp',['../database_8cpp.html',1,'']]],
  ['database_2eh_81',['database.h',['../database_8h.html',1,'']]],
  ['datearray_82',['dateArray',['../classReminderDateTable.html#ad810452e77a4496dce1992c7a4e37c29',1,'ReminderDateTable']]],
  ['db_83',['db',['../classDatabase.html#aea64d7d99483faec8f049cdd817bb693',1,'Database']]],
  ['deleteanimation_84',['deleteAnimation',['../classReminderTable.html#a6546638618a3d95984c28f14f3f12e05',1,'ReminderTable']]],
  ['dialogwindow_85',['DialogWindow',['../classDialogWindow.html',1,'']]],
  ['dialogwindow_2eqml_86',['DialogWindow.qml',['../DialogWindow_8qml.html',1,'']]],
  ['display_87',['display',['../classX11Info.html#a7f43f4869344f36f1541af7b622d4cb3',1,'X11Info::display()'],['../x11info_8h.html#a8eac9cd918bbb974619385adffa7c81b',1,'Display():&#160;x11info.h']]],
  ['displayerrormessage_88',['displayErrorMessage',['../classSpeechToText.html#a38620f08fd7b791a8c11981078a50f08',1,'SpeechToText']]],
  ['dorepetition_89',['doRepetition',['../classDatabase.html#a8102cf9d341d80b81612e1d56750f479',1,'Database']]],
  ['dx_90',['dx',['../classResizeArea.html#a086f793e2cb45068f9b57ca1ee48f071',1,'ResizeArea']]],
  ['dy_91',['dy',['../classResizeArea.html#a56f271a3748d1ab059cdc6d3cb8c7b8c',1,'ResizeArea']]]
];
