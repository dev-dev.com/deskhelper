; Script generated by the Inno Script Studio Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "deskhelper"
#define MyAppVersion "0.01-beta"
#define MyAppPublisher "dev-dev.com"
#define MyAppURL "https://gitlab.com/dev-dev.com"
#define MyAppExeName "deskhelper.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{C3ED6BE2-8D26-43D2-A9EE-196C476F362B}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
OutputBaseFilename=setup
SetupIconFile=..\..\..\icons\deskhelper.ico
Compression=lzma
SolidCompression=yes
OutputDir=..\..\..\build

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "..\..\..\buildWindows\*"; DestDir: "{app}"; Flags: recursesubdirs ignoreversion
Source: "..\..\..\buildWindows\plugins\*"; DestDir: "{app}\plugins"; Flags: recursesubdirs ignoreversion
Source: "..\..\..\buildWindows\qml\*"; DestDir: "{app}\qml"; Flags: recursesubdirs ignoreversion
Source: "..\..\..\buildWindows\translations\*"; DestDir: "{app}\translations"; Flags: recursesubdirs ignoreversion

; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
