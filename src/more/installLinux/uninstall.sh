#!/bin/bash

iconsPath="/usr/share/icons/hicolor/scalable/apps"
desktopPath="/usr/share/applications" 
installPath="/opt/deskhelper"
linkPath="/usr/bin/deskhelper"
echo "deskhelper uninstall"

#Check user
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root"
    exit 1
fi

rm -v $iconsPath'/deskhelper.svg'
rm -v $desktopPath'/deskhelper.desktop'

#Remove app folder
echo "---"
rm -rfv $installPath
rm -v  $linkPath

echo "Uninstall finished!"