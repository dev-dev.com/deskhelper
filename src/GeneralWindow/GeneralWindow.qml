/**
  * @file
  * @brief Основное окно работы со страницами
  *
  * Окно, имеющее стартовую страницу и меню с возможностью загрузки страниц
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import "../Other" as OtherModules
import "../Style" as StyleModules
import "../CharactersWindow" as CharactersWindowModules

/*!
    \class GeneralWindow
    \brief Окно, имеющее начальную страницу и меню с возможностью загрузки страниц
 */
ApplicationWindow {
    id: generalWindow

    width: Screen.width * 0.79
    height: Screen.height * 0.63

    flags: Qt.FramelessWindowHint | Qt.Window

    objectName: "generalWindow"

    minimumWidth: Screen.width * 0.45
    minimumHeight: Screen.height * 0.5

    signal reminderChanged
    signal settingsChanged
    signal showPromptChanged

    visible: true

    ResizeArea{
        id: resizeArea
        anchors.fill: parent
        window_id: generalWindow
    }

    onSettingsChanged: {
        var language = ""
        if (language_box.currentIndex == 0)
            language = "en"
        else if (language_box.currentIndex == 1)
            language = "ru"

        if (!settings.saveSettings(silent_mode.checked, tray_mode.checked, theme.checked, shortcutEdit.text, language, prompt_mode.checked,fcEdit.text))
        {
            error.err_name = settings.getErrorText()
            error.log = settings.getErrorText()
            error.onlyMessage = false
            error.visible = true
        }
        else
        {
            if (shortcutEdit.text.length > 0)
                stt.setShortcutAppActivation(shortcutEdit.text)
            if (fcEdit.text.length > 0)
                stt.setShortcutFastCommandsActivation(fcEdit.text)
            settings.selectLanguage(language)
            settings.setTrayMode(tray_mode.checked)
            settings.setSilentMode(silent_mode.checked)
            settings.setDarkMode(theme.checked)
            settings.setPromptMode(prompt_mode.checked)
            changeGuideVisible()
            settings.setAppActivation(shortcutEdit.text)
            settings.setFastCommandsActivation(fcEdit.text)
            settings.setActiveLanguage(language)
            settings.trayModeChanged()
            settings.themeChanged()
            settings.promptModeChanged()

            console.log(savedRectangle.state)
            savedTimer.running = true
            savedRectangle.state = "show"//(savedRectangle.state === "hide") ? "show" : "hide"
            listView.updTitles()
            //saveAnimation.start()
        }

    }

    Component.onCompleted: {
        x = Screen.width/2 - width/2
        y = Screen.height/2 - height/2
        repeater.showPrompts()
    }

    Connections {
        target: mainWindow

        onShowImageSelectWindow: {
            //requestActivate() // передаем управление окну выбора персонажа


            //stackView.replace(windowref.get(2).source)
            //requestActivate()
        }

        onReminderWindowChanged: {
            reminderChanged()
        }

        onShowCommandWindow: {

            stackView.replace(windowref.get(1).source)
            requestActivate()
        }
        onShowReminderWindow: {
            stackView.replace(windowref.get(2).source)
            requestActivate()
        }
        onShowNodesWindow: {
            stackView.replace(windowref.get(3).source)
            requestActivate()
        }
        onShowSettingsWindow: {
            //stackView.replace(windowref.get(5).source)
            //requestActivate()
        }

    }



    Shortcut {
        sequences: ["Esc", "Back"]
        enabled: stackView.depth > 1
        onActivated: navigateBackAction.trigger()
    }

    Shortcut {
        sequences: ["Shift+Tab"]
        enabled: true
        onActivated: navigateBackAction.trigger()
    }

    Shortcut {
        sequences: ["Ctrl+Tab"]
        enabled: true
        onActivated: charactersDrawer.open()
    }

    Action {
        id: navigateBackAction
        //  icon.source: "resources/img/trayicon.png" //"resources/icons/20x20@3/" + ( stackView.depth > 1 ? "back" : "drawer") + ".png"
        onTriggered: {
            if (stackView.depth > 1) {
                //stackView.pop()
                drawer.close()
                listView.currentIndex = -1
                //console.log("pop",stackView.depth)
            } else {
                drawer.open()
                //console.log("drawer",stackView.depth)
            }
        }
    }

    CustomSystemPanel{
        id: csp
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        window_id: generalWindow

        Rectangle{
            id: logo_rect
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width: 140
            color: "#30313A"
            Image {
                id: logo_img
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                width: 120
                source: "/resources/img/logo1.svg"
                sourceSize.width: 120
                sourceSize.height: 30
                fillMode: Image.PreserveAspectFit
            }
            Image {
                id: menu_icon
                anchors.top: parent.top
                anchors.left: logo_img.right
                anchors.bottom: parent.bottom
                source: "/resources/img/menu.png"
                width: 20
            }

            states:[
                State {
                    name: "BUTTON_ENTERED"
                    PropertyChanges { target: logo_rect; gradient: style.getAttentionGradientStandart()}
                },
                State {
                    name: "BUTTON_EXITED"
                    PropertyChanges { target: logo_rect; color: "#30313A"}
                }
            ]

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    logo_rect.state = "BUTTON_ENTERED"
                }
                onExited: {
                    logo_rect.state = "BUTTON_EXITED"
                }
                onClicked: {
                    drawer.open()
                }
            }
        }

        Rectangle{
            id: character_rect
            anchors.left: logo_rect.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 30
            color: "#30313A"

            Image {
                id: charact_img
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.topMargin: 3
                anchors.bottomMargin: 3
                source: "/resources/img/character.png"
            }

            states:[
                State {
                    name: "BUTTON_ENTERED"
                    PropertyChanges { target: character_rect; gradient: style.getGradientStandart()}
                },
                State {
                    name: "BUTTON_EXITED"
                    PropertyChanges { target: character_rect; color: "#30313A"}
                }
            ]

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    character_rect.state = "BUTTON_ENTERED"
                }
                onExited: {
                    character_rect.state = "BUTTON_EXITED"
                }
                onClicked: {
                    charactersDrawer.open()
                }
            }
        }
    }

    Drawer {
        id: drawer
        width: Math.min(generalWindow.width, generalWindow.height) / 3 * 2
        height: generalWindow.height

        clip: true
        interactive: stackView.depth === 1

        onClosed: {
            listView.state = "show"
            additional.state = "hide"
        }

        // фон грида
        Rectangle {
            id: fone
            width: parent.width
            height: parent.height
            gradient: style.getDrawerGradient()
            opacity: 1
            z: -1

            Image {
                id: circle
                //anchors.top: fone.bottom
                //anchors.left: fone.left
                anchors.bottom: parent.top
                anchors.left: parent.left
                anchors.bottomMargin: -width/3
                anchors.leftMargin: -width/3
                //width: 120
                source: "/resources/img/redcircle.svg"
                sourceSize.width: 200
                sourceSize.height: 200
                opacity: 0.9
                width: 200
                height: width
                fillMode: Image.PreserveAspectFit
            }
        }


        spacing: 5
        Rectangle {
            id: drawerRectangle
            width: parent.width
            height: parent.height
            color: "transparent"
            SwitchDelegate {
                id: theme
                text: qsTr("Dark mode") + settings.emptyString

                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 10
                width: parent.width - 3

                onCheckedChanged: {
                    if (drawer.opened)
                        settingsChanged()
                }

                checked: settings.getDarkMode()
                contentItem: Text {
                    rightPadding: theme.indicator.width + theme.spacing
                    text: theme.text
                    font: theme.font
                    opacity: enabled ? 1.0 : 0.3
                    color: theme.down ? style.getSdTextColorDown() : style.getSdTextColor()
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                }

                indicator: Rectangle {
                    implicitWidth: 48
                    implicitHeight: 26
                    x: theme.width - width - theme.rightPadding
                    y: parent.height / 2 - height / 2
                    radius: 13
                    color: theme.checked ? style.getSdIndicatorColorChecked() : style.getSdIndicatorColor()
                    border.color: theme.checked ? style.getSdIndicatorBorderColorChecked() : style.getSdIndicatorBorderColor()

                    Rectangle {
                        x: theme.checked ? parent.width - width : 0
                        width: 26
                        height: 26
                        radius: 13
                        color: theme.down ? style.getSdIndicatorLadenColorDown() : style.getSdIndicatorLadenColor()
                        border.color: theme.checked ? (theme.down ? style.getSdIndicatorColorChecked() : style.getSdIndicatorBorderColor()) : style.getSdIndicatorLadenBorderColor()
                    }
                }

                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    color: style.getSdBackgroundColor()
                    opacity: 0.3
                    border.color: theme.pressed ? style.getSdBackgroundBorderColorPressed() : style.getSdBackgroundBorderColor()
                    border.width: theme.pressed ? 3 : 2
                    radius: 5
                }
            }

            ListView {
                id: listView

                focus: true
                currentIndex: -1
                anchors {
                    right: parent.top
                    // left: parent.left
                    //  bottom: parent.bottom
                }
                height: windowref.count * 50

                state: "show"
                states: [ State {
                        name: "hide"
                        AnchorChanges {
                            target: listView

                            anchors.left: undefined
                            anchors.right: parent.left
                        }
                    }, State {
                        name: "show"
                        AnchorChanges {
                            target: listView

                            anchors.left: parent.left
                            anchors.right: parent.right
                        }
                    }

                ]
                transitions: Transition {
                    AnchorAnimation {duration: 500; easing.type: Easing.InOutQuad;
                    }

                }

                anchors.top:  theme.bottom
                anchors.topMargin: 10
                clip: true
                delegate: ItemDelegate {
                    width: parent.width
                    Text {
                        anchors.fill: parent
                        text: model.title
                        color: (model.index === listView.currentIndex) ? "black" : txtColor
                        padding: 10
                    }

                    highlighted: ListView.isCurrentItem
                    onClicked: {
                        listView.currentIndex = index
                        if (model.source === "home")
                            stackView.replace(initItem)
                        else if (model.source === "close")
                            generalWindow.close()
                        else
                            stackView.replace(model.source)
                        drawer.close()
                    }
                }

                model: ListModel {
                    id: windowref
                    ListElement { title: ""; trTitle: "Home"; source: "home" }
                    ListElement { title: ""; trTitle: "Command window"; source: "qrc:/CommandsWindow/CommandWindow.qml" }
                    //ListElement { title: ""; trTitle: "Characters window"; source: "qrc:/CharactersWindow/ImageSelectionMenu.qml" }
                    ListElement { title: ""; trTitle: "Reminders window"; source: "qrc:/RemindersWindow/ReminderWindow.qml" }
                    ListElement { title: ""; trTitle: "Notes window"; source: "qrc:/NotesWindow/NotesWindow.qml" }
                    //ListElement { title: "Settings"; source: "qrc:/SettingsWindow/SettingsWindow.qml" }
                    ListElement { title: ""; trTitle: "Close"; source: "close" }
                    Component.onCompleted: {
                        for(var i = 0; i < windowref.count;i++)
                        {
                            windowref.get(i).title = qsTr(windowref.get(i).trTitle)
                        }
                    }
                }
                function updTitles() {
                    for(var i = 0; i < windowref.count;i++)
                    {
                        windowref.get(i).title = qsTr(windowref.get(i).trTitle)
                    }
                }

                ScrollIndicator.vertical: ScrollIndicator { }
            }

            /*Rectangle {
                width: parent.width
                anchors.top: additional.top
                anchors.bottom: language_row.bottom
                gradient: style.getBackgroundGradient()
                z:0
                radius: 8

            }*/

            Rectangle {
                id: additional
                anchors.bottom: parent.bottom
                width: parent.width
                height: additionalText.height * 2.5
                //anchors.bottom: resizeArea.bottom
                anchors.topMargin: 15
                //gradient: style.getBackgroundGradient()
                color: "transparent"

                state: "hide"
                states: [ State {
                        name: "hide"
                        AnchorChanges {
                            target: additional
                            anchors.top: undefined
                            anchors.bottom: parent.bottom
                        }
                    }, State {
                        name: "show"
                        AnchorChanges {
                            target: additional

                            anchors.bottom: undefined
                            anchors.top: theme.bottom
                        }
                        PropertyChanges {
                            target: additional
                            anchors.topMargin: 10
                        }
                    }

                ]
                transitions: Transition {
                    AnchorAnimation {duration: 500; easing.type: Easing.InOutQuad;
                        onFinished: {
                            if (additional.state === "hide")
                                silent_mode.state = "hide"
                        }
                    }

                }


                MouseArea {
                    id: maAdditional
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: additional.opacity = 1
                    onExited: additional.opacity = 0.7
                    onClicked: {
                        if (additional.state === "hide") {
                            silent_mode.state = "show"
                            listView.state = "hide"
                            additional.state = "show"
                            repeater.itemAt(0).visible = false
                        } else {
                            listView.state = "show"
                            additional.state = "hide"
                            repeater.itemAt(0).visible = guideVisibleMenu[0]
                        }
                    }
                }
                opacity: 0.7
                /*Rectangle {
                    anchors.fill: additionalText

                    color: style.getSdBackgroundColor()
                    opacity: 0.3
                    border.color: style.getSdBackgroundBorderColor()
                    border.width: 2
                    radius: 5
                }*/
                Text {
                    id: additionalText
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Additionally") + settings.emptyString
                    color: style.getTextColor()

                }
                OtherModules.Arrow {
                    id: arrow
                    rotation: -90
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.horizontalCenterOffset: -5
                    anchors.top: additionalText.bottom
                    anchors.topMargin: 10
                    arrowColor: style.getTextColor()
                    width: additionalText.width / 15
                    height: width / 6

                    arrowInterval: width / 3.5
                    radius: 4
                }
            }

            SwitchDelegate {
                id: silent_mode
                text: qsTr("Silent mode") + settings.emptyString

                //anchors.top: parent.bottom
                //anchors.topMargin: 10
                width: parent.width

                state: "hide"
                states: [ State {
                        name: "hide"
                        AnchorChanges {
                            target: silent_mode

                            anchors.top: parent.bottom
                        }
                    }, State {
                        name: "show"
                        AnchorChanges {
                            target: silent_mode

                            anchors.top: additional.bottom
                        }
                        PropertyChanges {
                            target: silent_mode
                            anchors.topMargin: 5
                        }
                    }

                ]


                //anchors.top: theme.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                //anchors.topMargin: 10
                //width: parent.width - 3
                onCheckedChanged: {
                    if (drawer.opened)
                        settingsChanged()
                }

                checked: settings.getSilentMode()
                contentItem: Text {
                    rightPadding: silent_mode.indicator.width + silent_mode.spacing
                    text: silent_mode.text
                    font: silent_mode.font
                    opacity: enabled ? 1.0 : 0.3
                    color: silent_mode.down ? style.getSdTextColorDown() : style.getSdTextColor()
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                }

                indicator: Rectangle {
                    implicitWidth: 48
                    implicitHeight: 26
                    x: silent_mode.width - width - silent_mode.rightPadding
                    y: parent.height / 2 - height / 2
                    radius: 13
                    color: silent_mode.checked ? style.getSdIndicatorColorChecked() : style.getSdIndicatorColor()
                    border.color: silent_mode.checked ? style.getSdIndicatorBorderColorChecked() : style.getSdIndicatorBorderColor()

                    Rectangle {
                        x: silent_mode.checked ? parent.width - width : 0
                        width: 26
                        height: 26
                        radius: 13
                        color: silent_mode.down ? style.getSdIndicatorLadenColorDown() : style.getSdIndicatorLadenColor()
                        border.color: silent_mode.checked ? (silent_mode.down ? style.getSdIndicatorColorChecked() : style.getSdIndicatorBorderColor()) : style.getSdIndicatorLadenBorderColor()
                    }
                }

                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    color: style.getSdBackgroundColor()
                    opacity: 0.3
                    border.color: silent_mode.pressed ? style.getSdBackgroundBorderColorPressed() : style.getSdBackgroundBorderColor()
                    border.width: silent_mode.pressed ? 3 : 2
                    radius: 5
                }
            }

            SwitchDelegate {
                id: tray_mode
                text: qsTr("Minimize to tray") + settings.emptyString

                anchors.top: silent_mode.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 3
                width: parent.width - 3

                onCheckedChanged: {
                    if (drawer.opened)
                        settingsChanged()
                }

                checked: settings.getTrayMode()
                contentItem: Text {
                    rightPadding: tray_mode.indicator.width + tray_mode.spacing
                    text: tray_mode.text
                    font: tray_mode.font
                    opacity: enabled ? 1.0 : 0.3
                    color: tray_mode.down ? style.getSdTextColorDown() : style.getSdTextColor()
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                }

                indicator: Rectangle {
                    implicitWidth: 48
                    implicitHeight: 26
                    x: tray_mode.width - width - tray_mode.rightPadding
                    y: parent.height / 2 - height / 2
                    radius: 13
                    color: tray_mode.checked ? style.getSdIndicatorColorChecked() : style.getSdIndicatorColor()
                    border.color: tray_mode.checked ? style.getSdIndicatorBorderColorChecked() : style.getSdIndicatorBorderColor()

                    Rectangle {
                        x: tray_mode.checked ? parent.width - width : 0
                        width: 26
                        height: 26
                        radius: 13
                        color: tray_mode.down ? style.getSdIndicatorLadenColorDown() : style.getSdIndicatorLadenColor()
                        border.color: tray_mode.checked ? (tray_mode.down ? style.getSdIndicatorColorChecked() : style.getSdIndicatorBorderColor()) : style.getSdIndicatorLadenBorderColor()
                    }
                }

                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    color: style.getSdBackgroundColor()
                    opacity: 0.3
                    border.color: tray_mode.pressed ? style.getSdBackgroundBorderColorPressed() : style.getSdBackgroundBorderColor()
                    border.width: tray_mode.pressed ? 3 : 2
                    radius: 5
                }
            }

            Row {

                /** @brief сеттер для поля ввода сочетания клавиш */
                function setShortcut(shortcut) {
                    shortcutEdit.text = shortcut
                }

                id: shortcut_row
                anchors.top: tray_mode.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 3
                width: parent.width - 3
                height: 40
                Rectangle {
                    implicitWidth: parent.width
                    implicitHeight: parent.height
                    color: style.getSdBackgroundColor()
                    opacity: 0.3
                    border.color: style.getSdBackgroundBorderColor()
                    radius: 5
                }
                Label {
                    id: shortcut_label
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.leftMargin: 10
                    text: qsTr("Application activation: ") + settings.emptyString
                    font.pixelSize: tray_mode.font.pixelSize
                    color: style.getSdTextColor()
                }
                TextField {
                    id: shortcutEdit
                    //width: 200
                    height: 30

                    anchors.left: shortcut_label.right
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.rightMargin: 10
                    anchors.leftMargin: 27

                    selectByMouse: true
                    placeholderText: qsTr("Enter shortcut") + settings.emptyString
                    placeholderTextColor: "#C7C7C7"
                    color: style.getSdTextColor()

                    onTextChanged: {
                        if (text.length > 0 && drawer.opened)
                            settingsChanged()
                    }

                    readOnly: true
                    text: settings.getAppActivation()
                    background: Rectangle {
                        implicitWidth: parent.width
                        implicitHeight: parent.height
                        color: "transparent"
                        border.color: parent.activeFocus ? style.getSdBackgroundBorderColorPressed() : style.getSdBackgroundBorderColor()
                        border.width: 2
                        radius: 5
                    }

                    onPressed: {
                        keySelection.clearData()
                        keySelection.typeOfSettings = 0
                        keySelection.visible = (keySelection.visible) ? false : true
                    }
                }
            }

            OtherModules.ErrorDialog {
                id: keySelection
                onlyMessage: true
                onWindowClosed: {
                    var shortcutsText = ""
                    for (var i in keySelection.shortcutHead)
                    {
                        shortcutsText += keySelection.shortcutHead[i]

                    }
                    if (shortcutsText !== "")
                    {
                        if (typeOfSettings === 1)
                            fc_row.setFC(shortcutsText)
                        else if (typeOfSettings === 0)
                            shortcut_row.setShortcut(shortcutsText)
                    }
                }
            }

            Row {

                id: language_row

                anchors.top: shortcut_row.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 3
                width: parent.width - 3

                height: 40

                Rectangle {
                    implicitWidth: parent.width
                    implicitHeight: parent.height
                    color: style.getSdBackgroundColor()
                    opacity: 0.3
                    border.color: style.getSdBackgroundBorderColor()
                    radius: 5
                }

                Label {
                    id: language_label
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.leftMargin: 10
                    text: qsTr("Language: ") + settings.emptyString
                    font.pixelSize: tray_mode.font.pixelSize
                    color: style.getSdTextColor()
                }

                ComboBox {
                    id: language_box

                    anchors.left: shortcut_label.right
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.rightMargin: 10

                    leftPadding: 10

                    model: ["English", "Русский"]

                    onCurrentIndexChanged: {
                        if (drawer.opened)
                            settingsChanged()
                    }

                    delegate: ItemDelegate {
                        width: language_box.width

                        Rectangle {
                            anchors.fill: parent
                            color: style.getSdIndicatorColorChecked()
                            opacity: 0.9
                        }

                        contentItem: Text {
                            text: modelData
                            color: "white"
                            font: language_box.font
                            elide: Text.ElideRight
                            verticalAlignment: Text.AlignVCenter
                        }

                        highlighted: language_box.highlightedIndex === index
                    }

                    /*indicator: Canvas {
                        id: canvas
                        x: language_box.width - width - language_box.rightPadding
                        y: language_box.topPadding + (language_box.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"

                        Connections {
                            target: language_box
                            onPressedChanged: canvas.requestPaint()
                        }

                        onPaint: {
                            context.reset();
                            context.moveTo(0, 0);
                            context.lineTo(width, 0);
                            context.lineTo(width / 2, height);
                            context.closePath();
                            context.fillStyle = language_box.pressed ? style.getSdBackgroundBorderColor() : style.getSdIndicatorColorChecked();
                            context.fill();
                        }
                    }*/

                    contentItem: Text {
                        leftPadding: 0
                        rightPadding: language_box.indicator.width + language_box.spacing

                        text: language_box.displayText
                        font: language_box.font
                        color: language_box.pressed ? style.getSdTextColorDown() : style.getSdTextColor()
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                    }

                    background: Rectangle {
                        implicitWidth: 120
                        implicitHeight: 30
                        color: "transparent"
                        border.color: language_box.pressed ? style.getSdIndicatorBorderColor() : style.getSdBackgroundBorderColor()
                        border.width: language_box.pressed ? 3 : 2
                        radius: 5
                    }

                    popup: Popup {
                        y: language_box.height - 1
                        width: language_box.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1

                        contentItem: ListView {

                            clip: true

                            //implicitHeight: contentHeight
                            implicitHeight: 80
                            model: language_box.popup.visible ? language_box.delegateModel : null
                            currentIndex: language_box.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }

                        background: Rectangle {
                            color: style.getSdIndicatorColorChecked()
                            opacity: 0.7
                            border.color: style.getSdIndicatorBorderColor()
                            radius: 2
                        }
                    }

                    currentIndex: (settings.getActiveLanguage() === "en") ? 0 : 1
                }
            }

            SwitchDelegate {
                id: prompt_mode
                text: qsTr("Show prompts") + settings.emptyString

                anchors.top: language_row.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 3
                width: parent.width - 3

                onCheckedChanged: {
                    if (drawer.opened)
                        settingsChanged()
                }

                checked: settings.getPromptMode()
                contentItem: Text {
                    rightPadding: prompt_mode.indicator.width + prompt_mode.spacing
                    text: prompt_mode.text
                    font: prompt_mode.font
                    opacity: enabled ? 1.0 : 0.3
                    color: prompt_mode.down ? style.getSdTextColorDown() : style.getSdTextColor()
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                }

                indicator: Rectangle {
                    implicitWidth: 48
                    implicitHeight: 26
                    x: prompt_mode.width - width - prompt_mode.rightPadding
                    y: parent.height / 2 - height / 2
                    radius: 13
                    color: prompt_mode.checked ? style.getSdIndicatorColorChecked() : style.getSdIndicatorColor()
                    border.color: prompt_mode.checked ? style.getSdIndicatorBorderColorChecked() : style.getSdIndicatorBorderColor()

                    Rectangle {
                        x: prompt_mode.checked ? parent.width - width : 0
                        width: 26
                        height: 26
                        radius: 13
                        color: prompt_mode.down ? style.getSdIndicatorLadenColorDown() : style.getSdIndicatorLadenColor()
                        border.color: prompt_mode.checked ? (prompt_mode.down ? style.getSdIndicatorColorChecked() : style.getSdIndicatorBorderColor()) : style.getSdIndicatorLadenBorderColor()
                    }
                }

                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    color: style.getSdBackgroundColor()
                    opacity: 0.3
                    border.color: prompt_mode.pressed ? style.getSdBackgroundBorderColorPressed() : style.getSdBackgroundBorderColor()
                    border.width: prompt_mode.pressed ? 3 : 2
                    radius: 5
                }
            }

            Row {

                /** @brief сеттер для поля ввода сочетания клавиш */
                function setFC(fc) {
                    fcEdit.text = fc
                }

                id: fc_row
                anchors.top: prompt_mode.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 3
                width: parent.width - 3
                height: 40
                Rectangle {
                    implicitWidth: parent.width
                    implicitHeight: parent.height
                    color: style.getSdBackgroundColor()
                    opacity: 0.3
                    border.color: style.getSdBackgroundBorderColor()
                    radius: 5
                }
                Label {
                    id: fc_label
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.leftMargin: 10
                    text: qsTr("Fast commands activation: ") + settings.emptyString
                    font.pixelSize: tray_mode.font.pixelSize
                    color: style.getSdTextColor()
                }
                TextField {
                    id: fcEdit
                    //width: 200
                    height: 30

                    anchors.left: fc_label.right
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.rightMargin: 10
                    anchors.leftMargin: 27

                    selectByMouse: true
                    placeholderText: qsTr("Enter shortcut") + settings.emptyString
                    placeholderTextColor: "#C7C7C7"
                    color: style.getSdTextColor()

                    onTextChanged: {
                        if (text.length > 0 && drawer.opened)
                            settingsChanged()
                    }

                    readOnly: true
                    text: settings.getFastCommandsActivation()
                    background: Rectangle {
                        implicitWidth: parent.width
                        implicitHeight: parent.height
                        color: "transparent"
                        border.color: parent.activeFocus ? style.getSdBackgroundBorderColorPressed() : style.getSdBackgroundBorderColor()
                        border.width: 2
                        radius: 5
                    }

                    onPressed: {
                        keySelection.clearData()
                        keySelection.typeOfSettings = 1
                        keySelection.visible = (keySelection.visible) ? false : true
                    }
                }
            }

            Rectangle {
                //anchors.top: parent.top
                //anchors.horizontalCenter: parent.horizontalCenter
                id: savedRectangle
                //color: "black"
                //anchors.top: listView.bottom
                gradient: style.getAttentionGradientStandart()
                radius: 5
                width: parent.width / 2
                anchors.horizontalCenter: parent.horizontalCenter
                height: 30

                Text{
                    anchors.fill: parent
                    //anchors.horizontalCenter: parent.horizontalCenter
                    //anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Saved!") + settings.emptyString
                    color: "white"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                state: "hide"
                states: [ State {
                        name: "hide"
                        AnchorChanges {
                            target: savedRectangle
                            anchors.bottom: undefined
                            anchors.top: parent.bottom

                        }
                    }, State {
                        name: "show"
                        AnchorChanges {
                            target: savedRectangle

                            anchors.top: undefined
                            anchors.bottom: parent.bottom
                        }
                        PropertyChanges {
                            target: savedRectangle
                            anchors.bottomMargin: additional.height + 5
                        }
                    }

                ]
                transitions: Transition {
                    AnchorAnimation {
                        duration: 500
                        easing.type: Easing.InOutQuad
                    }
                }
                Timer {
                    id: savedTimer
                    interval: 1000
                    running: false
                    onTriggered: {
                        savedRectangle.state = "hide"
                        running = false
                    }
                }


                //anchors.left: csp.left
                //anchors.leftMargin: 10
                // anchors.bottom: csp.bottom
                //  anchors.top: drawerRectangle.bottom

            }


        }

    }

    StackView {
        id: stackView

        anchors.right: csp.right
        anchors.left: csp.left
        anchors.bottom: parent.bottom
        anchors.top: csp.bottom
        z: 1
        initialItem: initItem

    }

    Drawer {
        id: charactersDrawer
        width: generalWindow.width
        height: generalWindow.height / 5 * 3
        edge: Qt.BottomEdge
        Rectangle {
            height: parent.height
            width: parent.width
            gradient: style.getBackgroundGradient()
            CharactersWindowModules.ImageSelectionMenu {
                id: imgSelectMenu
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    horizontalCenter: parent.horizontalCenter
                }
                width: parent.width/5*3.5

            }
        }

    }

    Component {
        id: initItem

        Pane {
            id: pane
            property string tColor: style.getTextColor()
            background: LinearGradient {
                id: lg
                anchors.fill: parent
                anchors.topMargin: 0
                start: Qt.point(0,0)
                end: Qt.point(parent.width, parent.height)

                gradient: style.getBackgroundGradient()
            }
            Image {
                id: logo
                width: pane.availableWidth / 2
                height: pane.availableHeight / 2
                anchors.centerIn: parent
                anchors.verticalCenterOffset: -50
                fillMode: Image.PreserveAspectFit
                source: "/resources/img/biglogo1.svg"
            }

            Label {
                text: qsTr("There is a menu on the left, you can open it and choose what you need. I know it.") + settings.emptyString
                anchors.margins: 20
                anchors.top: logo.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter
                wrapMode: Label.Wrap
                font.family: "Cipitillo"
                color: tColor
            }

        }

    }

    property string txtColor: style.getTextColor()
    StyleModules.Style {
        id: style
    }

    Repeater{
        id: repeater
        model: 4
        delegate: OtherModules.GuideRectangle {
            id: guide_ism
            z: 10
            /*onPrompt_clicked: guide_timer.start()
            img_source: "/resources/img/prompt"

            onCheckedChanged: {
                var flag = false;
                if (checked) {
                    guideVisibleMenu[model.index] = false
                    for (var i = 0; i < repeater.count; ++i){
                        if (guideVisibleMenu[i] === true){
                            flag = false;
                            break;
                        }
                        else{
                            flag = true
                        }
                    }

                    if (flag === true){
                        console.log("success!");
                        menu_flag = true;
                    }
                }
            }
            Timer{
                id: guide_timer
                interval: 5000
                repeat: false
                running: false
                onTriggered: {
                    //guide_ism.visible = false
                    guide_ism.startMinimizeAnimation();
                }
            }*/

        }
        function showPrompts() {
            changeGuideVisible()
            for (var i = 0; i < repeater.count; ++i){

                if (i === 0){
                    repeater.itemAt(i).promptText = qsTr("This is the navigation menu. Here you can select the required module, as well as change application settings") + settings.emptyString
                    repeater.itemAt(i).parent = drawerRectangle
                    repeater.itemAt(i).anchors.top = listView.bottom
                }
                if (i === 1){
                    repeater.itemAt(i).promptText = qsTr("Click here to open the settings") + settings.emptyString
                    repeater.itemAt(i).parent = drawerRectangle
                    repeater.itemAt(i).anchors.bottom = additional.top
                    repeater.itemAt(i).anchors.horizontalCenter = additional.horizontalCenter
                }
                if (i === 2){
                    repeater.itemAt(i).promptText = qsTr("This is the settings here. You can configure the program as you please") + settings.emptyString
                    repeater.itemAt(i).parent = prompt_mode
                    repeater.itemAt(i).anchors.top = language_row.bottom //silent_mode
                }
                if (i === 3){
                    repeater.itemAt(i).promptText = qsTr("On the left is the navigation menu (Shift+Tab) and character library (Ctrl+Tab)") + settings.emptyString
                    repeater.itemAt(i).parent = charact_img
                    repeater.itemAt(i).promptRotation = 180
                    repeater.itemAt(i).anchors.left = charact_img.right
                    repeater.itemAt(i).anchors.top = charact_img.top
                }
            }
        }
    }
    onShowPromptChanged: {
        for (let i = 0; i < repeater.count; ++i) {

            repeater.itemAt(i).doUpdate(guideVisibleMenu[i])
            //repeater.itemAt(i).visible = guideVisibleMenu[i]
            //repeater.itemAt(i).enabled_value = true

        }
        imgSelectMenu.changeGuideVisible()
    }

    // меняем все флаги видимости и вызываем сигнал для измененения
    // видимостей подсказок соответственно этим флагам
    function changeGuideVisible() {
        let promptMode = settings.getPromptMode()
        let changeGuideVis = (mainWindow.guideVisibleImgSel !== [])
        for (var i = 0; i < 8;i++)
            mainWindow.guideVisibleImgSel[i] = promptMode
        for (i = 0; i < 7;i++)
            mainWindow.guideVisibleComWin[i] = promptMode
        for (i = 0; i < 4;i++)
            mainWindow.guideVisibleRemWin[i] = promptMode
        for (i = 0; i < 6;i++)
            mainWindow.guideVisibleNotWin[i] = promptMode
        for (i = 0; i < 4;i++)
            mainWindow.guideVisibleMenu[i] = promptMode
        if (changeGuideVis)
            showPromptChanged()
    }

    function getImgVisible(i) {
        return mainWindow.guideVisibleImgSel[i]
    }
    function setImgVisible(i) {
        mainWindow.guideVisibleImgSel[i] = false
    }

    function getComWinVisible(i) {
        return mainWindow.guideVisibleComWin[i]
    }
    function setComWinVisible(i) {
        mainWindow.guideVisibleComWin[i] = false
    }

    function getNotWinVisible(i) {
        return mainWindow.guideVisibleNotWin[i]
    }
    function setNotWinVisible(i) {
        mainWindow.guideVisibleNotWin[i] = false
    }

    function getRemWinVisible(i) {
        return mainWindow.guideVisibleRemWin[i]
    }
    function setRemWinVisible(i) {
        mainWindow.guideVisibleRemWin[i] = false
    }

    function setPromptModeInSettings(mode) {
        prompt_mode.checked = mode
    }

}

