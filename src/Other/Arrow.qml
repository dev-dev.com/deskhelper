import QtQuick 2.0

Rectangle {
    id: arrow

    color: "transparent"
    property string arrowColor
    property var arrowInterval : 0
    //width: 100
    //height: 40

    Rectangle {
        id: arrowTopParts
        width: parent.width / 2
        height: parent.height/2
        rotation: -45
        radius: 4
        anchors.top: parent.top
        anchors.right: parent.right
        color: arrowColor
    }
    Rectangle {
        width: arrowTopParts.width
        height: arrowTopParts.height
        rotation: -arrowTopParts.rotation
        radius: arrowTopParts.radius
        anchors.right: parent.right
        anchors.top: arrowTopParts.bottom
        //anchors.topMargin: arrowTopParts.width - height - height / 3
        anchors.topMargin: -height / 3 + arrowInterval
        color: arrowColor
    }
}
