/**
  * @file
  * @brief Диалог с ошибками
  *
  * Оповещение пользователя об ошибке
*/

import QtQuick 2.0
import QtQuick.Dialogs 1.3
import QtMultimedia 5.14

/*!
    \class ErrorDialog
    \brief Диалог ошибки (или же нажатия сочетания клавиш)
 */
Dialog {

    /** @brief Лог вывода*/
    property string log
    /** @brief Текст для вывода ошибки*/
    property string err_name

    /** @brief Функция для старта звукового уведомления*/
    function playSound (){
        if (messageDialog.visible == true && !settings.getSilentMode())
            error_sound.play()
    }

    /** @brief Флаг для разграничения диалога ошибки и диалога выбора сочетания клавиш*/
    property bool onlyMessage: false
    /** @brief Кол-во нажатых клавиш*/
    property var countPressed: 0
    /** @brief Кол-во отпущенных клавиш*/
    property var countReleased: 0
    /** @brief Массив спец клавиш(Ctrl,Shift..)*/
    property var shortcutHead: []
    /** @brief Массив остальных нажатых клавиш*/
    property var shortcutTail: []
    /** @brief Тип изменяемых настроек*/
    property var typeOfSettings: 0
    /** @brief Сигнал о том, что окно закрыто*/
    signal windowClosed


    id: messageDialog
    title: (onlyMessage) ? qsTr("Enter shortcuts") + settings.emptyString : qsTr("Error!") + settings.emptyString
    onAccepted: {
        console.log(log)
    }
    width: 450
    height: 200

    onVisibleChanged: playSound()

    /** @brief Очистка введенных сочетаний*/
    function clearData()
    {
        countPressed = 0
        countReleased = 0
        shortcutHead = []
        shortcutTail = []
    }

    Audio{
        id: error_sound
        source: "/resources/sounds/error_sound.mp3"
        //autoPlay: parent.visible == true ? true : false
        volume: 0.3
        muted: onlyMessage
    }

    contentItem: Rectangle {
        width: 400
        height: 200
        gradient: Gradient{
            GradientStop {
                position: 0
                color: "#3C4653"
            }
            GradientStop {
                position: 1
                color: "#30313A"
            }
        }

        Keys.onReleased: {



            if (onlyMessage)
            {
                if (event.key === Qt.Key_Alt)//Qt.AltModifier)
                {
                    if (shortcutHead.toString().search("Alt") === -1)
                        shortcutHead += (shortcutHead.length != 0) ? ["+"] + ["Alt"] : ["Alt"]
                    else
                        return
                }
                else if (event.key === Qt.Key_Shift)//Qt.ShiftModifier)
                {
                    if (shortcutHead.toString().search("Shift") === -1)
                        shortcutHead += (shortcutHead.length != 0) ? ["+"] + ["Shift"] : ["Shift"]
                    else
                        return
                }
                else if (event.key === Qt.Key_Control)//Qt.ControlModifier)
                {
                    if (shortcutHead.toString().search("Ctrl") === -1)
                    {
                        if (shortcutHead.length === 0)
                            shortcutHead += ["Ctrl"]
                        else
                        {
                            shortcutHead = ["Ctrl"] + ["+"] + shortcutHead
                        }
                    }
                    else
                        return
                }
                else if (event.key === Qt.Key_Tab)//Qt.ControlModifier)
                {
                    if (shortcutHead.toString().search("Tab") === -1)
                        shortcutHead += (shortcutHead.length != 0) ? ["+"] + ["Tab"] : ["Tab"]
                    else
                        return
                }
                else
                {
                    if (shortcutTail.toString().search(db.getSymbol(event.key)) === -1)
                        shortcutTail +=  (shortcutTail.length != 0) ? ["+" + db.getSymbol(event.key)] :
                                                                      [db.getSymbol(event.key)]
                    else
                        return
                }
                //console.log("!",event.text,"!")
                //console.debug(">",db.getSymbol(event.key))
                countReleased++

                if (countPressed === countReleased)
                {
                    if (shortcutTail.length === 0 || shortcutHead.length === 0)
                        shortcutHead = []
                    else
                        shortcutHead += ["+"] + shortcutTail
                    if (countReleased === 3)
                    {
                        if (shortcutHead.toString().search("Alt") != -1 &&
                            shortcutHead.toString().search("Ctrl") != -1 &&
                            shortcutHead.toString().search("S") != -1 )
                            shortcutHead = []
                    }

                    windowClosed()
                    close()
                }
            }
            //keySelection.visible = (keySelection.visible) ? false : true
            //bar.currentIndex = 1
        }


        Keys.onPressed: {
            if (onlyMessage) {

                if (event.key === Qt.Key_Alt)//Qt.AltModifier)
                {
                    if (shortcutHead.toString().search("Alt") === -1)
                        countPressed++
                    else
                        return
                }
                else if (event.key === Qt.Key_Shift)//Qt.ShiftModifier)
                {
                    if (shortcutHead.toString().search("Shift") === -1)
                        countPressed++
                    else
                        return
                }
                else if (event.key === Qt.Key_Control)//Qt.ControlModifier)
                {
                    if (shortcutHead.toString().search("Ctrl") === -1)
                    {
                        countPressed++
                    }
                    else
                        return
                }
                else if (event.key === Qt.Key_Tab)//Qt.ControlModifier)
                {
                    if (shortcutHead.toString().search("Tab") === -1)
                        shortcutHead += (shortcutHead.length != 0) ? ["+"] + ["Tab"] : ["Tab"]
                    else
                        return
                }
                else
                {
                    if (shortcutTail.toString().search(db.getSymbol(event.key)) === -1)
                        countPressed++
                    else
                        return
                }
            }
        }

        Image {
            id: error_image
            width: 100
            height: 100
            visible: !onlyMessage
            anchors{
                top: parent.top
                left: parent.left
                topMargin: 10
                leftMargin: 10
            }

            source: "/resources/img/error.png"
        }

        Text {
            anchors{
                top: parent.top
                left: error_image.right
                right: parent.right
                bottom: ok_button.top
                topMargin: 30
                bottomMargin: 10
                rightMargin: 10
                leftMargin: 10
            }
            text: err_name
            visible: !onlyMessage
            color: "white"
            //anchors.horizontalCenter: parent.horizontalCenter
            //anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 11
            wrapMode: Text.WordWrap

        }

        StylishButton {
            id: ok_button
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.bottomMargin: 10
            anchors.rightMargin: 20
            visible: !onlyMessage
            button_name: "OK"
            background_gradient: gradientStandart
            entered_gradient: gradientEntered
            width: 100
            onMouseAreaClicked: messageDialog.close()
        }
    }

    Gradient {
        id: gradientStandart
        GradientStop {
            position: 0
            color: "#6C669D"
        }
        GradientStop {
            position: 1
            color: "#6534AC"
        }
    }

    Gradient {
        id: gradientEntered
        GradientStop {
            position: 0
            color: "#777EC4"
        }
        GradientStop {
            position: 1
            color: "#7652AC"
        }
    }
}
