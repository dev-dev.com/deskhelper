/**
  * @file
  * @brief Пользовательская кнопка
  *
*/
import QtQuick 2.12

/*!
    \class StylishButton
    \brief Пользовательская кнопка
 */
Rectangle
{
    id: button
    /** @brief Текст кнопки */
    property string button_name
    /** @brief Градиент кнопки */
    property Gradient background_gradient
    /** @brief Градиент кнопки при нажатии */
    property Gradient entered_gradient
    /** @brief Цвет границ кнопки */
    property string border_color//: style.getBorderColor()
    /** @brief Цвет текста */
    property string text_color: "#ffffff"
    /** @brief Шрифт */
    property var font_size: 10
    /** @brief Путь до изображения */
    property string image_src

    /** @brief Сигнал о нажатии кнопки */
    signal mouseAreaClicked

    states:[
        State {
            name: "BUTTON_ENTERED"
            PropertyChanges { target: button; gradient: entered_gradient}
        },
        State {
            name: "BUTTON_EXITED"
            PropertyChanges { target: button; gradient: background_gradient}
        }
    ]

    MouseArea{
        id: m_a
        objectName: "stylishArea"
        anchors.fill:parent
        anchors.bottom: parent.bottom
        width:parent.width
        height: parent.height

        hoverEnabled: true
        onEntered: button.state = "BUTTON_ENTERED"
        onExited:  button.state = "BUTTON_EXITED"
        onClicked: button.mouseAreaClicked()
    }
    gradient: background_gradient
    implicitWidth: 150
    implicitHeight: 35
    opacity: enabled ? 1 : 0.3
    border.width: 1
    border.color: border_color
    radius: 4

    Text {
        anchors.fill:parent
        anchors.bottom: parent.bottom
        width:parent.width
        height: parent.height

        text: button_name
        font.pointSize: font_size
        opacity: enabled ? 1.0 : 0.3
        color: text_color
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    Image {
        //anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        height: button.height * 0.75
        width: button.height * 0.75
        anchors.topMargin: 5
        anchors.bottomMargin: 5
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        id: button_image
        source: image_src
    }
}
