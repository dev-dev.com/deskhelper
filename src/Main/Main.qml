/**
  * @file
  * @brief Безрамочное окно с персонажем
  *
  * Окно с персонажем, из которого открываются все другие окна.
  * Имеет контексное меню, "отзывается" на горячие клавиши
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import Qt.labs.platform 1.1
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import "../Other" as OtherModules
import "../Style" as StyleModules
import "../StartWindow" as StartWindowModules
import "../GeneralWindow" as GeneralWindowModules
import "../RemindersWindow" as RemindersWindowModules
import "../FastCommandsWindow" as FastCommandsWindowModules


Window {
    id: mainWindow
    /** @brief Флаг видимости стартового окна*/
    property bool visible_value: !start_window.visible
    visible: visible_value
    // ставим окну размеры изображения
    width: character.width
    height: character.height

    property var guideVisibleImgSel: []
    property var guideVisibleComWin: []
    property var guideVisibleRemWin: []
    property var guideVisibleNotWin: []
    property var guideVisibleMenu: []

    /*property real prevValue: 0.5
    Slider {

        x: 1
        id: slider
        snapMode: Slider.SnapAlways
        stepSize: 0.2
        z: 4
        orientation: Qt.Vertical
        value: 0.5
        //touchDragThreshold: 10
        onValueChanged: {
            //mainWindow.width = character.width + 100*(value - prevValue)
            character.width = character.width + 100*(value - prevValue)
            characterResizingAmination.start()
            prevValue = value
        }
        handle: Rectangle{
            id: handleRectangle
            width: 15
            height: 15
            radius: 10
            y: (slider.height - handleRectangle.height)/2
            x: -1
            color: "blue"
        }
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            drag {
                target: handleRectangle
                axis: Drag.YAxis

                minimumY: slider.y
                maximumY: slider.height - handleRectangle.height
            }
            onPositionChanged: {
                //console.log(handleRectangle.y)
                //bhvr.enabled = true
                slider.value = (slider.height - handleRectangle.height - handleRectangle.y)/(slider.height - handleRectangle.height)
            }
        }
    }*/

    // цвет окна - прозрачный
    color: "transparent"

    // имя окна
    title: qsTr("Desk Helper") + settings.emptyString

    // окно без рамок
    flags: Qt.FramelessWindowHint | Qt.Window

    /** @brief Х до перемещения мыши*/
    property int previousX
    /** @brief Y до перемещения мыши*/
    property int previousY

    // сигналы для передачи фокуса окнам
    /** @brief Сигнал для открытия окна выбора персонажа*/
    signal showImageSelectWindow
    /** @brief Сигнал для открытия окна команд*/
    signal showCommandWindow
    /** @brief Сигнал для открытия окна напоминаний*/
    signal showReminderWindow
    /** @brief Сигнал для открытия окна заметок*/
    signal showNodesWindow
    /** @brief Сигнал для открытия окна настроек*/
    signal showSettingsWindow
    /** @brief Сигнал для открытия окна быстрого открытия команд*/
    signal showFastCommandsWindow

    /** @brief Сигнал для начала оповещения*/
    signal triggerNotifTimer
    signal reminderWindowChanged
    /**
     * @brief Изменение размеров окна
     * @details Фиксация окна по размеру изображения
    */
    function showWindow() {
        mainWindow.width = character.width
        mainWindow.height = character.height
    }

    /**
     * @brief Изменение размеров окна
     * @details Окно сужается до 0х0
    */
    function hideWindow() {
        mainWindow.width = 0
        mainWindow.height = 0
    }

    StartWindowModules.StartWindow {
        id:start_window
        visible: !settings.getStartWindowViewed()
        onVisibleChanged: {
            if (!visible){
                guide_dialog.visible = true
            }
        }
    }

    FastCommandsWindowModules.FastCommandsWindow {
        id: fastCommandsWindow
        visible: false
    }

    function updFastCommands() {
        fastCommandsWindow.updFastCommands()
    }

    Dialog {
        id: guide_dialog
        width: 400
        height: 150
        contentItem: Rectangle {
            width: 400
            height: 200
            gradient: style.getBackgroundGradient()
            Text {
                anchors{
                    top: parent.top
                    left: parent.left
                    right: parent.right
                    bottom: ok_button.top
                    topMargin: 30
                    bottomMargin: 10
                    rightMargin: 10
                    leftMargin: 10
                }
                text: qsTr("Want to go through the program guide?")
                color: "white"
                font.pointSize: 11
                wrapMode: Text.WordWrap

            }

            OtherModules.StylishButton {
                id: cancel_button
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.bottomMargin: 10
                anchors.rightMargin: 20
                button_name: qsTr("Cancel")
                background_gradient: style.getAttentionGradientStandart()
                entered_gradient: style.getAttentionGradientEntered()
                width: 100
                onMouseAreaClicked: {guide.visible = false; guide_dialog.close()}
            }
            OtherModules.StylishButton {
                id: ok_button
                anchors.bottom: parent.bottom
                anchors.right: cancel_button.left
                anchors.bottomMargin: 10
                anchors.rightMargin: 20
                button_name: "OK"
                background_gradient: style.getGradientStandart()
                entered_gradient: style.getGradientEntered()
                width: 100
                onMouseAreaClicked: {
                    guide.visible = true
                    settings.setPromptMode(true)
                    genWindow.setPromptModeInSettings(true)
                    genWindow.changeGuideVisible()
                    guide_dialog.close()
                }
            }
        }
    }

    /** @brief Флаг успешного ознакомления с подсказками окна выбора персонажа*/
    property bool image_selection_flag: false
    /** @brief Флаг успешного ознакомления с подсказками окна команд*/
    property bool comand_window_flag: false
    /** @brief Флаг успешного ознакомления с подсказками окна напоминаний*/
    property bool reminder_window_flag: false
    /** @brief Флаг успешного ознакомления с подсказками окна подсказок*/
    property bool note_window_flag: false
    /** @brief Флаг успешного ознакомления с подсказками меню*/
    property bool menu_flag: false

    /*onActiveChanged: {
        if (image_selection_flag && comand_window_flag && reminder_window_flag && note_window_flag && menu_flag){
            guide.prompt_text = qsTr("Congratulations! You have completed the program guide")
            guide.anchors.top = undefined
            guide.anchors.left = undefined
            guide.anchors.horizontalCenter = guide.parent.horizontalCenter
            guide.anchors.verticalCenter = guide.parent.verticalCenter
            closeGuideTimer.start()
        }
    }

    Timer{
        id: closeGuideTimer
        interval: 5000
        repeat: false
        running: false
        onTriggered: {
            guide.visible = false
        }
    }*/

    OtherModules.GuideRectangle{
        id: guide
        anchors.verticalCenter: parent.verticalCenter
        z:2
        visible: settings.getPromptMode()
        promptText: qsTr("Press the right button on the character and choose menu item. If you created a voice command, then to enter it, press Ctrl+Alt+S") + settings.emptyString
    }



    /* Иконка в трее */
    SystemTrayIcon {
        id: tray
        visible: false
        icon.source: "/resources/img/trayicon.png"

        onActivated: {
            console.debug(reason)
            if ((reason === 3) || (reason === 1))
            {
                menu1.open()

            }
            else if ((reason === 2)){
                mainWindow.showWindow()

                mainWindow.raise()

                menu1.close()

                tray.visible = false

                mainWindow.flags += Qt.Window
            }
        }

        menu: Menu {
            id: menu1
            font.pointSize: 10
            visible: false
            onVisibleChanged: {
                console.debug(menu1.visible)
            }

            /*MenuItem {
                text: qsTr("Choose character") + settings.emptyString
                onTriggered: {
                    // передаем сигнал окну выбора персонажей о том, что передаем ему управление
                    visibleGeneralWindow = true
                    showImageSelectWindow()

                    // ставим окну выбора персонажей флаг видимости true
                    //visibleImageSelectWindow = true
                    //loader.active = true // нужно, чтобы снова загрузить окно

                    // вставляем наш компонент в Loader и если active == true начинается загрузка компонента
                    //loader.sourceComponent = imageSelectWindowComponent
                    //selectWindow.visible = true
                    //imageSelectionMenuLoad.source = "ImageSelectionMenu.qml"//selectWindow.show()
                }
            }*/
            MenuItem {
                text: qsTr("Add a commands") + settings.emptyString
                onTriggered: {

                    // передаем сигнал окну выбора персонажей о том, что передаем ему управление
                    visibleGeneralWindow = true
                    showCommandWindow()

                    // ставим окну выбора персонажей флаг видимости true
                    //visibleCommandWindow = true
                    //commandWindowLoader.active = true // нужно, чтобы снова загрузить окно

                    // вставляем наш компонент в Loader и если active == true начинается загрузка компонента
                    //commandWindowLoader.sourceComponent = commandWindowComponent
                    //selectWindow.visible = true
                    //imageSelectionMenuLoad.source = "ImageSelectionMenu.qml"//selectWindow.show()
                }
            }
            MenuItem {
                text: qsTr("Create a reminder") + settings.emptyString
                onTriggered: {
                    visibleGeneralWindow = true
                    showReminderWindow()
                    //visibleReminderWindow = true
                    //reminderWindowLoader.sourceComponent = reminderWindowComponent

                }
            }
            MenuItem {
                text: qsTr("Add a node") + settings.emptyString
                onTriggered: {
                    visibleGeneralWindow = true
                    showNodesWindow()
                    //visibleReminderWindow = true
                    //reminderWindowLoader.sourceComponent = reminderWindowComponent

                }
            }
            /*MenuItem {
                text: qsTr("Settings") + settings.emptyString
                onTriggered: {
                    visibleGeneralWindow = true
                    showSettingsWindow()
                    //visibleSettingsWindow = true
                    //settingsWindowLoader.sourceComponent = settingsWindowComponent
                }
            }*/
            MenuItem {
                text: qsTr("Maximize window") + settings.emptyString
                onTriggered: {
                    mainWindow.showWindow()
                    tray.visible = false
                    mainWindow.flags = Qt.Window + Qt.FramelessWindowHint
                }
            }

            MenuItem {
                text: qsTr("Close") + settings.emptyString
                onTriggered: {
                    //mainWindow.shouldClose = true
                    tray.visible = false
                    mainWindow.hideWindow()
                    mainWindow.visible = true
                    mainWindow.close()
                }
            }
        }

    }

    /* onClosing: {

              console.debug(settings.getTrayMode(),!shouldClose)
              if (settings.getTrayMode() && !shouldClose)
              {
                  close.accepted = false
                  mainWindow.visible = false
                  tray.visible = true
              }
              else
              {
                  close.accepted = true
              }


          }*/

    /** @brief Текущее время */
    property string current_time: Qt.formatTime(new Date(), "hh:mm")

    /** @brief Текущая дата */
    property string current_date: Qt.formatDate(new Date(), "yyyy.MM.dd")

    Timer{
        id: load_timer
        interval: 3000
        repeat: false
        running: true
        onTriggered: {
            console.log(new Date().getSeconds())
            var msg = db.getNotViewedNotifications(current_date + ' ' + current_time)
            if (msg.length > 0)
            {
                var i = 0
                for (; i < msg.length;)
                {
                    if (mainWindow.width !== 0){
                        reminders_dialog.addingNotification(msg[i],
                                                            msg[i + 1],
                                                            msg[i + 2],
                                                            msg[i + 3])
                        console.log(msg[i])
                        console.log(msg[i + 1])
                        console.log(msg[i + 2])
                        console.log(msg[i + 3])

                        reminders_dialog.visible = true
                        //reminders_dialog.flags += Qt.WindowStaysOnTopHint
                        notification.play()
                    }
                    else {
                        tray.showMessage(msg[i], msg[i + 1] + " " + msg[i + 2] + "\n" + msg[i + 3], 1, 100)
                        notification.play()
                    }
                    //db.updateViewed(msg[i + 5])
                    i += 4
                }
                reminderWindowChanged()
            }

        }
    }

    onTriggerNotifTimer: {
        current_time = Qt.formatTime(new Date(), "hh:mm")
        if (current_time == "00:00")
        {
            current_date = Qt.formatDate(new Date(), "yyyy.MM.dd")
        }

        var j = 0
        var messages = db.getReminderMessage(current_date,current_time)

        console.log(messages)

        if (messages.length > 0)
        {
            while (j < messages.length)
            {
                // Выполнение команд
                console.log(messages[j+4].length + " !")
                for (var i = 0; i < messages[j+4].length; i++){
                    var actionList = db.findCommand({commandName : messages[j+4][i]}, "action")
                    console.log(messages[j+4][i] + " !!")
                    console.log(actionList + "  !!!");

                    for (var k in actionList)
                    {
                        stt.evecuteAction(actionList[k])
                    }
                }

                //Отображение напоминания
                if (mainWindow.visible == true){
                    reminders_dialog.addingNotification(messages[j+1],
                                                        messages[j+2],
                                                        current_date,
                                                        current_time)
                    console.log(messages[j+1])
                    console.log(messages[j+2])

                    reminders_dialog.visible = true
                    //reminders_dialog.requestActivate()
                    //mainWindow.flags += Qt.WindowStaysOnTopHint

                    //context_menu.flags +=  Qt.WindowStaysOnTopHint


                    //reminders_dialog.flags -= Qt.WindowStaysOnTopHint
                    //mainWindow.flags -= Qt.WindowStaysOnTopHint
                    //context_menu.flags -=  Qt.WindowStaysOnTopHint
                    notification.play()

                }
                else {
                    tray.showMessage(messages[j+1], messages[j+2], 1, 100)

                    if (!stt.isWin())
                    {
                        push_notification.title = messages[j+1]
                        push_notification.err_name =  messages[j+2]
                        push_notification.open()
                    }
                    notification.play()
                }
                //db.updateViewed(current_date + " " + current_time)
                //repetition = db.getReminderRepeat(current_date + ' ' + current_time)[i]
                //if (messages[j+2] !== 0){
                // db.doRepetition(messages[j+2], current_date + ' ' + current_time,messages[j+3])
                //}
                j += 5

            }
            reminderWindowChanged()
        }
        console.log(current_time)
        console.log(current_date)
    }


    Timer{
        id: first_timer
        interval: 60000 - 1000 * new Date().getSeconds()
        repeat: false
        running: true
        onTriggered: {
            triggerNotifTimer()

            timer.interval = 60000 - 1000 * new Date().getSeconds()
            timer.start()
        }
    }

    // property string date
    Timer{
        id: timer
        interval: 60000 - 1000 * new Date().getSeconds()
        repeat: false
        running: false
        onTriggered: {
            triggerNotifTimer()

            first_timer.interval = 60000 - 1000 * new Date().getSeconds()
            first_timer.start()
        }
    }

    OtherModules.ErrorDialog {
        id: push_notification
        visible: false
    }


    /** @brief функция обновления сочетаний клавиш после изменений в БД*/
    function updShortcut()
    {
        stt.setGlobalShortcuts(db.receiveShortcuts())
    }

    // соединение с stt
    Connections {
        target: stt
        // когда сочетание клавиш активировано и пришел об этом сигнал
        // то ищем команды, которые выполняются при таком нажатии и выполняем их
        onProcessGlobalShortcut: {
            //console.debug(stt.getGlobalShortcut())
            var actionList = db.findCommand({KeysORVoice : stt.getGlobalShortcut(), isVoiceInput : false}, "action")
            for (var i in actionList)
            {
                //console.debug(actionList[i])
                stt.evecuteAction(actionList[i])
            }
        }
        // когда опред. сочетание клавиш активировано, приходит сигнал об этом
        // и начинается / заканчивается запись голоса
        onChangeRecording: {

            if (stt.isInternetAccess())
            {
                error.visible = false
                //if (!stt.getRunning())
                //{
                click.visible = true
                clickImage.running = true
                stt.toggleRecord()
                //if (!stt.getEvenClick())
                //    recordtime.running = true
                dialogWindow.show()
                mainWindow.requestActivate()
                //}
            }
            else
            {
                error.err_name = qsTr("No internet connection") + settings.emptyString
                error.log = "No internet"
                error.visible = true
            }
        }
        // если запись голоса не получилось выполнить = выводим ошибку
        onRecordError: {
            error.err_name = stt.getErrorText()
            error.log = error.err_name
            error.visible = true

        }
        // если вызвали окно сочетанием клавиш - выводим окно персонажа
        onShowApp: {
            tray.visible = false
            mainWindow.showWindow()
            mainWindow.flags += Qt.WindowStaysOnTopHint
            mainWindow.flags -= Qt.WindowStaysOnTopHint

            console.debug("!")
        }
        onPostError: {
            error.err_name = stt.getErrorText()
            error.log = error.err_name
            error.visible = true
        }
        onShowFastCommands: {
            fastCommandsWindow.showCommandsList()
        }
    }


    // таймер - для того, чтобы запись голоса длилась не более 10 секунД
    /*Timer {
        id: recordtime
        interval: 10000
        repeat: false
        running: false
        onTriggered: {
            // таймер работает единажды
            recordtime.running = false
            // если запись еще идет, останавливаем ее
            if (!stt.getEvenClick())
                stt.toggleRecord();
        }
    }*/

    // диалог ошибок - универсальный
    OtherModules.ErrorDialog {
        id: error
        onlyMessage: false
    }

    // таймер показа индикатора нажатия - 1 сек
    Timer {
        id: clickImage
        interval: 1000
        repeat: false
        running: false
        onTriggered: {
            click.visible = false
            clickImage.running = false
        }
    }

    // gif как индикатор нажатия
    AnimatedImage {
        width: 150
        height: 150
        x:parent.x - 35
        y:parent.y - 45
        id: click
        clip: false
        visible: false
        source: "/resources/img/click1.gif"
    }

    /*
    //minimumHeight: 50
    //minimumWidth: 50
    //maximumHeight: Screen.height/2
    //maximumWidth: Screen.width/2*/
    MouseArea {
        id: plusSizeArea

        anchors.right: parent.right
        anchors.top: parent.top
        width: 15
        height: width

        z:3
        //cursorShape: Qt.SizeBDiagCursor
        onPressed: {
            previousX = mouseX
            previousY = mouseY
        }
        hoverEnabled: true
        onEntered: circlePlus.opacity = 1
        onExited: circlePlus.opacity = 0.3
        Rectangle {

            id: circlePlus
            anchors.fill: parent
            radius: 10
            color: "white"
            opacity: 0.3
            border.width: 1
            border.color: "black"
            Text {
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "black"
                text: "+"
            }
        }
        onClicked: {
            character.resize(true)
            //right_area_resize(previousX, mouseX)
            //top_area_resize(previousY, mouseY)
        }
    }
    MouseArea {
        id: minusSizeArea

        anchors.right: parent.right
        anchors.top: plusSizeArea.bottom
        anchors.topMargin: 4
        width: 15
        height: width

        z:3
        //cursorShape: Qt.SizeBDiagCursor
        onPressed: {
            previousX = mouseX
            previousY = mouseY
        }
        hoverEnabled: true
        onEntered: circleMinus.opacity = 1
        onExited: circleMinus.opacity = 0.3
        Rectangle {

            id: circleMinus
            anchors.fill: parent
            radius: 10
            color: "white"
            opacity: 0.3
            border.width: 1
            border.color: "black"
            Text {
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "black"
                text: "-"
            }
        }
        onClicked: {

            character.resize(false)
            //right_area_resize(previousX, mouseX)
            //top_area_resize(previousY, mouseY)
        }
    }


    // область обработки нажатий мыши
    MouseArea {
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }


        // при нажатии на область мыши убираем видимость у контекстого меню
        onPressed: {
            //console.debug(character.activeFocus)
            context_menu.visible = false
            previousX = mouseX
            previousY = mouseY

        }

        //
        onMouseXChanged: {
            var dx = mouseX - previousX
            mainWindow.setX(mainWindow.x + dx)
        }

        onMouseYChanged: {
            var dy = mouseY - previousY
            mainWindow.setY(mainWindow.y + dy)
        }

        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {

            if(mouse.button & Qt.RightButton) {

                if ( Qt.RightButton)
                {
                    context_menu.raise()
                    context_menu.visible = true
                    context_menu.x = mouse.x + mainWindow.x
                    context_menu.y = mouse.y + mainWindow.y
                }
                else
                    context_menu.visible = false
                //console.debug(mouse.x, mouse.y)
            }
        }
    }
    /** @brief Отношение длины и ширины(<=1)*/
    readonly property real proportion: {
        if (character.implicitHeight > character.implicitWidth)
            character.implicitWidth / character.implicitHeight
        else
            character.implicitHeight / character.implicitWidth
    }

    MainContextMenu {
        id: context_menu
        objectName: "contextMenu"
    }



    /** @brief обновляем персонажа при изменении изображения в окне изображений*/
    function updateImg(path)
    {
        character.source = path
        character.width = character.sourceSize.width
        character.height = character.sourceSize.height
        if (character.implicitHeight > character.implicitWidth)
        {
            if (character.height > Screen.height / 2)
            {
                character.height = Screen.height / 2
                character.width = character.height * proportion
            }
        }
        else
        {
            if (character.width > Screen.width / 4)
            {
                character.width = Screen.width / 4
                character.height = character.width * proportion
            }
        }
        character.playing = true
    }

    // элемент-изображение для основного окна
    AnimatedImage {
        id: character
        clip: false
        visible: true
        autoTransform: true
        source: db.getImageMainPath()
        playing: true
        onStatusChanged: {
            load_img_size.start()
        }

        /* SequentialAnimation on width {
            id: characterResizingAmination
            running: false
            loops: 1
            NumberAnimation {
                from: character.width
                to: character.width + 100*(slider.value - prevValue)
                duration: 100
            }
            onRunningChanged: {
                console.log(character.width,character.width + 100*(slider.value - prevValue))
            }
        }*/

        function resize(plus) {

                var dSize = (plus) ? 50 : -50
                var extHeight = (plus) ? Screen.height : -70
                var extWidth = (plus) ? Screen.width : -100
                var mark = (plus) ? 1 : -1
                if (character.height < character.width)
                {
                    if (mark*(character.width + dSize) < extWidth &&
                            mark*(character.width + dSize)*proportion < extHeight)
                    {
                        character.width += dSize
                        character.height = character.width * proportion
                    }
                } else {
                    if (mark*(character.width + dSize) < extWidth &&
                            mark*(character.width + dSize)/proportion < extHeight)
                    {
                        character.width += dSize
                        character.height = character.width / proportion
                    }
                }

        }

        /*onWidthChanged: {
            if (character.height < character.width)
            {
                character.height = character.width * proportion
            } else {
                character.height = character.width / proportion
            }
        }*/
    }

    //Таймер для того, чтобы дать картинке или gif прогрузиться
    Timer{
        id: load_img_size
        interval: 1
        repeat: false
        running: false
        onTriggered: {
            updateImg(character.source)
            mainWindow.x = Screen.width / 2 - width / 2
            mainWindow.y = Screen.height / 2 - height / 2
        }
    }

    /** @brief Флаг видимости окна основного окна */
    property var visibleGeneralWindow: false
    GeneralWindowModules.GeneralWindow {
        id: genWindow
        visible: visibleGeneralWindow
        onShowPromptChanged: {
            guide.visible = settings.getPromptMode()
        }
    }


    // диалог для общения персонажа с пользователем
    DialogWindow {
        id: dialogWindow
        x: mainWindow.x + mainWindow.width / 2 - width / 2
        y: mainWindow.y - height - 5

        MouseArea {
            onMouseXChanged: {
                var dx = mouseX - previousX
                mainWindow.setX(mainWindow.x + dx)
            }
            onMouseYChanged: {
                var dy = mouseY - previousY
                mainWindow.setY(mainWindow.y + dy)
            }
        }
    }

    // диалог для вывода уведомлений


    RemindersWindowModules.RemindersDialog {
        id: reminders_dialog
        x: Screen.width - width
        y: Screen.height - height

        opacity: 0

        visible: false

        SequentialAnimation on opacity {
            id: opacityAnimation
            running: false
            loops: 1
            PropertyAnimation { to: 1; duration: 200 }
        }

        onVisibleChanged: {
            opacityAnimation.start()
        }

    }


    Audio{
        id: notification
        source: "/resources/sounds/notification_sound.mp3"
        volume: 0.7
    }

    StyleModules.Style{
        id: style
    }

}
