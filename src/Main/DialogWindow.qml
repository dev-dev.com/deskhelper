/**
  * @file
  * @brief Вспомогательное окно для отображения ответа персонажа
  *
  * Диалог в форме прямоугольника с загнутым кончиком для имитации того, что персонаж "общается"
  * Выдает обработанные сообщения в виде текста внутри эллипса
*/
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

/*!
    \class DialogWindow
    \brief Диалог в форме прямоугольника с загнутым кончиком для имитации того, что персонаж "общается"
 */
Window {
    id: dialogWindow

    width: Screen.width / 8.6
    height: Screen.height / 10.2

    color: "transparent"
    flags: Qt.FramelessWindowHint

    Timer {
        id: clickImage
        interval: 1000
        repeat: false
        running: false
        onTriggered: {
            click.visible = false
            clickImage.running = false
        }
    }

    Rectangle {
        Text {
            id: textField
            //anchors.horizontalCenter: parent.horizontalCenter
            //anchors.verticalCenter: parent.verticalCenter
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            visible: true
            text: qsTr("Speak") + settings.emptyString
            //width: parent.width
            font.pixelSize: 15
            clip: true
            wrapMode: Text.WordWrap
            //horizontalAlignment: Text.AlignHCenter
            //Component.onCompleted: input.ensureVisible(0)
        }

        AnimatedImage {
            id: loading
            //anchors.horizontalCenter: parent.horizontalCenter/3
            anchors.verticalCenter: parent.verticalCenter
            anchors.fill: parent
            anchors.topMargin: parent.height/4
            anchors.bottomMargin: parent.height/4
            anchors.leftMargin: parent.width/3
            anchors.rightMargin: parent.width/3
            clip: true
            visible: false
            source: "/resources/img/wait.gif"
        }

        id: diallog
        anchors{
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        clip: true
        visible: true
        border.color: "purple"
        border.width: 5
        gradient: Gradient{
            id: backgroundGradient
            GradientStop {
                position: 0
                color: "#3C4653"

                SequentialAnimation on color {
                    id: colorAnimation
                    running: true
                    loops: Animation.Infinite
                    ColorAnimation { to: "#493DA0" }
                    ColorAnimation { to: "#5C4653"; duration: 1000 }
                    ColorAnimation { to: "#773143"; duration: 1000 }
                    ColorAnimation { to: "#771E36"; duration: 1000 }
                    ColorAnimation { to: "#771E4D"; duration: 1000 }
                    ColorAnimation { to: "#770140"; duration: 1000 }
                    ColorAnimation { to: "#771E4D"; duration: 1000 }
                    ColorAnimation { to: "#771E36"; duration: 1000 }
                    ColorAnimation { to: "#773143"; duration: 1000 }
                    ColorAnimation { to: "#5C4653"; duration: 1000 }
                    ColorAnimation { to: "#493DA0"}
                }

            }
            GradientStop {
                position: 1
                color: "#30313A"
                SequentialAnimation on color {
                    id: colorAnimation2
                    running: true
                    loops: Animation.Infinite
                    ColorAnimation { to: "#30123A"}
                    ColorAnimation { to: "#301262"; duration: 1000 }
                    ColorAnimation { to: "#22264A"; duration: 1000 }
                    ColorAnimation { to: "#1B2050"; duration: 1000 }
                    ColorAnimation { to: "#4A2050"; duration: 1000 }
                    ColorAnimation { to: "#712050"; duration: 1000 }
                    ColorAnimation { to: "#4A2050"; duration: 1000 }
                    ColorAnimation { to: "#1B2050"; duration: 1000 }
                    ColorAnimation { to: "#22264A"; duration: 1000 }
                    ColorAnimation { to: "#301262"; duration: 1000 }
                    ColorAnimation { to: "#30123A";}
                }

            }
        }
        radius: 10

    }

    AnimatedImage {
        width: parent.width
        height: 150
        x:parent.x + 70
        y:parent.y + 60
        id: click
        clip: false
        visible: false
        source: "/resources/img/click1.gif"
    }

    Connections {
        target: stt // Указываем целевой объект для соединения
        /* Объявляем и реализуем функцию, как параметр
         * объекта и с имененем похожим на название сигнала
         * Разница в том, что добавляем в начале on и далее пишем
         * с заглавной буквы
         * */
        onSecondClick: {
            loading.visible = true
            textField.text = ""
        }

        onSendTexttoMainClass: {
            loading.visible = false
            //textField.font.pixelSize = 14
            //textField.text = "Выполняю" // Вставляем обработанные данные в текстовый лейбл
            var actionList = db.findCommandWithLevenshteinDistance(stt.getRezult())
            //console.debug(actionList, emptyList, actionList===emptyList)
            //if (actionList === emptyList)
           // {
                textField.text = qsTr("Don't understand command") + settings.emptyString
          //  }
          //  else
          //  {
                for (var i in actionList)
                {
                    //console.debug(actionList[i])
                    stt.evecuteAction(actionList[i])
                    textField.text = qsTr("Execute..") + settings.emptyString
                }

         //   }



            textField.visible = true
            time.running = true
        }//
    }

    Timer {
        id: time
        interval: 3000
        repeat: false
        running: false
        onTriggered: {
            textField.text = qsTr("Speak") + settings.emptyString
            textField.font.pixelSize = 20
            loading.visible = false
            stt.setRunning(false)
            time.running = false
            dialogWindow.close()      
        }
    }
}
