/**
  * @file
  * @brief Конкекстное меню в виде окна
  *
  * Позволяет открыть любую страницу
*/
import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

/*!
    \class MainContextMenu
    \brief Конкекстное меню в виде окна
 */
Window {

    id: context_menu_window
    width: context_menu.width
    height: context_menu.height
    opacity: 1
    flags: Qt.FramelessWindowHint //+ Qt.WindowStaysOnTopHint
    color: "transparent"
    onVisibleChanged: {
        if (context_menu_window.visible)
            context_menu.open()
    }

   x: context_menu.x
   y: context_menu.y

    Menu {

        id: context_menu
        font.pointSize: 10

       /* background: Rectangle {
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#777EC4"
                }
                GradientStop {
                    position: 1
                    color: "#7652AC"
                }
            }
            implicitWidth: 200
            implicitHeight: 50
            opacity: enabled ? 1 : 0.3
            border.color: "white"
            border.width: 3
            radius: 15
        }*/

        /*MenuItem {
            text: qsTr("Choose character") + settings.emptyString
            onClicked: {

                context_menu_window.close()

                // передаем сигнал окну выбора персонажей о том, что передаем ему управление
                //showImageSelectWindow()

                visibleGeneralWindow = true
                showImageSelectWindow()
                // ставим окну выбора персонажей флаг видимости true
                //visibleImageSelectWindow = true

                //loader.active = true // нужно, чтобы снова загрузить окно

                // вставляем наш компонент в Loader и если active == true начинается загрузка компонента
                //loader.sourceComponent = imageSelectWindowComponent
                //selectWindow.visible = true
                //imageSelectionMenuLoad.source = "ImageSelectionMenu.qml"//selectWindow.show()

            }
        }*/
        MenuItem {
            text: qsTr("Add a commands") + settings.emptyString
            onClicked: {

                context_menu_window.close()
                // передаем сигнал окну выбора персонажей о том, что передаем ему управление
                visibleGeneralWindow = true
                showCommandWindow()

                // ставим окну выбора персонажей флаг видимости true
                //visibleCommandWindow = true
                //commandWindowLoader.active = true // нужно, чтобы снова загрузить окно

                // вставляем наш компонент в Loader и если active == true начинается загрузка компонента
                //commandWindowLoader.sourceComponent = commandWindowComponent
                //selectWindow.visible = true
                //imageSelectionMenuLoad.source = "ImageSelectionMenu.qml"//selectWindow.show()

            }
        }
        MenuItem {
            text: qsTr("Create a reminder") + settings.emptyString
            onTriggered: {
                context_menu_window.close()

                visibleGeneralWindow = true
                showReminderWindow()
                //visibleReminderWindow = true
                //reminderWindowLoader.sourceComponent = reminderWindowComponent

            }
        }
        MenuItem {
            text: qsTr("Add a node") + settings.emptyString
            onTriggered: {
                context_menu_window.close()

                visibleGeneralWindow = true
                showNodesWindow()
                //visibleReminderWindow = true
                //reminderWindowLoader.sourceComponent = reminderWindowComponent

            }
        }
        /*MenuItem {
            text: qsTr("Settings") + settings.emptyString
            onTriggered: {
                context_menu_window.close()

                visibleGeneralWindow = true
                showSettingsWindow()
                //visibleSettingsWindow = true
                //settingsWindowLoader.sourceComponent = settingsWindowComponent
            }

        }*/
        MenuItem {
            id:mtt
            text: qsTr("Minimize to tray") + settings.emptyString
            onTriggered: {
                context_menu_window.close()

                mainWindow.hideWindow()
                tray.visible = true
                //mainWindow.flags -= Qt.Window
            }
            //visible: settings.getTrayMode()

            enabled: settings.getTrayMode() ? true : false

            //height: close.height
        }

        MenuItem {
            id: close
            text: qsTr("Exit") + settings.emptyString
            onTriggered: {
                //if (!settings.getTrayMode())
                    mainWindow.close()
                /*else
                {
                    mainWindow.hideWindow()
                    tray.visible = true
                }*/
            }

        }

        Connections {
            target: settings

            onTrayModeChanged: {
                mtt.enabled = settings.getTrayMode()
                /*var newHeight = (settings.getTrayMode()) ? close.height : 0
                if (mtt.height !== newHeight)
                {
                    mtt.height = newHeight
                    context_menu_window.height += (settings.getTrayMode()) ? close.height : -close.height
                    context_menu_window.update()
                }*/
            }
        }
    }
}
