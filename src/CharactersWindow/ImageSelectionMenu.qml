/**
  * @file
  * @brief Страница для манипуляций над персонажем
  *
  * Страница, на которой можно выбрать/удалить/добавить персонажа
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.3
import "../Other" as OtherModules
import "../Style" as StyleModules

/*!
    \class ImageSelectionMenu
    \brief Страница, на которой можно выбрать/удалить/добавить персонажа
 */
Rectangle {
    id: selectWindow

    /** @brief Название таблицы в БД */
    property string table: "imagesList"


    /** @brief Изменение видимости элементов добавления и элементов выбора действий*/
    function changeAddingVisible() {
        tag.state = (tag.state === "hide") ? "show" : "hide"
        pathText.state = (pathText.state === "hide") ? "show" : "hide"
        rowInputCenterBtns.state = (rowInputCenterBtns.state === "hide") ? "show" : "hide"
        rowBtns.state = (rowBtns.state === "hide") ? "show" : "hide"
    }
    color: "transparent"
    /*LinearGradient {
        id: lg
        //z:0
        anchors.fill: parent
        //anchors.top: parent.top
        // anchors.topMargin: -30
        start: Qt.point(0,0)
        end: Qt.point(parent.width, parent.height)

        gradient: style.getBackgroundGradient()

    }*/

    z: 5

    Column {
        id: scrcol
        width: parent.width
        spacing: 30
        anchors.fill: parent
        anchors.topMargin: 20


        // строка поиска посередине
        TextField {
            id: search
            objectName: "search"
            //  z:1
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * 2 / 3

            placeholderText: qsTr("Search by file name") + settings.emptyString
            placeholderTextColor: style.getPlaceholderTextColor()
            color: style.getTextColor()
            background: Rectangle {
                implicitWidth: parent.width
                implicitHeight: parent.height
                color: "transparent"
                border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                border.width: 3
                radius: 5
            }
            state: "horCenter"
            onTextChanged: {

                charactersTable.findByName(text)

            }

            states: State {
                name: "anchorLeft"
                AnchorChanges {
                    target: search
                    anchors.horizontalCenter: undefined
                    anchors.left: parent.left

                }
                PropertyChanges {
                    target: search
                    anchors.leftMargin: 20
                }

            }
            State {
                name: "horCenter"
                AnchorChanges {
                    target: search
                    anchors.left: undefined
                    anchors.horizontalCenter: lg.horizontalCenter
                }
            }
            transitions: Transition {
                // smoothly reanchor myRect and move into new position
                AnchorAnimation { duration: 500 }
            }
        }




        CharactersTable {
            id: charactersTable
            objectName: "charactersTable"
            anchors.top: search.bottom
            //anchors.bottom: parent.bottom
            //anchors.left: parent.left
            //anchors.right: add_button.left
            anchors.topMargin: 20
            //  anchors.leftMargin: 50
            anchors.horizontalCenter: search.horizontalCenter
            width: parent.width / 9 * 8
            height: selectWindow.height * 4 / 7
            textColor: style.getTextColor()
            // anchors.bottomMargin: 40
            // anchors.rightMargin: 50

        }




        FileDialog {
            id: fileDialog
            visible: false
            title: qsTr("Select a file") + settings.emptyString
            nameFilters: [ "Image files (*.jpg *.png *.svg *.gif)", "All files (*)" ]
            folder: shortcuts.home
            onAccepted: {
                console.log("You chose: " + fileDialog.fileUrl)
                path.text = fileDialog.fileUrl
            }
            onRejected: {
                console.log("Canceled")
            }
        }


        Loader {
            id: errorLoader
        }


        Component {
            id: errorComponent
            OtherModules.ErrorDialog {
                id: error
                visible: visibleErrorDialog
                log: log
                err_name: errName
            }
        }
    }
    Rectangle {
        id: charactersManagesRectangle
        color: "transparent"
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        property int marginBottom: 0
        anchors.bottomMargin: -12
        height: 60
        Row {
            id:rowBtns
            objectName: "rowBtns"
            state: "show"
            states: State {
                name: "hide"
                AnchorChanges {
                    target: rowBtns
                    anchors.bottom: undefined
                    anchors.top: parent.bottom
                }
            } State {
                name: "show"
                AnchorChanges {
                    target: rowBtns
                    anchors.top: undefined
                    anchors.bottom: parent.bottom
                }
                PropertyChanges {
                    target: rowBtns
                    //anchors.bottomMargin: marginBottom
                }

            }
            transitions: Transition {
                AnchorAnimation { duration: 500 }
            }

            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 5
            OtherModules.StylishButton {
                id: new_button

                image_src: selectWindow.width < 100 + generalWindow.minimumWidth ? "/resources/img/add.png" : ""

                width: charactersTable.width / 10
                Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                button_name: selectWindow.width >= 100 + generalWindow.minimumWidth ? qsTr("New") + settings.emptyString : ""
                background_gradient: style.getGradientStandart()
                entered_gradient: style.getGradientEntered()

                onMouseAreaClicked: {
                    changeAddingVisible()

                    if (settings.getPromptMode()) {
                        for (var i = 0; i < repeater.count; i++){

                            if (i >= 3)
                                repeater.itemAt(i).doUpdate(getImgVisible(i))
                            else {
                                repeater.itemAt(i).doUpdate(false)
                            }
                        }
                    }


                }
            }

            OtherModules.StylishButton {
                id: delete_button

                objectName: "deleteButton"

                image_src: selectWindow.width < 100 + generalWindow.minimumWidth ? "/resources/img/delete.png" : ""

                width: new_button.width
                Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }

                button_name: selectWindow.width >= 100 + generalWindow.minimumWidth ? qsTr("Remove") + settings.emptyString : ""
                background_gradient: style.getGradientStandart()
                entered_gradient: style.getGradientEntered()

                onMouseAreaClicked: {

                    if (db.removeTuple({ tag : charactersTable.getImageTag()},table))
                        charactersTable.removeImage()
                    else
                    {
                        //console.debug("Ошибка удаления строки из БД")
                        visibleErrorDialog = true
                        log = qsTr("Error deleting database row") + settings.emptyString
                        errName = qsTr("Error deleting database row: ") + settings.emptyString + db.getError()
                        errorLoader.sourceComponent = errorComponent
                        //error.log = "Ошибка удаления строки из базы данных"
                        //error.err_name = "Ошибка удаления строки из базы данных: " + db.getError();
                        //error.open()
                    }
                }
            }

            OtherModules.StylishButton {
                id: assign_button

                image_src: selectWindow.width < 100 + generalWindow.minimumWidth ? "/resources/img/make_main.png" : ""

                width: new_button.width
                Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }


                button_name: selectWindow.width >= 100 + generalWindow.minimumWidth ? qsTr("Make main") + settings.emptyString : ""
                background_gradient: style.getGradientStandart()
                entered_gradient: style.getGradientEntered()

                onMouseAreaClicked: {

                    if (db.makeImageMain(charactersTable.getImageTag()))
                        charactersTable.setGrad()

                    else
                    {
                        //console.debug("Ошибка изменения текущего персонажа в БД")
                        visibleErrorDialog = true
                        log = qsTr("Error changing current character in database") + settings.emptyString
                        errName = qsTr("Error changing current character in database: ") + settings.emptyString + db.getError()
                        errorLoader.sourceComponent = errorComponent
                        // error.log = "Ошибка изменения текущего персонажа в базе данных"
                        // error.err_name = "Ошибка изменения текущего персонажа в базе данных: " + db.getError();
                        // error.open()
                    }
                }

            }

        }


        TextField {
            id: tag

            objectName: "tag"
            anchors.right: rowInputCenterBtns.left
            anchors.rightMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 25
            width: charactersTable.width / 3
            height: rowBtns.height

            state: "hide"
            states: State {
                name: "hide"
                AnchorChanges {
                    target: tag
                    //  anchors.right: undefined
                    anchors.bottom: undefined
                    anchors.top: parent.bottom
                }
            } State {
                name: "show"
                AnchorChanges {
                    target: tag
                    //      anchors.right: rowBtns.left
                    anchors.top:  undefined
                    anchors.bottom: parent.bottom
                }
                PropertyChanges {
                    target: tag
                    //      anchors.rightMargin: 5
                    anchors.bottomMargin: marginBottom
                }

            }
            transitions: Transition {
                AnchorAnimation { duration: 500 }
            }

            placeholderText: qsTr("Enter tag") + settings.emptyString
            placeholderTextColor: style.getPlaceholderTextColor()
            color: style.getTextColor()

            background: Rectangle {
                implicitWidth: parent.width
                implicitHeight: parent.height
                color: "transparent"
                border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                border.width: 3
                radius: 5
            }
        }

        Row {
            id: rowInputCenterBtns
            objectName: "rowInputCenterBtns"
            //anchors.top: rowBtns.top
            anchors.horizontalCenter: rowBtns.horizontalCenter

            anchors.bottom: parent.bottom
            anchors.bottomMargin: 25
            //width: charactersTable.width / 14
            height: rowBtns.height

            state: "hide"
            states: State {
                name: "hide"
                AnchorChanges {
                    target: rowInputCenterBtns
                    //anchors.left: undefined
                    anchors.bottom: undefined
                    anchors.top: parent.bottom
                }
            } State {
                name: "show"
                AnchorChanges {
                    target: rowInputCenterBtns
                    // anchors.right: rowBtns.left
                    anchors.top:  undefined
                    anchors.bottom: parent.bottom
                }
                PropertyChanges {
                    target: rowInputCenterBtns
                    // anchors.rightMargin: 5
                    anchors.bottomMargin: marginBottom
                }

            }
            transitions: Transition {
                AnchorAnimation { duration: 500 }
            }
            spacing: 5
            OtherModules.StylishButton {
                id: add_button
                objectName: "addButton"

                //image_src: "/resources/img/add.png"

                width: charactersTable.width / 8
                Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                button_name: qsTr("Add") + settings.emptyString
                background_gradient: style.getGradientStandart()
                entered_gradient: style.getGradientEntered()

                onMouseAreaClicked: {

                    if ((tag.text === "") || (path.text === ""))
                    {
                        visibleErrorDialog = true
                        log = qsTr("Enter tag and path!") + settings.emptyString
                        errName = qsTr("Enter tag and path!") + settings.emptyString
                        errorLoader.sourceComponent = errorComponent
                        //error.log = "Введите тег и путь!";
                        //error.err_name = "Введите тег и путь!";
                        //error.open();
                    }
                    else
                    {
                        if (db.addTuple([path.text,tag.text,false], table))
                            charactersTable.addingImage()

                        else
                        {
                            if (db.addTuple(path.text,tag.text))
                                charactersTable.addingImage()
                            else
                            {
                                //console.debug("Ошибка добавления строки в БД")
                                visibleErrorDialog = true
                                log = qsTr("Error adding database row") + settings.emptyString
                                errName = qsTr("Error adding database row: ") + settings.emptyString + db.getError()
                                errorLoader.sourceComponent = errorComponent
                                //error.log = "Ошибка добавления строки в базу данных";
                                //error.err_name = "Ошибка добавления строки в базу данных: " + db.getError();
                                //error.open();
                            }
                        }
                    }
                    changeAddingVisible()
                    charactersTable.highlightNewElement()
                    tag.text = ""
                    path.text = ""

                    if (settings.getPromptMode()) {
                        for (var i = 0; i < repeater.count; i++){
                            if (i < 3)
                                repeater.itemAt(i).doUpdate(getImgVisible(i))
                            else {
                                repeater.itemAt(i).doUpdate(false)
                            }
                        }
                    }

                    //onAddData()
                    //db.addTuple(path.text,tag.text)
                    //charactersTable.
                }
            }
            OtherModules.StylishButton {
                id: cancel_button
                objectName: "cancelButton"
                width: charactersTable.width / 8
                Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                button_name: qsTr("Cancel") + settings.emptyString
                background_gradient: style.getGrayGradientStandart()
                entered_gradient: style.getGrayGradientEntered()

                onMouseAreaClicked: {
                    changeAddingVisible()
                    tag.text = ""
                    path.text = ""

                    if (settings.getPromptMode()) {
                        for (var i = 0; i < repeater.count; i++){
                            if (i < 3)
                                repeater.itemAt(i).doUpdate(getImgVisible(i))
                            else {
                                repeater.itemAt(i).doUpdate(false)
                            }
                        }
                    }

                }
            }

        }

        Row {
            id: pathText
            objectName: "pathTextRow"

            anchors.left: rowInputCenterBtns.right
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 25
            height: rowBtns.height

            state: "hide"
            states: State {
                name: "hide"
                AnchorChanges {
                    target: pathText
                    //anchors.left: undefined
                    anchors.bottom: undefined
                    anchors.top: parent.bottom
                }
            } State {
                name: "show"
                AnchorChanges {
                    target: pathText
                    // anchors.right: rowBtns.left
                    anchors.top:  undefined
                    anchors.bottom: parent.bottom
                }
                PropertyChanges {
                    target: pathText
                    // anchors.rightMargin: 5
                    anchors.bottomMargin: marginBottom
                }

            }
            transitions: Transition {
                AnchorAnimation { duration: 500 }
            }
            //  width: 150
            //  height: rowBtns.height
            TextField {
                id: path
                objectName: "pathText"
                placeholderText: qsTr("Enter file path") + settings.emptyString
                placeholderTextColor: style.getPlaceholderTextColor()
                color: style.getTextColor()
                width: charactersTable.width / 27 * 7
                height: parent.height
                background: Rectangle {
                    implicitWidth: parent.width
                    implicitHeight: parent.height
                    color: "transparent"
                    border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                    border.width: 3
                    radius: 5
                }
            }
            OtherModules.StylishButton {
                id: openfile_button

                image_src: "/resources/img/file.png"

                anchors.left: path.right
                anchors.leftMargin: 5
                height: parent.height
                width: height
                //button_name: qsTr("O") + settings.emptyString
                background_gradient: style.getGradientStandart()
                entered_gradient: style.getGradientEntered()

                onMouseAreaClicked:{
                    fileDialog.visible = true
                }
            }
        }




        Repeater{
            id: repeater
            model: 8

            property bool guide_visible: false


            delegate: OtherModules.GuideRectangle{
                id: guide_ism
                onPromptClicked: {
                    setImgVisible(model.index)
                }
            }

            Component.onCompleted: {
                showPrompts()
            }

        }
    }

    StyleModules.Style {
        id: style
    }


    function showPrompts() {
        for (var i = 0; i < repeater.count; ++i){
            if (i === 0){
                repeater.itemAt(i).promptText = qsTr("Click to add image.") + settings.emptyString
                repeater.itemAt(i).parent = new_button
                repeater.itemAt(i).anchors.bottom = new_button.top
                repeater.itemAt(i).anchors.horizontalCenter = new_button.horizontalCenter
            }
            if (i === 1){
                repeater.itemAt(i).promptText = qsTr("Delete image from library") + settings.emptyString
                repeater.itemAt(i).parent = delete_button
                repeater.itemAt(i).anchors.bottom = delete_button.top
                repeater.itemAt(i).anchors.horizontalCenter = delete_button.horizontalCenter
            }
            if (i === 2){
                repeater.itemAt(i).promptText = qsTr("Setting the image on the main window") + settings.emptyString
                repeater.itemAt(i).parent = assign_button
                repeater.itemAt(i).anchors.bottom = assign_button.top
                repeater.itemAt(i).anchors.horizontalCenter = assign_button.horizontalCenter
            }
            if (i === 3){
                repeater.itemAt(i).promptText = qsTr("Enter the name of the image (the tag should not be repeated with the existing one) (required field)") + settings.emptyString
                repeater.itemAt(i).parent = tag
                repeater.itemAt(i).anchors.bottom = tag.top
                repeater.itemAt(i).anchors.horizontalCenter = tag.horizontalCenter
            }
            if (i === 4){
                repeater.itemAt(i).promptText = qsTr("File path input field (File may have extensions .png, .jpg, .svg, .gif) (required field)") + settings.emptyString
                repeater.itemAt(i).parent = path
                repeater.itemAt(i).anchors.bottom = path.top
                repeater.itemAt(i).anchors.horizontalCenter = path.horizontalCenter
            }
            if (i === 5){
                repeater.itemAt(i).promptText = qsTr("Click to select picture") + settings.emptyString
                repeater.itemAt(i).parent = openfile_button
                repeater.itemAt(i).anchors.bottom = openfile_button.top
                repeater.itemAt(i).anchors.horizontalCenter = openfile_button.horizontalCenter
            }
            if (i === 6){
                repeater.itemAt(i).promptText = qsTr("Click to add an image to the library") + settings.emptyString
                repeater.itemAt(i).parent = add_button
                repeater.itemAt(i).anchors.bottom = add_button.top
                repeater.itemAt(i).anchors.horizontalCenter = add_button.horizontalCenter
            }
            if (i === 7){
                repeater.itemAt(i).promptText = qsTr("Click to cancel upload") + settings.emptyString
                repeater.itemAt(i).parent = cancel_button
                repeater.itemAt(i).anchors.bottom = cancel_button.top
                repeater.itemAt(i).anchors.horizontalCenter = cancel_button.horizontalCenter

            }
        }
    }

    function changeGuideVisible() {
        for (var i = 0; i < repeater.count; ++i){
            if (settings.getPromptMode()) {
                if (i < 3) {
                    repeater.itemAt(i).doUpdate(getImgVisible(i))
                 } else {
                    repeater.itemAt(i).doUpdate(false)
                }
            } else {
                repeater.itemAt(i).doUpdate(false)
            }

        }
    }

    /** @brief Видимость окна ошибок */
    property var visibleErrorDialog : false
    /** @brief Лог ошибки*/
    property var log : ""
    /** @brief Текст ошибки*/
    property var errName: ""


    /** @brief Видимость подсказок*/
    property bool guide_visible: guide.visible

    /*Item
    {
        id: root
        property Window theWindow: Window.window
        Connections
        {
            target: root.theWindow
        }
    }*/

}

