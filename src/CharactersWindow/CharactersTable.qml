/**
  * @file
  * @brief Таблица персонажей
  *
  * Прямоугольник, внутри которого грид с возможностью выбора персонажа
*/

import QtQuick 2.0
import QtQuick.Controls 2.12

import "../Other" as OtherModules

/* Создаем таблицу выбора персонажа*/

/*!
    \class CharactersTable
    \brief Прямоугольник, внутри которого грид с возможностью выбора персонажа
 */
Rectangle {

    color: "transparent"

    /** @brief имя таблицы для запросов к БД */
    property string table: "imagesList"

    property string textColor
    /** @brief текущий индекс выбранного персонажа в гриде */
    property int currentMainImg: 0

    /** @brief добавление элемента к таблицу (грид) */
    function addingImage()
    {
        characterList.append({name: tag.text, portrait: path.text});
    }

    /** @brief удаление элемента из грида */
    function removeImage()
    {
        var index = characters_table.currentIndex

        if (currentMainImg > index)
            currentMainImg--
        else if (currentMainImg === index)
        {
            if (currentMainImg === 0)
            {
                if (characterList.count > 1)
                    characterList.setProperty(1, "isSelected", true)
            }
            else
            {
                currentMainImg--
                characterList.setProperty(currentMainImg, "isSelected", true)
            }
            db.makeImageMain(characterList.get(currentMainImg).name)
            mainWindow.updateImg(characterList.get(currentMainImg).portrait)
            //db.removeTuple(characterList.get(index).name)
        }

        characterList.remove(index)
    }

    /** @brief установка нового основного персонажа */
    function setGrad()
    {
        characterList.setProperty(currentMainImg, "isSelected", false)
        currentMainImg = characters_table.currentIndex
        characterList.setProperty(characters_table.currentIndex, "isSelected", true)
        mainWindow.updateImg(characterList.get(currentMainImg).portrait)
    }

    /** @brief получение тэга текущего персонажа в гирде */
    function getImageTag()
    {
        return characterList.get(characters_table.currentIndex).name;
    }

    /** @brief перенос текущего элемента на последний элемент грида */
    function highlightNewElement()
    {
        characters_table.currentIndex = characters_table.count - 1
    }

    /** @brief поиск по тэгу */
    function findByName(cn)
    {
        var list
        if (cn === "")
            list = db.selectAllTuples(3, table)
        else
            list = db.selectWithCond({tag : cn},3, table)

        characterList.clear()
        var i = 1
        if (list[0] > 1)
        {
            for (; i < list[0];) {
                characterList.append({name: list[i + 1],
                                      portrait: list[i],
                                      isSelected: (list[i+2] === "0") ? false : true})
                if (!(list[i+2] === "0"))
                {
                    currentMainImg = characterList.count - 1
                    console.debug(currentMainImg)
                }
                i = i + 3
            }
        }
    }

    // модель с данными для грида
    ListModel {

        id: characterList

        Component.onCompleted: {

            var list = db.selectAllTuples(3, table)

            var i = 1
            if (list[0] > 1)
            {
                for (; i < list[0];) {
                    append({name: list[i + 1], portrait: list[i], isSelected: (list[i+2] === "0") ? false : true})
                    if (!(list[i+2] === "0"))
                    {
                        currentMainImg = characterList.count - 1
                        console.debug(currentMainImg)
                    }
                    i = i + 3
                }
            }
        }
    }

    // компонент для отображения данных в гриде
    Component {
        id: characterComponentDelegate

        Column {

            width: characters_table.cellWidth
            height: (characters_table.currentIndex === model.index) ? characters_table.cellHeight :
                                                                      characters_table.cellHeight - 20

            // Behavior on height { NumberAnimation { duration: 500 } }
            //spacing: 15


            Rectangle {

                id: img_plase
                Behavior on height { SpringAnimation { spring: 3; damping: 0.2 } }

                //Behavior on height { NumberAnimation { duration: 500 } }
                states:[
                    State {
                        name: "BUTTON_ENTERED"
                        PropertyChanges { target: img_plase; gradient: (isSelected) ? style.getAttentionGradientEntered() : style.getGradientEntered();} //implicitWidth: 300; implicitHeight: 300}
                    },
                    State {
                        name: "BUTTON_EXITED"
                        PropertyChanges { target: img_plase; gradient: (isSelected) ? style.getAttentionGradientStandart() : style.getGradientStandart()}
                    }
                ]
                MouseArea {
                    anchors.fill:parent
                    anchors.top: parent.top
                    width:parent.width
                    height: parent.height

                    hoverEnabled: true
                    onEntered: img_plase.state = "BUTTON_ENTERED"
                    onExited:  img_plase.state = "BUTTON_EXITED"
                    onClicked: {

                        var pos = getAbsolutePosition(parent)
                        //  characters_table.flick(-1000,-1000)
                        var number = Math.floor((pos.x + 1 - (selectWindow.width / 2 - charactersTable.width / 2)) / characters_table.cellWidth)
                        console.debug(selectWindow.width / 2, charactersTable.width / 2,pos.x,characters_table.cellWidth)
                        var flick = (selectWindow.width > screen.width / 5 * 4) ? 1000 : 500
                        console.debug(flick)
                        if (number === 0)
                        {
                            characters_table.currentIndex = characters_table.indexAt(parent.parent.x,parent.parent.y)
                            characters_table.flick(flick,flick)
                        }
                        else if (number === 2)
                        {
                            characters_table.currentIndex = characters_table.indexAt(parent.parent.x,parent.parent.y)
                            characters_table.flick(-flick,-flick)
                        }
                        else if (number === 1)
                        {
                            characters_table.currentIndex = characters_table.indexAt(parent.parent.x,parent.parent.y)
                        }

                        // console.debug(parent.parent.x,parent.parent.y, characters_table.cellWidth)
                        //characters_table.currentIndex = characters_table.indexAt(pos.x + 1,pos.y)
                    }
                }

                gradient: (isSelected) ? style.getAttentionGradientStandart() : style.getGradientStandart()
                onColorChanged: {
                    gradient = style.getGradientStandart()
                }

                width: parent.width
                height: (characters_table.currentIndex === model.index) ? characters_table.cellHeight - 35:
                                                                          characters_table.cellHeight - 55
                opacity: enabled ? 1 : 0.3
                border.width: 1
                radius: 4

                Rectangle {

                    id: tagText
                    /*  gradient: Gradient
                    {
                        GradientStop {
                            position: 0
                            color: "#5F2CAB"
                        }
                        GradientStop {
                            position: 1
                            color: "#25427B"
                        }
                    }*/
                    color: "transparent"

                    anchors.right: parent.right
                    //anchors.rightMargin: 7
                    anchors.top: parent.top
                    anchors.topMargin: 7
                    implicitWidth: parent.width / 3
                    implicitHeight: 30
                    opacity: enabled ? 1 : 0.3
                    border.width: 0
                    border.color: style.getSdBackgroundBorderColor()
                    radius: 1

                    Text {
                        id: nameText
                        text: name
                        anchors.right: parent.right
                        anchors.rightMargin: 7
                        anchors.verticalCenter: parent.verticalCenter
                        font.pointSize: 11
                        color: "white"
                    }

                }

                AnimatedImage {
                    id: animatedImage

                    anchors.fill: parent
                    anchors.margins: 5
                    // visible: (portrait.indexOf(".gif") === -1) ? false : true
                    clip: false
                    visible: true
                    fillMode: AnimatedImage.PreserveAspectFit
                    source: portrait

                    onStatusChanged: {
                        if (animatedImage.status === AnimatedImage.Error)
                        {
                            db.removeTuple({ tag : characterList.get(characterList.count - 1).name},table)
                            removeImage(characterList.count - 1)
                            error.log = qsTr("Couldn't open image.") + settings.emptyString
                            error.err_name = qsTr("Couldn't open image.") + settings.emptyString
                            error.open()
                        }
                    }
                }
            }

            Rectangle {

                id: portraitText
                gradient: style.getGrayGradientStandart()
                    /*Gradient
                {
                    GradientStop {
                        position: 0
                        color: "#5F2CAB"
                    }
                    GradientStop {
                        position: 1
                        color: "#25427B"
                    }
                }*/
                implicitWidth: parent.width
                implicitHeight: 30
                opacity: enabled ? 1 : 0.3
                border.width: 1
                radius: 4

                Flickable {
                    id: flickable
                    anchors.fill: parent
                    flickableDirection: Flickable.HorizontalFlick
                    TextArea.flickable: TextArea {

                    text: portrait
                   // anchors.horizontalCenter: parent.horizontalCenter
                    clip: false
                    anchors.top: flickable.top
                    anchors.right: flickable.right
                    anchors.left: flickable.left
                    anchors.rightMargin: 5
                    anchors.leftMargin: 5
                    anchors.topMargin: 5
                    horizontalAlignment: Text.AlignRight
                    enabled: false
                    //anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 10
                    color: "white"
                    ScrollBar.horizontal: ScrollBar {}
                    ScrollBar.vertical: null
                }
                }
            }

        }
    }

    /** @brief получение абсолютной позиции элемента node по X и Y */
    function getAbsolutePosition(node) {
        var returnPos = {};
        returnPos.x = node.x;
        returnPos.y = node.y;
        var p = node.parent;
        while(p) {
            returnPos.x += p.x;
            returnPos.y += p.y;
            p = p.parent;
        }
        return returnPos;
    }

    // фон грида
    Rectangle {
        id: fone
        width: parent.width
        height: parent.height
        color: "black"
        opacity: 0.1
        radius: 5
        z: -1
    }


    /** @brief флаг для каждого персонажа, выбран ли он */
    property bool isSelected


    /* область мыши грида с возможностью листать колесиком мыши*/
    MouseArea {
        objectName: "mouseAreaGridView"
        anchors.fill: parent
        onWheel: {
            //scrollBar.increase()
            characters_table.flick(-wheel.angleDelta.y * 7,-wheel.angleDelta.y * 7)
        }
        propagateComposedEvents: true
        /* таблица персонажей = грид*/
        GridView {
            objectName: "gridView"
            interactive: true
            id: characters_table
            cellWidth: width / 3
            cellHeight: height
            anchors.fill: parent
            //  anchors.bottomMargin: 50
            //  anchors.topMargin: 50
            clip: true
            focus: true
            flow: GridView.FlowTopToBottom
            //layoutDirection: Qt.LeftToRight
            //verticalLayoutDirection: Grid.TopToBottom
            snapMode: GridView.SnapToRow
            flickableDirection: Flickable.HorizontalFlick

            onCountChanged: {
                if (count > 0)
                    emptyText.visible = false
                else
                    emptyText.visible = true
            }

            Text {
                id: emptyText
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Here is empty") + settings.emptyString
                color: textColor
                font.pixelSize: 15
                visible: false
                opacity: 0.6
            }

            //snapMode: GridView.SnapToRow
            //verticalLayoutDirection: GridView.LeftToRight
            highlight: Rectangle {
                width: 1
                height: 1
                border.width: 2
                border.color: "white"
                radius: 4
                color: "transparent"
                z: 2
                x: characters_table.currentItem.x
                y: characters_table.currentItem.y
                Behavior on x { SpringAnimation { spring: 3; damping: 0.2 } }
                Behavior on y { SpringAnimation { spring: 3; damping: 0.2 } }
            }
            highlightFollowsCurrentItem: true

            model: characterList

            delegate: characterComponentDelegate

            add: Transition {
                NumberAnimation { properties: "x,y"; easing.amplitude: 40;easing.type: Easing.InOutQuad; duration: 500 }
                //SpringAnimation { spring: 3; damping: 0.2 }
            }
            displaced: Transition {
                NumberAnimation { properties: "x,y"; easing.type: Easing.InOutQuad;duration: 500 }
            }

            remove: Transition {
                ParallelAnimation {
                    NumberAnimation { property: "opacity"; to: 0; duration: 500 }
                    NumberAnimation { property: "y"; to: y+100; duration: 500 }
                }
            }


            property string tst: ""
            OtherModules.ErrorDialog {
                id: error
            }
        }

    }
}
