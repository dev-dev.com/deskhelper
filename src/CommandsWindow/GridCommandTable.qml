/**
  * @file
  * @brief Грид с командами
  *
*/

import QtQuick 2.0
import QtQuick.Controls 2.12


Rectangle {

    id: gct
    color: "transparent"

    /** @brief Название таблицы в БД */
    property string table: "commandsList"

    /** @brief Добавление команд */
    function addingCommand()
    {
        commandList.append({name : nameEdit.text,
                               shortCuts : (bar.currentIndex === 0) ? shortcutsEdit.text : voiceInputEdit.text,
                               command : commandEdit.text,
                               isVoiceInput: bar.currentIndex === 1})
    }

    /** @brief Изменение команды*/
    function editCommand(prevName)
    {
        for (let i = 0; i < gridCommandTable.count;i++)
        {
            if (prevName === commandList.get(i).name)
            {
                commandList.get(i).name = nameEdit.text
                commandList.get(i).shortCuts = (bar.currentIndex === 0) ? shortcutsEdit.text : voiceInputEdit.text
                commandList.get(i).command = commandEdit.text
                commandList.get(i).isVoiceInput = bar.currentIndex === 1
            }
        }
    }

    /** @brief Удаление команды*/
    function removeRow()
    {
        commandList.remove(gridCommandTable.currentIndex)
    }

    /** @brief Получение имени текущей команды*/
    function getCommandName()
    {
        if (gridCommandTable.currentIndex !== -1)
            return commandList.get(gridCommandTable.currentIndex).name
        return ""
    }

    /** @brief Получение флага "это голосовой ввод" текущего элемента*/
    function getisVoiceInput()
    {
        return commandList.get(gridCommandTable.currentIndex).isVoiceInput
    }

    /** @brief Поиск команд по имени команды*/
    function findByName(cn)
    {
        var list
        if (cn === "")
            list = db.selectAllTuples(4, table)
        else
            list = db.selectWithCond({commandName : cn},4, table)

        commandList.clear()
        var i = 1
        if (list[0] > 1)
        {
            for (; i < list[0];) {
                commandList.append({name : list[i],
                                       shortCuts : list[i + 1],
                                       command : list[i + 2],
                                       isVoiceInput: (list[i + 3] === "0") ? false : true})
                i = i + 4
            }
        }
    }


    ListModel {

        id: commandList

        Component.onCompleted: {


            var list = db.selectAllTuples(4, table)

            var i = 1
            if (list[0] > 1)
            {
                for (; i < list[0];) {
                    append({name : list[i],
                               shortCuts : list[i + 1],
                               command : list[i + 2],
                               isVoiceInput: (list[i + 3] === "0") ? false : true})
                    i = i + 4
                }
            }
        }

    }



    Component {
        id: gridComponentDelegate

        Item {
            //width: (GridView.isCurrentItem) ? gridCommandTable.cellWidth :  gridCommandTable.cellWidth - 20
            //height: (GridView.isCurrentItem) ? gridCommandTable.cellHeight :  gridCommandTable.cellHeight- 20

            width: gridCommandTable.cellWidth
            height: gridCommandTable.cellHeight

            Behavior on width { NumberAnimation { duration: 400;easing.type: Easing.InOutQuad } }
            //  Behavior on height { NumberAnimation { duration: 400;easing.type: Easing.InOutQuad  } }

            MouseArea {
                anchors.fill:parent
                //anchors.bottom: parent.bottom
                width:parent.width
                height: parent.height

                hoverEnabled: true
                onPressed: {
                    gridMenu.visible = false
                }
                acceptedButtons: Qt.RightButton | Qt.LeftButton
                onClicked: {
                    if(mouse.button & Qt.RightButton) {
                        gridCommandTable.currentIndex = model.index
                        if ( Qt.RightButton)
                        {
                            gridMenu.visible = true
                            //  gridMenu.x = parent.x + mouseX
                            //  gridMenu.y = parent.y + mouseY - menushka.height
                            var pos = getAbsolutePosition(parent);
                            gridMenu.x = pos.x + mouseX
                            gridMenu.y = pos.y + mouseY
                            //console.debug(parent.y, mouseY, pos.x,pos.y)
                        }
                        else
                            gridMenu.visible = false


                        //console.debug(mouse.x, mouse.y)
                    }
                    else
                    {
                        gridCommandTable.currentIndex = model.index
                        gridMenu.visible = false
                    }
                }
                // onClicked: gridCommandTable.currentIndex = model.index
            }
            //Behavior on width { SpringAnimation { spring: 3; damping: 0.2 } }
            //Behavior on height { SpringAnimation { spring: 3; damping: 0.2 } }
            Column {

                function getTextColor() {
                    return style.getTextColor()
                }

                anchors.fill: parent
                anchors.margins: 12
                Rectangle {
                    id: image
                    color: "gray"
                    anchors.horizontalCenter: parent.horizontalCenter
                    //anchors.fill: parent
                    width: parent.width - 10
                    height: width
                    Text {
                        anchors.fill: parent
                        //anchors.bottom: parent.bottom
                        //anchors.bottomMargin: 2
                        //anchors.horizontalCenter: parent.horizontalCenter
                        text: (command.indexOf("/") === -1) ? ("> " + command.substr(0,10) +
                                                               ((command.length > 10) ? "..." : "")) :  (((command.substring(command.indexOf("/"),command.length - 1)).length > 10) ? "*" : "") + (command.substring(command.length - 11,command.length - 1))

                        //  width: parent.width
                        //  height: parent.height
                        font.pixelSize: 30
                        wrapMode: Text.WrapAnywhere
                    }

                    /*Image {
                        source: "/resources/img/chika.png"
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.fill: parent
                    }*/
                    opacity: 0.4
                }
                Text {
                    id: commandText
                   // anchors.top: image.bottom
                    anchors.horizontalCenter: image.horizontalCenter
                    text: name
                    color: parent.getTextColor()
                }

                /*Text {
                    id: commandName
                    anchors.top: command.bottom
                    text: "commandName"
                }

                Text {
                    id: call
                    anchors.top: commandName.bottom
                    text: "call"
                }*/

            }



        }
    }

    /** @brief Получение глобальных координат элемента*/
    function getAbsolutePosition(node) {
        var returnPos = {};
        returnPos.x = node.x;
        returnPos.y = node.y;
        var p = node.parent;
        while(p) {
            returnPos.x += p.x;
            returnPos.y += p.y;
            p = p.parent;
            if (p === gct) {
                break;
            }
        }
        return returnPos;
    }


    Menu {
        id: gridMenu
        font.pointSize: 10

        property string textColor: style.getTextColor()
        property string backColor: style.getBackgroundGradientStart()

        background: Rectangle {
            // anchors.fill: parent
            gradient: style.getBackgroundGradient()
            border.width: 1
            border.color: style.getTextColor()

            implicitWidth: 130
            //implicitHeight: 20
            radius: 2
        }



        MenuItem {
            contentItem: Text {
                text: qsTr("New") + settings.emptyString
                color: parent.highlighted ? gridMenu.backColor : gridMenu.textColor
                anchors.fill: parent
                font.pointSize: 10
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
            background: Rectangle {
                //gradient: parent.highlighted ? none : style.getBackgroundGradient()
                color: parent.highlighted ? gridMenu.textColor : "transparent"
                border.width: 0
                border.color: gridMenu.backColor

                implicitWidth: 130
                implicitHeight: 40
                radius: 2
            }

            onClicked: {
                clearForm()
                add_button.isAdd = true
                inputRect.state = "show"
            }
        }
        MenuItem {
            Text {
                text: qsTr("Edit") + settings.emptyString
                color: parent.highlighted ? gridMenu.backColor : gridMenu.textColor
                anchors.fill: parent
                font.pointSize: 10
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
            background: Rectangle {
                //gradient: parent.highlighted ? none : style.getBackgroundGradient()
                color: parent.highlighted ? gridMenu.textColor : "transparent"
                border.width: 0
                border.color: gridMenu.backColor

                implicitWidth: 130
                implicitHeight: 40
                radius: 2
            }

            onClicked: {

                clearForm()
                add_button.isAdd = false
                nameEdit.text = commandList.get(gridCommandTable.currentIndex).name
                previousCommandName = nameEdit.text
                //bar.currentIndex =
                bar.setCurrentIndex((commandList.get(gridCommandTable.currentIndex).isVoiceInput === true) ?
                                        1 : 0)
                if (bar.currentIndex === 0)
                    shortcutsText = commandList.get(gridCommandTable.currentIndex).shortCuts
                else
                    rezultText = commandList.get(gridCommandTable.currentIndex).shortCuts
                commandEdit.text = commandList.get(gridCommandTable.currentIndex).command
                inputRect.state = "show"
            }
        }

        MenuItem {
            Text {
                text: qsTr("Remove") + settings.emptyString
                color: parent.highlighted ? gridMenu.backColor : gridMenu.textColor
                anchors.fill: parent
                font.pointSize: 10
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
            background: Rectangle {
                //gradient: parent.highlighted ? none : style.getBackgroundGradient()
                color: parent.highlighted ? gridMenu.textColor : "transparent"
                border.width: 0
                border.color: gridMenu.backColor

                implicitWidth: 130
                implicitHeight: 40
                radius: 2
            }

            onClicked: {
                var isVoice = false

                var commandName = getCommandName()

                if (commandName === "")
                    return

                if (db.removeTuple({ commandName : commandName}, table))
                {
                    isVoice = getisVoiceInput()
                    removeRow()
                    if (!isVoice)
                    {
                        mainWindow.updShortcut()
                    }
                    mainWindow.updFastCommands()
                }
                else
                {
                    error.log = qsTr("Error deleting database row") + settings.emptyString
                    error.err_name = qsTr("Error deleting database row: ") + settings.emptyString + db.getError();
                    error.open()
                }
            }
        }
    }


    Component {
        id: listComponentDelegate

        Item {
            //width: (GridView.isCurrentItem) ? gridCommandTable.cellWidth :  gridCommandTable.cellWidth - 20
            //height: (GridView.isCurrentItem) ? gridCommandTable.cellHeight :  gridCommandTable.cellHeight- 20

            width: gridCommandTable.cellWidth
            height: gridCommandTable.cellHeight

            Behavior on width { NumberAnimation { duration: 400 } }
            Behavior on height { NumberAnimation { duration: 400  } }
            Column {

                anchors.fill: parent
                anchors.margins: 12

                GridCommandListItem
                {
                    id: commandName
                    backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    anchors.top: parent.top
                    anchors.topMargin: -4
                    width: parent.width
                    height: parent.height / 10 * 3
                    itemName: qsTr("Name: ") + settings.emptyString + name

                }

                GridCommandListItem
                {
                    id: commandShortCuts
                    backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    anchors.top: commandName.bottom
                    anchors.topMargin: 7
                    width: parent.width
                    height: parent.height / 10 * 3
                    itemName: ((isVoiceInput) ? qsTr("Voice: ") + settings.emptyString :
                                                qsTr("Shortcut: ") + settings.emptyString) + shortCuts

                }
                GridCommandListItem
                {
                    id: commandCommand
                    backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    anchors.top: commandShortCuts.bottom
                    anchors.topMargin: 7
                    width: parent.width
                    height: parent.height / 10 * 3
                    itemName: qsTr("Command: ") + settings.emptyString + command

                }

            }
            MouseArea {
                anchors.fill:parent
                //anchors.bottom: parent.bottom
                width:parent.width
                height: parent.height

                hoverEnabled: true
                onPressed: {
                    gridMenu.visible = false
                }
                acceptedButtons: Qt.RightButton | Qt.LeftButton
                onClicked: {
                    if(mouse.button & Qt.RightButton) {
                        gridCommandTable.currentIndex = model.index
                        if ( Qt.RightButton)
                        {
                            gridMenu.visible = true
                            //  gridMenu.x = parent.x + mouseX
                            //  gridMenu.y = parent.y + mouseY - menushka.height
                            var pos = getAbsolutePosition(parent);
                            gridMenu.x = pos.x + mouseX
                            gridMenu.y = pos.y + mouseY
                            //console.debug(parent.y, mouseY, pos.x,pos.y)
                        }
                        else
                            gridMenu.visible = false


                        //console.debug(mouse.x, mouse.y)
                    }
                    else
                    {
                        gridCommandTable.currentIndex = model.index
                        gridMenu.visible = false
                    }
                }
                // onClicked: gridCommandTable.currentIndex = model.index
            }
        }
    }

    Component {
        id: tableComponentDelegate

        Item {
            //width: (GridView.isCurrentItem) ? gridCommandTable.cellWidth :  gridCommandTable.cellWidth - 20
            //height: (GridView.isCurrentItem) ? gridCommandTable.cellHeight :  gridCommandTable.cellHeight- 20

            width: gridCommandTable.cellWidth
            height: gridCommandTable.cellHeight

            Behavior on width { NumberAnimation { duration: 400;easing.type: Easing.InOutQuad } }
            Behavior on height { NumberAnimation { duration: 400;easing.type: Easing.InOutQuad  } }
            Row {

                anchors.fill: parent
                anchors.margins: 2

                GridCommandListItem
                {
                    id: commandName
                    backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    _radius: 0
                    width: parent.width / 3
                    height: parent.height
                    itemName: name

                }

                GridCommandListItem
                {
                    id: commandShortCuts
                    backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    _radius: 0
                    width: parent.width / 3
                    height: parent.height
                    itemName: shortCuts

                }
                GridCommandListItem
                {
                    id: commandCommand
                    backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    _radius: 0
                    width: parent.width / 3
                    height: parent.height
                    itemName: command

                }

            }
            MouseArea {
                anchors.fill:parent
                //anchors.bottom: parent.bottom
                width:parent.width
                height: parent.height

                hoverEnabled: true
                onPressed: {
                    gridMenu.visible = false
                }
                acceptedButtons: Qt.RightButton | Qt.LeftButton
                onClicked: {
                    if(mouse.button & Qt.RightButton) {
                        gridCommandTable.currentIndex = model.index
                        if ( Qt.RightButton)
                        {
                            gridMenu.visible = true
                            //  gridMenu.x = parent.x + mouseX
                            //  gridMenu.y = parent.y + mouseY - menushka.height
                            var pos = getAbsolutePosition(parent);
                            gridMenu.x = pos.x + mouseX
                            gridMenu.y = pos.y + mouseY
                            //console.debug(parent.y, mouseY, pos.x,pos.y)
                        }
                        else
                            gridMenu.visible = false


                        //console.debug(mouse.x, mouse.y)
                    }
                    else
                    {
                        gridCommandTable.currentIndex = model.index
                        gridMenu.visible = false
                    }
                }
                // onClicked: gridCommandTable.currentIndex = model.index
            }
        }
    }

    Component {
        id:tableheader
        Rectangle  {
            z: 5
            width: parent.width
            height: 30
            //anchors.bottom: parent.top
            //gradient: style.getAttentionGradientStandart()
            color: "transparent"
            Row {

                anchors.fill: parent
                anchors.margins: 2

                GridCommandListItem
                {
                    id: commandName
                    //backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    _radius: 0
                    width: parent.width / 3
                    height: parent.height
                    itemName: qsTr("Command name") + settings.emptyString

                }

                GridCommandListItem
                {
                    id: commandShortCuts
                    //    backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    _radius: 0
                    width: parent.width / 3
                    height: parent.height
                    itemName: qsTr("Shortcuts / text") + settings.emptyString

                }
                GridCommandListItem
                {
                    id: commandCommand
                    //     backgroundGrad: style.getGradientStandart()
                    text_color: style.getTextColor()
                    border_color: style.getSdBackgroundBorderColor()
                    _radius: 0
                    width: parent.width / 3
                    height: parent.height
                    itemName: qsTr("Command") + settings.emptyString

                }

            }
        }

    }

    Rectangle {
        anchors.fill: parent
        height: parent.height
        width: parent.width
        color: "black"
        opacity: 0.1
        radius: 10
        z: -1
    }

    /** @brief Ширина элемента грида*/
    property int gridFormatItemW: 135
    /** @brief Длина элемента грида*/
    property int gridFormatItemH: 165
    /** @brief Ширина элемента списка*/
    property int listFormatItemW: gridCommandTable.width
    /** @brief Длина элемента списка */
    property int listFormatItemH:80

    /** @brief Ширина элемента таблицы */
    property int tableFormatItemW: gridCommandTable.width
    /** @brief Длина элемента таблицы*/
    property int tableFormatItemH:40


    Component {
        id: empty
        Rectangle {

        }
    }

    Transition {
        id:transition
        NumberAnimation { property: "x"; from: gridCommandTable.width; duration: 800 }
        NumberAnimation { property: "y"; from: gridCommandTable.height; duration: 100 }

    }
    /** @brief Текущее состояние таблицы (текущее представление - по умолчанию - грид)*/
    property string currentState: "grid"
    GridView {

        /*MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            onPressed: {
                inputRect.state = "hide"

            }
        }*/

        id: gridCommandTable
        objectName: "gridCommandTable"
        state: currentState
        states: [ State {
                name: "table"
                PropertyChanges {
                    target: gridCommandTable
                    add: transition
                    cellWidth: tableFormatItemW
                    cellHeight: tableFormatItemH
                    delegate: tableComponentDelegate
                    header: tableheader
                }
            },
            State {
                name: "grid"
                PropertyChanges {
                    target: gridCommandTable
                    header: empty
                    //add: undefined
                    cellWidth: gridFormatItemW
                    cellHeight: gridFormatItemH
                    delegate: gridComponentDelegate
                }
            },
            State {
                name: "list"
                PropertyChanges {
                    target: gridCommandTable
                    header: empty
                    add: transition
                    cellWidth: listFormatItemW
                    cellHeight: listFormatItemH
                    delegate: listComponentDelegate
                }
            }]
        anchors.fill: parent

        model: commandList

        focus: true
        clip: true

        flow: GridView.FlowLeftToRight
        verticalLayoutDirection: GridView.TopToBottom

        onCountChanged: {
            if (count > 0)
                emptyText.visible = false
            else
                emptyText.visible = true
        }

        Text {
            id: emptyText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("Here is empty") + settings.emptyString
            color: textColor
            font.pixelSize: 15
            visible: false
            opacity: 0.6
        }

        highlight: Rectangle {
            border.width: 2
            border.color: "white"
            radius: 4
            opacity: 1
            color: "transparent"
            z: 2
            x: gridCommandTable.currentItem.x
            y: gridCommandTable.currentItem.y
            Behavior on x { SpringAnimation { spring: 3; damping: 0.2 } }
            Behavior on y { SpringAnimation { spring: 3; damping: 0.2 } }
        }
        highlightFollowsCurrentItem: true

    }
    property string textColor
}

