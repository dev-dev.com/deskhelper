﻿/**
  * @file
  * @brief Окно манипуляций над персонажем
  *
  * Страница, на которой можно изменить/удалить/добавить команду
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.2
import Qt.labs.qmlmodels 1.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.14
import "../Other" as OtherModules
import "../Style" as StyleModules

Page {

    id: commandWindow


    /** @brief путь до файла изображения*/
    property string pathText: ""
    /** @brief сочетание клавиш*/
    property string shortcutsText: ""

    /** @brief Имя таблицы БД: для отправки запросов*/
    property string table: "commandsList"
    /** @brief Текущая активная кнопка выбора отображения таблицы*/
    property OtherModules.StylishButton activeButton

    background: LinearGradient {
        id: lg
        z:0
        anchors.fill: parent

        start: Qt.point(0,0)
        end: Qt.point(parent.width, parent.height)

        gradient: style.getBackgroundGradient()

    }

    z: 4

    Column {

        id: scrcol
        width: parent.width
        spacing: 30
        anchors.fill: parent
        anchors.topMargin: 20
        //  width: Screen.width * 0.61
        //  height: Screen.height * 0.65

        //  minimumWidth: Screen.width * 0.3
        //  minimumHeight: Screen.height * 0.57
        visible: true

        //  flags: Qt.FramelessWindowHint | Qt.Window


        // строка поиска посередине
        TextField {
            id: search
            //z:3
            //  anchors.top: parent.bottom
            //anchors.right: parent.right
            //  anchors.topMargin: 15
            //anchors.rightMargin: 0
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * 2 / 3
            placeholderText: qsTr("Search by file name") + settings.emptyString
            placeholderTextColor: style.getPlaceholderTextColor()
            color: style.getTextColor()
            background: Rectangle {
                implicitWidth: parent.width
                implicitHeight: parent.height
                color: "transparent"
                border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                border.width: 3
                radius: 5
            }

            onTextChanged: {
                grid.findByName(text)
            }
        }



        GridCommandTable {
            id: grid
            objectName: "grid"
            anchors.top: search.bottom
            anchors.topMargin: 30
            //z:100
            //anchors.right: parent.right
            //anchors.rightMargin: 35
            //anchors.left: parent.left
            //anchors.leftMargin: 30
            anchors.horizontalCenter: search.horizontalCenter
            //width: parent.width * 4 / 5//search.width
            //height: parent.height * 5 / 7
            width:  parent.width * 4 / 5
            height: commandWindow.height / 15 * 11//Screen.height * 0.39
            textColor: style.getTextColor()

            function reanchorToLeft() {
                anchors.horizontalCenter = undefined
                anchors.left = parent.left
                anchors.leftMargin = 20

            }

            // кнопки слева сверху
            Column {
                id: buttonsRow
                objectName: "buttonsRow"
                anchors.right: parent.left
                anchors.top: parent.top
                anchors.rightMargin: 10
                //anchors.verticalCenter: grid.verticalCenter
                //anchors.horizontalCenter: grid.horizontalCenter
                // height: grid.height
                width: grid.width / 15
                spacing: 10
                Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }

                //property bool isEntered: false
                OtherModules.StylishButton {
                    id: new_button

                    image_src: "/resources/img/add.svg"

                    objectName: "newButton"


                    // width: 200
                    width: parent.width
                    //height: 60
                    //button_name: qsTr("New") + settings.emptyString
                    background_gradient: style.getGradientStandart()
                    entered_gradient: style.getGradientEntered()
                    //border_color: style.getBorderColor()
                    //text_color: style.getTextColor()

                    onMouseAreaClicked: {
                        //inputRect.reanchorToLeft()
                        clearForm()
                        add_button.isAdd = true
                        inputRect.state =  "show"
                        //search.state = (search.state == "anchorLeft") ? "horCenter" : "anchorLeft"

                        if (settings.getPromptMode()) {
                            for (var i = 2; i < repeater.count; ++i){
                                repeater.itemAt(i).doUpdate(root.theWindow.getComWinVisible(i))
                            }
                        }
                    }
                }


                OtherModules.StylishButton {
                    id: delete_button

                    image_src: "/resources/img/remove.svg"

                    //   width: 200
                    width: parent.width

                    //button_name: qsTr("Remove") + settings.emptyString
                    background_gradient: style.getGradientStandart()
                    entered_gradient: style.getGradientEntered()


                    onMouseAreaClicked: {

                        var commandName = grid.getCommandName()

                        if (commandName === "")
                            return

                        var list = db.selectAllTuples(9, "remindersList")

                        var i = 1
                        var commands = []
                        var idList = []
                        if (list[0] > 1)
                        {
                            for (; i < list[0];) {
                                idList.push(list[i])
                                commands.push(list[i + 8])
                                i = i + 9
                            }
                            console.log(commands)
                        }
                        var commandsList
                        var flag = false
                        for (i = 0; i < commands.length; i++){
                            commandsList = db.toList(commands[i])
                            for (var j = 0; j < commandsList.length; j++){
                                if (commandsList[j] === commandName){
                                    flag = true
                                    break
                                }
                            }
                            if (flag)
                                break
                        }

                        if (flag){
                            delete_dialog.open()
                        }
                        else{
                            removeCommand()//delete_dialog.visible = true//
                        }


                    }

                    function removeCommand(){
                        var commandName = grid.getCommandName()
                        var isVoice = false
                        if (db.removeTuple({ commandName : commandName}, table))
                        {
                            isVoice = grid.getisVoiceInput()
                            grid.removeRow()
                            if (!isVoice)
                            {
                                mainWindow.updShortcut()
                            }
                            mainWindow.updFastCommands()
                        }
                        else
                        {
                            error.log = qsTr("Error deleting database row") + settings.emptyString
                            error.err_name = qsTr("Error deleting database row: ") + settings.emptyString + db.getError();
                            error.open()
                        }
                    }
                }


                Dialog {
                    id: delete_dialog
                    width: 400
                    height: 150
                    contentItem: Rectangle {
                        width: 400
                        height: 200
                        gradient: style.getBackgroundGradient()
                        Text {
                            anchors{
                                top: parent.top
                                left: parent.left
                                right: parent.right
                                bottom: ok_button.top
                                topMargin: 30
                                bottomMargin: 10
                                rightMargin: 10
                                leftMargin: 10
                            }
                            text: qsTr("This command is contained in the reminders, do you really want to delete this command?")
                            color: "white"
                            font.pointSize: 11
                            wrapMode: Text.WordWrap

                        }

                        OtherModules.StylishButton {
                            id: cancel_button
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            anchors.bottomMargin: 10
                            anchors.rightMargin: 20
                            button_name: qsTr("Cancel")
                            background_gradient: style.getAttentionGradientStandart()
                            entered_gradient: style.getAttentionGradientEntered()
                            width: 100
                            onMouseAreaClicked: {
                                delete_dialog.close()
                            }
                        }
                        OtherModules.StylishButton {
                            id: ok_button
                            anchors.bottom: parent.bottom
                            anchors.right: cancel_button.left
                            anchors.bottomMargin: 10
                            anchors.rightMargin: 20
                            button_name: "OK"
                            background_gradient: style.getGradientStandart()
                            entered_gradient: style.getGradientEntered()
                            width: 100
                            onMouseAreaClicked: {
                                var commandName = grid.getCommandName()
                                var list = db.selectAllTuples(9, "remindersList")

                                var i = 1
                                var commands = []
                                var idList = []
                                if (list[0] > 1)
                                {
                                    for (; i < list[0];) {
                                        idList.push(list[i])
                                        commands.push(list[i + 8])
                                        i = i + 9
                                    }
                                }

                                var commandsList
                                var newCommands = ""
                                for (i = 0; i < commands.length; i++){
                                    commandsList = db.toList(commands[i])
                                    for (var j = 0; j < commandsList.length; j++){
                                        if (commandsList[j] === commandName)
                                            continue
                                        else
                                            newCommands += commandsList[j] + ","
                                    }
                                    db.editTuple({commands : newCommands}, "remindersList",{ID: idList[i]})
                                    newCommands = ""
                                }
                                delete_button.removeCommand()
                                delete_dialog.close()
                            }
                        }
                    }
                }

            }

            // кнопки внизу справа
            Column {
                id: rightNav
                anchors.left: parent.right
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.top: undefined
                anchors.leftMargin: 10
                //anchors.verticalCenter: grid.verticalCenter
                //anchors.horizontalCenter: grid.horizontalCenter
                //height: grid.height
                width: grid.width / 15
                spacing: 10
                Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }

                //property OtherModules.StylishButton activeButton: grid_button
                OtherModules.StylishButton {
                    id: grid_button

                    image_src: "/resources/img/grid.svg"

                    // width: 200
                    width: parent.width
                    //height: 60
                    //button_name: qsTr("grid") + settings.emptyString
                    background_gradient: style.getAttentionGradientStandart()
                    entered_gradient: style.getAttentionGradientEntered()
                    //border_color: style.getBorderColor()
                    //text_color: style.getTextColor()

                    onVisibleChanged: {
                        activeButton = grid_button
                    }

                    onMouseAreaClicked: {
                        if (activeButton)
                        {
                            activeButton.background_gradient = style.getGradientStandart()
                            activeButton.entered_gradient = style.getGradientEntered()
                        }
                        activeButton = grid_button
                        grid_button.background_gradient = style.getAttentionGradientStandart()
                        grid_button.entered_gradient =style.getAttentionGradientEntered()

                        //  grid.currentFormatItemW = grid.gridFormatItemW
                        //  grid.currentFormatItemH = grid.gridFormatItemH
                        //  grid.currentDelegate = grid.gridDelegate
                        //grid.currentHeader = undefined
                        grid.currentState = "grid"
                    }
                }

                OtherModules.StylishButton {
                    id: list_button

                    image_src: "/resources/img/list.svg"

                    width: parent.width

                    //button_name: qsTr("list") + settings.emptyString
                    background_gradient: style.getGradientStandart()
                    entered_gradient: style.getGradientEntered()


                    onMouseAreaClicked: {
                        if (activeButton)
                        {
                            activeButton.background_gradient = style.getGradientStandart()
                            activeButton.entered_gradient = style.getGradientEntered()
                        }
                        else
                        {
                            grid_button.background_gradient = style.getGradientStandart()
                            grid_button.entered_gradient = style.getGradientEntered()
                        }

                        activeButton = list_button
                        list_button.background_gradient = style.getAttentionGradientStandart()
                        list_button.entered_gradient =style.getAttentionGradientEntered()

                        /// grid.currentFormatItemW = grid.listFormatItemW
                        // grid.currentFormatItemH = grid.listFormatItemH
                        // grid.currentDelegate = grid.listDelegate
                        //grid.currentHeader = undefined
                        grid.currentState = "list"
                    }
                }

                OtherModules.StylishButton {
                    id: table_button

                    image_src: "/resources/img/table.svg"

                    //anchors.right: parent.right
                    width: parent.width

                    //button_name: qsTr("table") + settings.emptyString
                    background_gradient: style.getGradientStandart()
                    entered_gradient: style.getGradientEntered()

                    onMouseAreaClicked: {
                        if (activeButton)
                        {
                            activeButton.background_gradient = style.getGradientStandart()
                            activeButton.entered_gradient = style.getGradientEntered()
                        }
                        else
                        {
                            grid_button.background_gradient = style.getGradientStandart()
                            grid_button.entered_gradient = style.getGradientEntered()
                        }
                        activeButton = table_button
                        table_button.background_gradient = style.getAttentionGradientStandart()
                        table_button.entered_gradient =style.getAttentionGradientEntered()

                        // grid.currentFormatItemW = grid.tableFormatItemW
                        // grid.currentFormatItemH = grid.tableFormatItemH
                        // grid.currentDelegate = grid.tableDelegate
                        // grid.currentHeader = grid._tableHeader
                        grid.currentState = "table"

                    }
                }
            }

        }


        MouseArea {
            id: inputRectClosingMouseArea
            anchors {
                top: scrcol.top
                topMargin: -scrcol.anchors.topMargin
                bottom: scrcol.bottom
                left: scrcol.left
                right: inputRect.left
            }

            enabled: false
            visible: enabled
            onClicked: {
                inputRect.state = "hide"
                enabled = false
            }
            Rectangle {
                anchors.fill: parent
                color: "black"
                opacity: 0.2
            }
        }

        Item {

            id: inputRect
            objectName: "inputRect"

            anchors.top: parent.top
            anchors.topMargin: -(scrcol.anchors.topMargin + 5)
            anchors.bottom: parent.bottom
            //anchors.left: lg.right
            //z: 100
            //  color: "#43346f"
            state: "hide"
            //anchors.topMargin: 105
            width: Math.min(commandWindow.width, commandWindow.height) / 7 * 4//grid.width
            //height: commandWindow.height
            anchors.left: parent.right
            //property int oldPosition: anchors.leftMargin
            function reanchorToLeft() {
                anchors.left =  search.right
                anchors.leftMargin =  commandWindow.width -  search.width - search.x
                //   console.debug(">>>", commandWindow.width, search.width,search.x, commandWindow.x, anchors.leftMargin)
            }
            //color: "transparent"
            implicitWidth: grid.width
            implicitHeight: grid.height

            Timer {
                id: timerRightNavCloses
                interval: 250
                running: false
                //triggered: false
                onTriggered: {
                    if (inputRect.state === "hide")
                    {
                        running = false
                        inputRect.visible = false
                    }
                }
            }
            onStateChanged: {
                if (state === "show") {
                    inputRect.visible = true
                    if (settings.getPromptMode()) {
                        for (var i = 2; i < repeater.count; ++i){
                            repeater.itemAt(i).doUpdate(root.theWindow.getComWinVisible(i))
                        }
                    }
                    inputRectClosingMouseArea.enabled = true
                } else {
                    timerRightNavCloses.running = true
                    if (settings.getPromptMode()) {
                        for (i = 2; i < repeater.count; ++i){
                            repeater.itemAt(i).doUpdate(false)
                        }
                    }
                    inputRectClosingMouseArea.enabled = false
                }
            }

            states: State {
                name: "show"
                AnchorChanges {
                    target: inputRect
                    //anchors.left: lg.left
                    anchors.left: undefined
                    anchors.right: parent.right
                    //anchors.left: undefined
                    //anchors.horizontalCenter: search.horizontalCenter

                }
            }
            State {
                name: "hide"
                AnchorChanges {
                    target: inputRect
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.right: undefined
                    anchors.left: parent.right
                    //anchors.horizontalCenter: undefined
                    //anchors.left: lg.right
                }
            }
            transitions: Transition {
                // smoothly reanchor myRect and move into new position
                AnchorAnimation { duration: 200 }
            }

            Rectangle {
                anchors.left: parent.left
                //anchors.verticalCenter: commandWindow.verticalCenter
                // anchors.right: parent.right
                height: commandWindow.height + 10
                width: inputRect.width + inputRect.width / 3
                color: "black"
                opacity: 0.6
                //radius: 10
                z: -1
            }


            //  gradient: style.getBackgroundGradient()
            Column {

                id: col
                objectName: "col"
                anchors.fill: parent
                anchors.topMargin: 50
                anchors.leftMargin: 20
                anchors.rightMargin: 15

                width: Math.min(commandWindow.width, commandWindow.height) / 8 * 3


                TextField {
                    id: nameEdit
                    objectName: "nameEdit"
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: col.width
                    //height: 40

                    selectByMouse: true
                    placeholderText: qsTr("Enter command name") + settings.emptyString
                    placeholderTextColor: "#fffddd"//style.getPlaceholderTextColor()
                    color: "white"//style.getTextColor()

                    background: Rectangle {
                        implicitWidth: parent.width
                        implicitHeight: parent.height
                        color: "transparent"
                        border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                        border.width: 3
                        radius: 5
                    }

                }

                Row {
                    id: commandInputRow
                    objectName: "commandInputRow"
                    anchors.top: nameEdit.bottom
                    anchors.topMargin: 30
                    spacing: 5

                    /** @brief Очистка поля ввода команды*/
                    function clearCommandEdit() {
                        commandEdit.clear()
                    }
                    /** @brief Получение значения поля ввода команды*/
                    function getCommandEdit() {
                        return commandEdit.text
                    }

                    Rectangle {
                        id: scrollTextArea
                        objectName: "scrollTextArea"
                        width: col.width - 80
                        height: 35
                        color: "transparent"
                        border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                        border.width: 3
                        radius: 5
                        Flickable {
                            id: flickable
                            objectName: "flickable"
                            anchors.fill: parent
                            flickableDirection: Flickable.VerticalFlick
                            TextArea.flickable: TextArea {
                                id: commandEdit
                                objectName:"commandEdit"
                                text: pathText
                                selectByMouse: true
                                placeholderText: qsTr("Enter command") + settings.emptyString
                                placeholderTextColor: "#fffddd"//style.getPlaceholderTextColor()
                                color: "white"//style.getTextColor()

                                wrapMode: TextInput.WrapAnywhere

                                /*onFocusChanged: {
                                if (activeFocus)
                                    scrollTextArea.height = 100
                                else
                                    scrollTextAreaЯЯ.height = 40
                            }*/
                            }
                            clip: false

                            ScrollBar.vertical: ScrollBar {}
                            ScrollBar.horizontal: null
                        }
                    }
                    OtherModules.StylishButton {
                        id: inputPath
                        //anchors.left: scrollTextArea.right
                        //anchors.leftMargin: 5
                        width: 35
                        height: 35



                        image_src:"/resources/img/file.png"
                        //button_name: "O"
                        background_gradient: style.getGradientStandart()
                        entered_gradient: style.getGradientEntered()

                        //button_name: qsTr("Choose application") + settings.emptyString
                        onMouseAreaClicked: {
                            fileDialog.visible = true
                        }
                    }

                    OtherModules.StylishButton {
                        id: folderPath
                        //anchors.leftMargin: 5
                        width: 35
                        height: 35

                        image_src:"/resources/img/folder.png"
                        //button_name: "O"
                        background_gradient: style.getGradientStandart()
                        entered_gradient: style.getGradientEntered()

                        //button_name: qsTr("Choose application") + settings.emptyString
                        onMouseAreaClicked: {
                            folderDialog.visible = true
                        }
                    }


                    FileDialog {
                        id: fileDialog
                        visible: false
                        title: qsTr("Select a file") + settings.emptyString
                        nameFilters: db.getExpansion()
                        folder: shortcuts.home
                        onAccepted: {
                            console.log("You chose: " + fileDialog.fileUrl)
                            pathText = "\"" + db.trimString(fileDialog.fileUrl) + "\""
                        }
                        onRejected: {
                            console.log("Canceled")
                        }
                    }

                    FileDialog {
                        id: folderDialog
                        visible: false
                        folder: shortcuts.home
                        selectFolder: true
                        onAccepted: {
                            console.log("You chose: " + folderDialog.fileUrl)
                            if (stt.isWin())
                                pathText = "start " + db.trimString(folderDialog.fileUrl)
                            else
                                pathText = "xdg-open " + db.trimString(folderDialog.fileUrl)
                        }
                        onRejected: {
                            console.log("Canceled")
                        }
                    }

                }

                Rectangle {
                    id: delimiter
                    anchors.top: commandInputRow.bottom
                    // anchors.right: parent.right
                    anchors.topMargin: 50

                    width: col.width
                    height: 2
                    color: "white"
                    radius: 5
                }



                Rectangle {
                    id: currentVal
                    height: 3
                    width: bar.width / 2
                    implicitWidth: height
                    implicitHeight: height
                    anchors.bottom: comandInputLayout.top
                    anchors.bottomMargin: 20
                    anchors.right: bar.right
                    color: "#fffddd"//style.getSdTextColor()
                    state: "first"
                    states: State {
                        name: "first"
                        AnchorChanges {
                            target: currentVal
                            anchors.right: undefined
                            anchors.left: bar.left

                        }
                    }
                    State {
                        name: "second"
                        AnchorChanges {
                            target: currentVal
                            anchors.left: undefined
                            anchors.right: bar.right

                        }
                    }
                    transitions: Transition {
                        // smoothly reanchor myRect and move into new position
                        AnchorAnimation { duration: 200 }
                    }
                }

                TabBar {
                    id: bar
                    objectName: "bar"
                    anchors.top: delimiter.bottom
                    // anchors.right: parent.right
                    anchors.topMargin: 20


                    onCurrentIndexChanged: {
                        if (currentIndex === 0)
                            currentVal.state = "first"
                        else
                            currentVal.state = "second"
                    }
                    background: Rectangle {
                        color: "transparent"
                    }

                    width: col.width
                    height: 20

                    //property string colorF: style.getSdIndicatorLadenBorderColor()
                    TabButton {
                        id:keybrdbtn

                        background: Rectangle {
                            color: "transparent"
                        }
                        contentItem: Text {
                            id:keyboardTextLbl
                            text: qsTr("Keyboard") + settings.emptyString
                            color: (keybrdbtn.checked) ? "white ": style.getSdIndicatorLadenBorderColor()
                            anchors.horizontalCenter: keybrdbtn.horizontalCenter
                        }

                        onCheckedChanged: {
                            if (checked)
                                keyboardTextLbl.color = "white"//style.getSdTextColor()
                            else
                                keyboardTextLbl.color = style.getSdIndicatorLadenBorderColor()
                        }
                    }
                    TabButton {
                        id: voicebtn
                        background: Rectangle {
                            color: "transparent"
                        }

                        contentItem:  Text {
                            id:voiceTextLbl
                            text: qsTr("Voice") + settings.emptyString
                            color:(voicebtn.checked) ? "white ": style.getSdIndicatorLadenBorderColor()
                        }
                        onCheckedChanged: {
                            if (checked)
                                voiceTextLbl.color = "white"//style.getSdTextColor()
                            else
                                voiceTextLbl.color = style.getSdIndicatorLadenBorderColor()
                        }
                    }
                }


                StackLayout {
                    id: comandInputLayout
                    objectName: "comandInputLayout"
                    anchors.top: bar.bottom
                    anchors.topMargin: 30

                    /** @brief Очистка поля ввода голоса*/
                    function clearVoice() {
                        voiceInputEdit.clear()
                    }

                    currentIndex: bar.currentIndex
                    Item {
                        id: homeTab
                        objectName: "homeTab"
                        TextField {
                            id: shortcutsEdit

                            width: col.width
                            //height: 40

                            placeholderText: qsTr("Press to enter") + settings.emptyString
                            placeholderTextColor:"#fffddd"// style.getPlaceholderTextColor()
                            color: "white"//style.getTextColor()

                            text: shortcutsText
                            readOnly: true
                            background: Rectangle {
                                implicitWidth: parent.width
                                implicitHeight: parent.height
                                color: "transparent"
                                border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                border.width: 3
                                radius: 5


                            }

                            onTextChanged: {
                                shortcutsText = text
                            }

                            onPressed: {
                                keySelection.clearData()
                                keySelection.visible = (keySelection.visible) ? false : true

                            }

                            Keys.onReleased: {

                                console.debug(event.key)

                                bar.currentIndex = 1
                            }
                        }
                    }
                    Item {
                        id: discoverTab
                        objectName: "discoverTab"
                        TextField {
                            id: voiceInputEdit
                            objectName: "voiceInputEdit"
                            width: col.width - 35
                            //  height: 40
                            selectByMouse: true
                            placeholderText: qsTr("Voice input field") + settings.emptyString
                            placeholderTextColor: "#fffddd"//style.getPlaceholderTextColor()
                            color: "white"//style.getTextColor()
                            text: rezultText

                            // языкозависимый параметр
                            validator: RegExpValidator {regExp: /[А-Яа-я ]{0,}/}

                            background: Rectangle {
                                implicitWidth: parent.width
                                implicitHeight: parent.height
                                color: "transparent"
                                border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                border.width: 3
                                radius: 5
                            }

                        }
                        OtherModules.StylishButton {
                            id: record_button

                            image_src: "/resources/img/record.png"

                            anchors.left: voiceInputEdit.right
                            anchors.top: voiceInputEdit.top
                            // anchors.topMargin: 5
                            anchors.leftMargin: 5
                            width: 30
                            height: 30

                            background_gradient: (isRecord == 1) ? style.getAttentionGradientStandart() :
                                                                   (isRecord == 2) ? style.getGradientStandart() :
                                                                                     style.getStartingGradientStandart()
                            entered_gradient: (isRecord == 1) ? style.getAttentionGradientEntered() :
                                                                (isRecord == 2) ? style.getGradientEntered() :
                                                                                  style.getStartingGradientEntered()
                            //button_name: "R"
                            onMouseAreaClicked: {
                                if (isRecord && stt.getEvenClick())
                                    return

                                if (!stt.isInternetAccess())
                                {
                                    error.err_name = qsTr("No internet connection") + settings.emptyString
                                    error.log = "No internet"
                                    error.visible = true
                                    return
                                }

                                isRecord = (!isRecord) ? 1 : 2
                                stt.toggleRecord()
                                if (!stt.getEvenClick())
                                    recordtime.running = true
                            }
                        }
                    }
                }

                OtherModules.StylishButton {
                    id: add_button
                    objectName: "addButton"

                    /** @brief Принадлежность кнопки к добавлению(иначе - редактирование)*/
                    property bool isAdd: true
                    anchors.top: comandInputLayout.bottom

                    anchors.topMargin: 50
                    width: col.width

                    background_gradient: style.getGradientStandart()
                    entered_gradient: style.getGradientEntered()

                    button_name: (isAdd) ? qsTr("Add") + settings.emptyString :
                                           qsTr("Edit") + settings.emptyString
                    onMouseAreaClicked: {

                        if ((nameEdit.text === "") || (commandInputRow.getCommandEdit() === "") ||
                                ((bar.currentIndex === 0) ?
                                     shortcutsEdit.text === "" : voiceInputEdit.text === ""))

                        {
                            error.log = qsTr("Fill in all the fields!") + settings.emptyString
                            error.err_name = qsTr("Fill in all the fields!") + settings.emptyString
                            error.visible = true
                        }
                        else
                        {

                            if (isAdd)
                            {

                                if (db.addTuple([nameEdit.text, (bar.currentIndex === 0) ?
                                                     shortcutsEdit.text : voiceInputEdit.text,
                                                 commandEdit.text, bar.currentIndex === 1], table))
                                {
                                    grid.addingCommand()
                                    if (bar.currentIndex === 0)
                                    {
                                        mainWindow.updShortcut()
                                    }
                                    mainWindow.updFastCommands()

                                    clearForm()
                                    inputRect.state = "hide"

                                }
                                else
                                {
                                    error.log = qsTr("Error adding database row") + settings.emptyString
                                    error.err_name = qsTr("Error adding database row: ") + settings.emptyString + db.getError();
                                    error.open()
                                }
                            }



                            else
                            {


                                if (db.editTuple(
                                            {commandName : nameEdit.text,
                                                KeysORVoice : (bar.currentIndex === 0) ?
                                                                  shortcutsEdit.text : voiceInputEdit.text,
                                                action : commandEdit.text,
                                                isVoiceInput : (bar.currentIndex === 1)}, table,
                                            {commandName : previousCommandName}))
                                {
                                    grid.editCommand(previousCommandName)
                                    if (bar.currentIndex === 0)
                                        mainWindow.updShortcut()

                                    mainWindow.updFastCommands()
                                    clearForm()
                                    inputRect.state = "hide"

                                }
                                else
                                {
                                    error.log = qsTr("Error changing database row") + settings.emptyString
                                    error.err_name = qsTr("Error changing database row: ") + settings.emptyString + db.getError();
                                    error.open()
                                }
                            }
                        }
                    }
                }
            }
        }





        // таймер работы голосового ввода (макс. 10 секунд чтения голоса)
        /*Timer {
            id: recordtime
            interval: 10000
            repeat: false
            running: false
            onTriggered: {
                recordtime.running = false
                if (!stt.getEvenClick())
                {
                    stt.toggleRecord();
                    isRecord = 2;
                }
            }
        }*/

        // Соединение с классом для получения ответа, когда голос будет обработан
        Connections {
            target: stt

            onSendTexttoMainClass: {
                isRecord = 0
                rezultText = stt.getRezult()
            }
        }

        // диалог ожидания нажатия сочетания клавиш
        OtherModules.ErrorDialog {
            id: keySelection
            onlyMessage: true
            onWindowClosed: {
                shortcutsText = ""
                for (var i in keySelection.shortcutHead)
                {
                    shortcutsText += keySelection.shortcutHead[i]

                }
            }
        }

        // прямоугольник с подсказками
        Repeater{
            id: repeater
            model: 7
            delegate: OtherModules.GuideRectangle{
                id: guide_ism

                onPromptClicked: {
                    root.theWindow.setComWinVisible(model.index)
                }

            }
            Component.onCompleted: {
                for (var i = 0; i < repeater.count; ++i){
                    if (i === 0){
                        repeater.itemAt(i).promptText = qsTr("Below are the buttons for adding and removing comands") + settings.emptyString
                        repeater.itemAt(i).parent = new_button
                        repeater.itemAt(i).anchors.bottom = new_button.top
                        repeater.itemAt(i).anchors.horizontalCenter = new_button.horizontalCenter
                    }
                    else if (i === 1){
                        repeater.itemAt(i).promptText = qsTr("Change the appearance of the table (grid, list, table)") + settings.emptyString
                        repeater.itemAt(i).parent = grid_button
                        repeater.itemAt(i).anchors.bottom = grid_button.top
                        repeater.itemAt(i).anchors.horizontalCenter = grid_button.horizontalCenter
                    }
                    else if (i === 2){
                        repeater.itemAt(i).promptText = qsTr("Command name input field") + settings.emptyString
                        repeater.itemAt(i).promptRotation = -90
                        //repeater.itemAt(i).anchors.right = inputRect.left
                        //repeater.itemAt(i).anchors.top = inputRect.top
                        //repeater.itemAt(i).anchors.topMargin = col.anchors.topMargin - nameEdit.height / 2
                        repeater.itemAt(i).parent = nameEdit
                        repeater.itemAt(i).anchors.verticalCenter = nameEdit.verticalCenter
                        repeater.itemAt(i).anchors.right = nameEdit.left
                        repeater.itemAt(i).anchors.rightMargin = nameEdit.height
                        //repeater.itemAt(i).anchors.verticalCenterOffset = nameEdit.height/2
                    }
                    else if (i === 3){

                        repeater.itemAt(i).promptText = qsTr("Command input field cmd (also you can select the program to open)") + settings.emptyString
                        repeater.itemAt(i).promptRotation = -90
                        repeater.itemAt(i).parent = commandEdit
                        repeater.itemAt(i).anchors.verticalCenter = commandEdit.verticalCenter
                        repeater.itemAt(i).anchors.right = commandEdit.left
                        repeater.itemAt(i).anchors.rightMargin = nameEdit.height
                    }
                    else if (i === 4){
                        repeater.itemAt(i).promptText = qsTr("Select voice input and shortcuts") + settings.emptyString
                        repeater.itemAt(i).promptRotation = -90
                        repeater.itemAt(i).parent = bar
                        repeater.itemAt(i).anchors.verticalCenter = bar.verticalCenter
                        repeater.itemAt(i).anchors.right = bar.left
                        repeater.itemAt(i).anchors.rightMargin = nameEdit.height

                    }
                    else if (i === 5){
                        repeater.itemAt(i).promptText = qsTr("Shortcut or voice input field") + settings.emptyString
                        repeater.itemAt(i).promptRotation = -90
                        repeater.itemAt(i).parent = bar
                        repeater.itemAt(i).anchors.top = bar.bottom
                        repeater.itemAt(i).anchors.topMargin = 30 - (repeater.itemAt(i).height/2 - nameEdit.height/2)
                        repeater.itemAt(i).anchors.right = bar.left
                        repeater.itemAt(i).anchors.rightMargin = nameEdit.height
                    }
                    else if (i === 6){
                        repeater.itemAt(i).promptText = qsTr("Adding a command (all fields must be filled)") + settings.emptyString
                        repeater.itemAt(i).promptRotation = -90
                        repeater.itemAt(i).parent = add_button
                        repeater.itemAt(i).anchors.verticalCenter = add_button.verticalCenter
                        repeater.itemAt(i).anchors.right = add_button.left
                        repeater.itemAt(i).anchors.rightMargin = nameEdit.height
                    }
                    changeGuideVisible()
                }
            }
            function changeGuideVisible(){
                for (var i = 0; i < repeater.count; ++i){
                    if (settings.getPromptMode()) {
                        if (i < 2 || inputRect.state === "show") {
                            repeater.itemAt(i).doUpdate(root.theWindow.getComWinVisible(i))
                        } else {
                            repeater.itemAt(i).doUpdate(false)
                        }
                    } else {
                        repeater.itemAt(i).doUpdate(false)
                    }
                }
            }
        }



        // диалог ошибки
        OtherModules.ErrorDialog {
            id: error
        }

        // соединение с Main
        Item
        {
            id: root
            property Window theWindow: Window.window
            Connections
            {
                target: root.theWindow
                onShowPromptChanged: {
                    repeater.changeGuideVisible()
                }

                /*onClosing:
                {
                    guide_visible = false
                    comand_window_flag = true
                }*/
            }
        }

        // элемент хранения всех цветов
        StyleModules.Style {
            id: style
        }
    }
    /** @brief Очистка формы добавления команд*/
    function clearForm() {
        nameEdit.clear()

        commandInputRow.clearCommandEdit()
        comandInputLayout.clearVoice()
        shortcutsText = ""
        rezultText = ""
        pathText = ""
    }
    /** @brief Текст голосового ввода*/
    property string rezultText: ""
    /** @brief Режим записи: 0-не активна,нет данных; 1-активна;2-не активна, обрабатываются данные*/
    property int isRecord: 0
    /** @brief Прошлое название команды - нужно для того, чтобы корректно изменить строку
        в БД при изменнеии команды*/
    property string previousCommandName
    /** @brief Флаг работы подсказок*/
    property bool guide_visible: guide.visible
}

