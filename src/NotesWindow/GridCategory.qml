/**
  * @file
  * @brief Грид для отображения категорий каждой заметки
  *
*/
import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0

/*!
    \class GridCategory
    \brief Грид для отображения категорий каждой заметки
 */
Item {

    id: gridCat
    /** @brief категории для добавления*/
    property var categories

    height: 25
    /*onWheel: {
        //scrollBar.increase()
        catgrid.flick(-wheel.angleDelta.y * 4,-wheel.angleDelta.y * 4)
    }
    acceptedButtons: Qt.RightButton
    onPressAndHold: {
        //console.log(mouseX,mouseY,catgrid.indexAt(mouseX,mouseY))
        if (catgrid.indexAt(mouseX,mouseY) !== -1)
            lm.get(catgrid.indexAt(mouseX,mouseY)).toolTipTextVisible = true
    }

    propagateComposedEvents: true*/

    Rectangle {
        id: leftStrip
        anchors {
            left: parent.left
            leftMargin: 5
            top: parent.top
            topMargin: -2

        }
        width: 6
        height: 27
        radius: 1
        z: 2
        //gradient: style.getGrayGradientStandart()
        color: "transparent"//"#777a7f"
        Label {
            anchors.fill: parent
            verticalAlignment: Label.AlignTop
            text: "("
            font.pixelSize: 25
            font.bold: true
            color: "#b6bbc2"
        }
    }
    Rectangle {
        id: rightStrip
        anchors {
            right: parent.right
            rightMargin: 5
            top: parent.top
            topMargin: -2

        }
        width: leftStrip.width
        height: leftStrip.height
        radius: leftStrip.radius
        z: leftStrip.z
        color: leftStrip.color
        //gradient: leftStrip.gradient
        Label {
            anchors.fill: parent
            verticalAlignment: Label.AlignTop
            text: ")"
            font.pixelSize: 25
            font.bold: true
            color: "#b6bbc2"
        }
    }

    ListView {
        id: catgrid

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: leftStrip.right
            leftMargin: -2
            right: rightStrip.left
            rightMargin: -4
            topMargin: 5
        }

        height: parent.height
        clip: true

        orientation: Qt.Horizontal

        delegate: componentCategory
        model: lm
        spacing: 5

        onCountChanged: {
            runCatGridAnimation.stop()
            runCatGridPropertyAnimation.changeOptions()
            runCatGridAnimation.start()
        }

        SequentialAnimation on contentX {
            id: runCatGridAnimation
            running: true
            loops: Animation.Infinite

            PropertyAnimation {
                id: runCatGridPropertyAnimation
                from: -gridCat.width
                to: (catgrid.count * 60 + 6 * (catgrid.count - 1)); duration: 8000;
                function changeOptions() {
                    console.log(">>>",gridCat.width)
                    //from = -catgrid.width
                    to = (catgrid.count * 60 + 6 * (catgrid.count - 1))
                }
            }
        }
        onWidthChanged: {
            runCatGridAnimation.stop()
            runCatGridPropertyAnimation.from = -gridCat.width
            runCatGridAnimation.start()
        }

        add: Transition {
            NumberAnimation { properties: "x,y"; easing.amplitude: 40;easing.type: Easing.InOutQuad; duration: 500 }
            //SpringAnimation { spring: 3; damping: 0.2 }
        }
        displaced: Transition {
            NumberAnimation { properties: "x,y"; easing.type: Easing.InOutQuad;duration: 500 }
        }

        remove: Transition {
            ParallelAnimation {
                NumberAnimation { property: "opacity"; to: 0; duration: 700 }
                NumberAnimation { properties: "x,y"; to: 100; duration: 1000 }
            }
        }
    }
    onCategoriesChanged: {
        lm.clear()
        for (var i in categories)
        {
            //console.log(">",categories[i])
            lm.append({cat: categories[i], colorCat: rightGrid.getColor(categories[i]),toolTipTextVisible : false})
        }
    }

    ListModel
    {
        id: lm
        Component.onCompleted: {



        }

    }

    Component {
        id: componentCategory

        Rectangle {
            width: height * 3
            height: 20
            color: colorCat
            radius: 5

            Label {
                id: catText
                width: parent.width
                height: parent.height

                text: cat.substr(0,8) + ((cat.length > 8) ? "..." : "")
                clip: true
                horizontalAlignment: Label.AlignHCenter
                topPadding: 2
                enabled: false

               /* ToolTip.text: cat
                ToolTip.visible: toolTipTextVisible
                ToolTip.onVisibleChanged: {
                    if (ToolTip.visible)
                        toolTipTextVisible = true
                    else
                        toolTipTextVisible = false
                }*/
                font.pointSize: 8
                color: "white"

            }

        }

    }

}
