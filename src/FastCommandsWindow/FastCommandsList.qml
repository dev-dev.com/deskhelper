import QtQuick 2.0

import "../Other" as OtherModules

ListView {

    id: fcListView
    property string table: "commandsList"
    property string textColor: style.getTextColor()
    /** @brief Поиск команд по имени команды*/
    function findByName(cn, type)
    {
        var list
        if (cn === "")
            list = db.selectAllTuples(4, table)
        else {
            if (type === 0) {
                list = db.selectWithCond({commandName : cn},4, table)
            } else if (type === 1) {
                list = db.selectWithCond({action : cn},4, table)
            }
        }


        fcListModel.clear()
        var i = 1
        if (list[0] > 1)
        {
            for (; i < list[0];) {
                fcListModel.append({name : list[i],
                                       command : list[i + 2]})
                i = i + 4
            }
        }
    }

    function getCurrentCommand() {
        return fcListModel.get(fcListView.currentIndex).command
    }

    function changeCurrentIndex(down) {
        if (currentIndex !== -1)
        {
            if (down) {
                if (currentIndex !== fc.count - 1)
                    currentIndex++
            } else {
                if (currentIndex !== 0)
                    currentIndex--
            }
        }
    }


    /*function getFirstString(type) {
        if (type === 0)
            return (fcListModel.count > 0) ? fcListModel.get(0).name : ""
        else if (type === 1)
            return (fcListModel.count > 0) ? fcListModel.get(0).command : ""

    }*/

    model: ListModel {

        id: fcListModel
        Component.onCompleted: {


            var list = db.selectAllTuples(4, table)

            var i = 1
            if (list[0] > 1)
            {
                for (; i < list[0];) {
                    append({name : list[i],
                               command : list[i + 2]})
                    i = i + 4
                }
            }
        }
    }

    delegate: Rectangle {
        width: parent.width - 5
        height: 30
        opacity: 1
        color: "transparent"

        Rectangle {
            id: fone
            anchors.fill: parent
            opacity: 0.5
            radius: 3
            z: -1
            color: model.index === fcListView.currentIndex ? style.getBorderColor() : "transparent"
        }

        Text {

            text: typeSearchBox.currentIndex === 0 ? name : command
            color: textColor
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.verticalCenter: parent.verticalCenter
        }
        OtherModules.StylishButton {
            id: executeBtn
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.verticalCenter: parent.verticalCenter
            button_name: qsTr("Execute") + settings.emptyString
            width: 50
            height: 20
            visible: fcListView.currentIndex === model.index
            background_gradient: style.getGradientStandart()
            entered_gradient: style.getGradientEntered()
            onMouseAreaClicked: {
                loading.visible = true
                loadingTimer.start()
                stt.evecuteAction(command)
            }
        }

        MouseArea {
            anchors {
                top: parent.top
                bottom: parent.bottom
                left: parent.left
                right: executeBtn.left
            }

            onClicked: fcListView.currentIndex = model.index
        }
    }

}
