import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import "../Style" as StyleModules
import "../Other" as OtherModules

Window {

    id: fcwindow
    width: Screen.width * 0.3
    height: Screen.height// * 0.5

    color: "transparent"
    flags: Qt.FramelessWindowHint

    Component.onCompleted: {
        x = Screen.width - width
        y = 0//Screen.height/2 - height/2
    }


    function showCommandsList() {

        if (commandsList.anchors.leftMargin === 0) {

            visible = true
            fcwindow.requestActivate()
        } else if (!visible) {
            visible = true
            fcwindow.requestActivate()
            return
        } else if (visible) {
            if (!fcwindow.active) {
                fcwindow.flags += Qt.WindowStaysOnTopHint
                fcwindow.flags -= Qt.WindowStaysOnTopHint
                fcwindow.requestActivate()
                return
            }
        }

        commandsList.anchors.leftMargin = (commandsList.anchors.leftMargin === -Screen.width * 0.3) ? 0 : -Screen.width * 0.3
        anchorsLeftMarginTimer.start()
    }


    function updFastCommands() {
        fc.findByName(search.text,typeSearchBox.currentIndex)
    }


    Shortcut {
        sequences: ["Enter", "Return"]
        onActivated: {
            if (fc.currentIndex !== -1)
            {
                loading.visible = true
                loadingTimer.start()
                stt.evecuteAction(fc.getCurrentCommand())
            }
        }
    }

    Shortcut {
        sequence: "Down"
        onActivated: {
            fc.changeCurrentIndex(true)
        }
    }

    Shortcut {
        sequence: "Up"
        onActivated: {
            fc.changeCurrentIndex(false)
        }
    }

    Shortcut {
        sequences: ["Right","Left"]
        onActivated: {
            typeSearchBox.currentIndex = (typeSearchBox.currentIndex === 1) ? 0 : 1
        }
    }


    Item {
        id: commandsList
        width: Screen.width * 0.3
        height: Screen.height// * 0.5


        Rectangle {
            anchors.fill: parent
            gradient: style.getBackgroundGradient()
            z: -1
            radius: 2
            opacity: 0.7
            border.width: 1
            border.color: "black"

        }

        anchors.left: parent.right
        anchors {
            top: parent.top
            bottom: parent.bottom
        }

        Behavior on anchors.leftMargin {
            NumberAnimation { duration: 800; easing.type: Easing.InOutQuad;}
        }

        Timer {
            id: anchorsLeftMarginTimer
            interval: 800
            running: false
            repeat: false
            onTriggered: {
                if (commandsList.anchors.leftMargin === 0) {
                    fcwindow.visible = false
                }
            }
        }


        Column {
            anchors.fill: parent

            anchors.margins: 10
            //anchors.topMargin: Screen.height * 0.15
            spacing: 3

            Image {
                //id: logo
                width: parent.width / 1.5
                height: parent.width * 0.35
                anchors.horizontalCenter: parent.horizontalCenter
                //anchors.centerIn: parent
                //anchors.verticalCenterOffset: -50
                fillMode: Image.PreserveAspectFit
                source: "/resources/img/biglogo1.svg"
            }

            Row {

                id: typeSearch

                width: parent.width - 3

                spacing: 5
                height: 40

                // строка поиска посередине
                TextField {
                    id: search
                    z:1
                    //  anchors.top: parent.bottom
                    //anchors.right: parent.right
                    //  anchors.topMargin: 15
                    //anchors.rightMargin: 0
                    // anchors.horizontalCenter: parent.horizontalCenter
                    //anchors.left: parent.left
                    width: parent.width - 160
                    placeholderText: (typeSearchBox.currentIndex === 0) ?
                                         qsTr("Search by command name") + settings.emptyString :
                                         qsTr("Search by command") + settings.emptyString
                    placeholderTextColor: style.getPlaceholderTextColor()
                    color: style.getTextColor()
                    selectByMouse: true
                    background: Rectangle {
                        implicitWidth: parent.width
                        implicitHeight: parent.height
                        color: style.getBackgroundGradientStart()
                        border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                        border.width: 3
                        radius: 5
                    }

                    onTextChanged: {
                        fc.findByName(text,typeSearchBox.currentIndex)
                        /*if (search.text.length > 0)
                            additionalText.text = fc.getFirstString(typeSearchBox.currentIndex)
                        else
                            additionalText.text = ""*/
                    }

                    /*TextField {
                        id: additionalText
                        anchors.fill: parent
                        text: ""
                        color: "gray"
                        opacity: 0.5
                        readOnly: true
                        enabled: false
                        z: 0
                        background: Rectangle {
                            color: "transparent"
                        }

                    }*/
                }

                ComboBox {
                    id: typeSearchBox

                    //font.pixelSize: 10
                    leftPadding: 10

                    model: ["Name", "Command"]

                    delegate: ItemDelegate {
                        width: typeSearchBox.width

                        Rectangle {
                            anchors.fill: parent
                            color: style.getSdIndicatorColorChecked()
                            opacity: 0.9
                        }

                        contentItem: Text {
                            text: modelData
                            color: "white"
                            font: typeSearchBox.font
                            elide: Text.ElideRight
                            verticalAlignment: Text.AlignVCenter
                        }

                        highlighted: typeSearchBox.highlightedIndex === index
                    }

                    /*indicator: Canvas {
                        id: canvas
                        x: language_box.width - width - language_box.rightPadding
                        y: language_box.topPadding + (language_box.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"

                        Connections {
                            target: language_box
                            onPressedChanged: canvas.requestPaint()
                        }

                        onPaint: {
                            context.reset();
                            context.moveTo(0, 0);
                            context.lineTo(width, 0);
                            context.lineTo(width / 2, height);
                            context.closePath();
                            context.fillStyle = language_box.pressed ? style.getSdBackgroundBorderColor() : style.getSdIndicatorColorChecked();
                            context.fill();
                        }
                    }*/

                    contentItem: Text {
                        leftPadding: 0
                        rightPadding: typeSearchBox.indicator.width + typeSearchBox.spacing

                        text: typeSearchBox.displayText
                        font: typeSearchBox.font
                        color: style.getTextColor()
                        //color: typeSearchBox.pressed ? style.getSdTextColorDown() : style.getSdTextColor()
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                    }

                    background: Rectangle {
                        implicitWidth: 120
                        implicitHeight: 30
                        color: style.getBackgroundGradientStart()
                        //color: "transparent"
                        border.color: typeSearchBox.activeFocus ? style.getSdIndicatorBorderColor() : style.getSdBackgroundBorderColor()
                        border.width: typeSearchBox.pressed ? 3 : 2
                        radius: 5
                    }

                    popup: Popup {
                        y: typeSearchBox.height - 1
                        width: typeSearchBox.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1

                        contentItem: ListView {

                            clip: true

                            //implicitHeight: contentHeight
                            implicitHeight: 80
                            model: typeSearchBox.popup.visible ? typeSearchBox.delegateModel : null
                            currentIndex: typeSearchBox.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }

                        background: Rectangle {
                            color: style.getSdIndicatorColorChecked()
                            opacity: 0.7
                            border.color: style.getSdIndicatorBorderColor()
                            radius: 2
                        }
                    }

                    //currentIndex: (settings.getActiveLanguage() === "en") ? 0 : 1
                }
            }


            FastCommandsList {

                id: fc
                width: parent.width
                height: Screen.height * 0.4
                clip: true
                //anchors.fill: parent
                /*anchors {
                    bottom: parent.bottom
                    left: parent.left
                    right: parent.right
                }*/
            }

            OtherModules.StylishButton {
                id: closeBtn
                anchors.right: parent.right
                anchors.rightMargin: 10
                //anchors.verticalCenter: parent.verticalCenter
                button_name: qsTr("Close") + settings.emptyString
                width: 50
                height: 20
                background_gradient: style.getAttentionGradientStandart()
                entered_gradient: style.getAttentionGradientEntered()
                onMouseAreaClicked: {
                    showCommandsList()
                }
            }

            AnimatedImage {
                id: loading
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width/3
                height: 100
                clip: true
                visible: false
                source: "/resources/img/wait.gif"
            }

            Timer {
                id: loadingTimer
                interval: 2700
                repeat: false
                running: false
                onTriggered: {
                    loading.visible = false
                }
            }


        }


    }

    /*Connections {
        target: stt
        // когда сочетание клавиш активировано и пришел об этом сигнал
        // то ищем команды, которые выполняются при таком нажатии и выполняем их
        onWorkFinished: {
            loading.visible = false
        }
    }*/

    StyleModules.Style {
        id: style
    }



}
