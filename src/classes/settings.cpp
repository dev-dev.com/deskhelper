/*!
 * \file
 * \brief Хранение, загрузка и сохранение настроек, а также перевод
 *
 *  Выбор языка, темы, режим звука, сворачивания в трей
 */

#include "../deskhelper/include/settings.h"



/*!
 * \brief Конструктор Settings
 * \details Выполняется создания переводчика
 */
Settings::Settings()
{
    translator1 = new QTranslator(this);
    //translator2 = new QTranslator(this);
}

/*!
 * \brief Получение пустой строки
 * \return пустая строка
 */
QString Settings::getEmptyString()
{
    return "";
}

/*!
 * \brief Выбор языка
 * \param language Выбираемый язык
 * \return void
 */
Q_INVOKABLE void Settings::selectLanguage(QString language) {
 if(language == QString("ru")) {
  translator1->load("QtLanguage_ru", ".");
  qApp->installTranslator(translator1);
 }

 if(language == QString("en")) {
  qApp->removeTranslator(translator1);
 }

 emit languageChanged();
}

/* Работа с настройками*/

/*!
 * \brief Загрузка настроек
 * \return успешно / не успешно
 */
bool Settings::loadSettings() {
    QFile f("settings.conf");

    if (!f.open(QIODevice::ReadOnly))
    {
        qDebug() << QCoreApplication::tr("Failed to load settings: configuration file is not readable; default settings selected.");
        return false;
    }
    else
    {
        QTextStream qts(&f);
        QString data = qts.readAll();

        auto mData = data.split(QRegExp("[\r\n]"),Qt::SkipEmptyParts);
        if (mData.length() < 8)
        {
            //errorText = QCoreApplication::tr("Не удалось загрузить настройки: файл конфигурации не доступен для записи");
            qDebug() << QCoreApplication::tr("Failed to load custom settings: default settings selected.");
            f.close();
            if (mData.length() > 0) {
                startWindowViewed = false;
            }
            return false;
        }
        else
        {
            silentMode = (mData[0] == "1") ? true : false;
            trayMode = (mData[1] == "1") ? true : false;
            appActivation = mData[2];
            activeLanguage = mData[3];
            darkMode = (mData[4] == "1") ? true : false;
            startWindowViewed = (mData[5] == "1") ? true : false;
            promptMode = (mData[6] == "1") ? true : false;
            fastCommandsActivation = mData[7];
            selectLanguage(activeLanguage);
            f.close();
            return true;
        }

    }
}


bool Settings::generateSettings(){

    QFile f("settings.conf");

    if (!f.open(QIODevice::WriteOnly))
    {
        errorText = QCoreApplication::tr("Failed to save settings: configuration file is not writable");
        return false;
    }
    else
    {
        f.resize(0);
        QTextStream qts(&f);
        qts << silentMode << '\n' << trayMode << '\n' << appActivation
            << '\n' << activeLanguage << '\n' << darkMode
            << '\n' << startWindowViewed << '\n' << promptMode << '\n' << fastCommandsActivation;
        f.close();
        return true;
    }
}


/*!
 * \brief Сохранение настроек
 * \return успешно / не успешно
 */
bool Settings::saveSettings(bool bsilentMode,
                            bool btrayMode,
                            bool bdarkMode,
                            QString shortcut,
                            QString language,
                            bool bpromptMode,
                            QString fastCommandsShortcut) {
    QFile f("settings.conf");

    if (!f.open(QIODevice::WriteOnly))
    {
        errorText = QCoreApplication::tr("Failed to save settings: configuration file is not writable");
        return false;
    }
    else
    {
        f.resize(0);
        QTextStream qts(&f);
        qts << bsilentMode << '\n' << btrayMode << '\n' << shortcut << '\n' <<
               language << '\n' << bdarkMode << '\n' << startWindowViewed << '\n' << bpromptMode << '\n' << fastCommandsShortcut;
        f.close();
        return true;
    }
}

void Settings::setStartWindowViewed(bool viewing) {
    startWindowViewed = viewing;
}


/*!
 * \brief сеттер для silentMode
 * \param mode со звуком / юез звука
 * \return void
 */
void Settings::setSilentMode(bool mode) {
    silentMode = mode;
}

/*!
 * \brief сеттер для trayMode
 * \param mode с треем / без трея
 * \return void
 */
void Settings::setTrayMode(bool mode) {
    trayMode = mode;
}

/*!
 * \brief сеттер для darkMode
 * \param mode темная тема / светлая тема
 * \return void
 */
void Settings::setDarkMode(bool mode) {
    darkMode = mode;
}

/*!
 * \brief сеттер для promptMode
 * \param mode подсказки вкл / выкл
 * \return void
 */
void Settings::setPromptMode(bool mode) {
    promptMode = mode;
}

/*!
 * \brief сеттер для appActivation
 * \param shortcut сочетание клавиш для активации приложения
 * \return void
 */
void Settings::setAppActivation(QString shortcut) {
    appActivation = shortcut;
}

/*!
 * \brief сеттер для fastCommandsActivation
 * \param shortcut сочетание клавиш для активации приложения
 * \return void
 */
void Settings::setFastCommandsActivation(QString shortcut) {
    fastCommandsActivation = shortcut;
}

/*!
 * \brief сеттер для activeLanguage
 * \param language языка интерфейса
 * \return void
 */
void Settings::setActiveLanguage(QString language) {
    activeLanguage = language;
}


/*!
 * \brief геттер для startWindowViewed
 * \return startWindowViewed
 */
bool Settings::getStartWindowViewed() {
    return startWindowViewed;
}

/*!
 * \brief геттер для silentMode
 * \return silentMode
 */
bool Settings::getSilentMode() {
    return silentMode;
}

/*!
 * \brief геттер для trayMode
 * \return trayMode
 */
bool Settings::getTrayMode() {
    return trayMode;
}

/*!
 * \brief геттер для darkMode
 * \return darkMode
 */
bool Settings::getDarkMode() {
    return darkMode;
}

/*!
 * \brief геттер для promptMode
 * \return promptMode
 */
bool Settings::getPromptMode() {
    return promptMode;
}

/*!
 * \brief геттер для appActivation
 * \return appActivation
 */
QString Settings::getAppActivation() {
    return appActivation;
}

/*!
 * \brief геттер для fastCommandsActivation
 * \return fastCommandsActivation
 */
QString Settings::getFastCommandsActivation() {
    return fastCommandsActivation;
}

/*!
 * \brief геттер для activeLanguage
 * \return activeLanguage
 */
QString Settings::getActiveLanguage() {
    return activeLanguage;
}

/*!
 * \brief геттер для errorText
 * \return errorText
 */
QString Settings::getErrorText() {
    return errorText;
}
