/*!
 * \file
 * \brief Работа с базой данных и обработкой данных
 *
 * Чтение из бд, удаление, изменение строк таблиц бд,
 * обработка данных для приведения в нужный вид для работы с бд
 */

#include "../deskhelper/include/database.h"



/*!
 * \brief Конструктор: Инициализация БД
 */
Database::Database()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("database.db3");
    if (!db.open())
    {
        qDebug() << db.lastError().text();
    }

}

/*!
 * \brief Деструктор: Отключение БД
 */
Database::~Database()
{
    if (db.isOpen())
        db.close();
}


/*!
 * \brief Геттер для состояния БД: подключено/отключено
 * \return isOpen
 */
bool Database::isOpen()
{
    return db.isOpen();
}

/*!
 * \brief Создание таблиц
 * \details Создание таблиц для БД
 * \return булево - успешно созданы / неудача
 */
bool Database::createDatabaseTables()
{
    QSqlQuery query;
    QString strCommandsList = "CREATE TABLE IF NOT EXISTS commandsList ("
                              "commandName    VARCHAR(255) PRIMARY KEY NOT NULL, "
                              "KeysORVoice    text, "
                              "action         text, "
                              "isVoiceInput   boolean "
                              ");";

    QString strImagesList =   "CREATE TABLE IF NOT EXISTS imagesList ("
                              "pathToImage    text, "
                              "tag            text PRIMARY KEY NOT NULL,"
                              "isSelected     boolean"
                              ");";

    QString strRemindersList = "CREATE TABLE IF NOT EXISTS remindersList ("
                               "ID             integer PRIMARY KEY AUTOINCREMENT,"
                               "nameOfEvent    text, "
                               "date           date, "
                               "description    text, "
                               "repetition     integer,"
                               "frequency      integer DEFAULT 0,"
                               "frequencyYet   integer DEFAULT 0,"
                               "period         VARCHAR(5) DEFAULT '00:00',"
                               "isViewed       boolean"
                               ");";

    QString strNotesList = "CREATE TABLE IF NOT EXISTS notesList ("
                           "ID             integer PRIMARY KEY AUTOINCREMENT,"
                           "note           text, "
                           "date           date DEFAULT CURRENT_TIME, "
                           "categories     text  "
                           ");";

    QString strCategoriesList = "CREATE TABLE IF NOT EXISTS catsList ("
            //"ID             integer PRIMARY KEY AUTOINCREMENT,"
            "category       text PRIMARY KEY NOT NULL, "
            "color          character(7)"
            ");";

    if (!query.exec(strCommandsList) ||
            !query.exec(strImagesList) ||
            !query.exec(strRemindersList) ||
            !query.exec(strNotesList) ||
            !query.exec(strCategoriesList))
    {
        qDebug() << query.lastError().text();
        return  false;
    }
    return true;
}

/*!
 * \brief Универсальное добавление элемента в любую таблицу
 * \param v Список значений добавляемой строки
 * \param table Название таблицы для добавления
 * \return Добавлена / не добавлена
 */
bool Database::addTuple(QVariantList v, QString table)
{
    QString qstr = "'";
    QString comma = ", ";
    QSqlQuery query;
    QString strInsert;

    if (table == "remindersList")
    {

        strInsert = "INSERT INTO " + table + " " + "(nameOfEvent, date, description, repetition, frequency, frequencyYet, period, isViewed, commands) " + "VALUES (";
    }
    else if (table == "notesList")
    {
        strInsert = "INSERT INTO " + table + " " + "(note,categories) " + "VALUES (";

    }
    else if (table == "catsList")
    {
        strInsert = "INSERT INTO " + table + " " + "(category,color) " + "VALUES (";

    }
    else
    {
        strInsert = "INSERT INTO " + table + " "
                                             "VALUES (";
    }
    qDebug() << strInsert;
    //qDebug() << v.count();
    for(int i = 0;i < v.count(); i++)
    {
        //qDebug() << v.value(i).type();
        if (v.value(i).type() == QVariant::String)
        {
            strInsert += qstr + v.value(i).toString() + qstr + ((i == (v.count() - 1)) ? ");" : comma);
        }
        else
        {
            strInsert += v.value(i).toString() + ((i == (v.count() - 1)) ? ");" : comma);
        }
    }

    qDebug() << strInsert;
    if (!query.exec(strInsert))
    {
        qDebug() << query.lastError().text();

        errorStr = query.lastError().text();
        return false;
    }
    //if (table == "notesList")
    //{
    //strInsert = strInsert.left(strInsert.length() -1);
    //strInsert += " SELECT last_insert_rowid();";
    id = query.lastInsertId().toString();
    //}
    /*if (table == "notesList")
    {
        while (query.next())
        {
            qDebug() << query.value(0).toString();
        }
    }*/
    return true;
}

/*!
 * \brief Универсальное изменение элементов любой таблицы
 * \param v Список значений изменяемой строки
 * \param table Название таблицы для изменения строк
 * \param condition Условие отбора строк из таблицы
 * \return Изменена / не изменена
 */
bool Database::editTuple(QJsonObject v, QString table, QJsonObject condition)
{
    QString qstr = "'";
    QString comma = ", ";
    QSqlQuery query;
    QString strUpd = "UPDATE " + table + " SET ";

    if (table == "commandsList" && v.value(condition.keys().first()).toString() != condition.value(condition.keys().first()).toString())
    {
        QString strSelect = "SELECT * FROM " + table;
        strSelect += " WHERE " + condition.keys().first() + " = '" +
                v.value(condition.keys().first()).toString() + "';";
        qDebug() << strSelect;
        if (!query.exec(strSelect))
        {
            qDebug() << query.lastError().text();

            errorStr = query.lastError().text();
            return false;
        }
        while (query.next())
        {
            errorStr = "Name " + query.value(0).toString() + " already exists";
            return false;
        }
    }
    qDebug() << strUpd;
    //qDebug() << v.keys().last() << v.value(v.keys().last()).toBool();
    qDebug() << v.keys().count();
    for(int i = 0;i < v.keys().count(); i++)
    {
        //qDebug() << v.keys().value(i) << v.value(v.keys().value(i));
        //qDebug() << v.value(i).type();
        if (v.value(v.keys().value(i)).type() == QJsonValue::String)
        {
            strUpd += v.keys().value(i) + "=" + qstr + v.value(v.keys().value(i)).toString() + qstr + ((i == (v.keys().count() - 1)) ? "" : comma);
        }
        else if (v.value(v.keys().value(i)).type() == QJsonValue::Bool)
        {
            strUpd += v.keys().value(i) + "=" + ((v.value(v.keys().value(i)).toBool()) ? "1" : "0") + ((i == (v.keys().count() - 1)) ? "" : comma);
        }
        else if (v.value(v.keys().value(i)).type() == QJsonValue::Double)
        {
            strUpd += v.keys().value(i) + "=" + QString::number(v.value(v.keys().value(i)).toInt()) + ((i == (v.keys().count() - 1)) ? "" : comma);
        }
        else
        {
            strUpd += v.keys().value(i) + "=" + v.value(v.keys().value(i)).toString() + ((i == (v.keys().count() - 1)) ? "" : comma);

        }
    }

    if (condition.value(condition.keys().first()).type() == QJsonValue::String)
    {
        strUpd += " WHERE " + condition.keys().first() + " = '" +
                condition.value(condition.keys().first()).toString() + "';";
    }
    else if (condition.value(condition.keys().first()).type() == QJsonValue::Bool)
    {
        strUpd += " WHERE " + condition.keys().first() + " = " +
                ((condition.value(condition.keys().first()).toBool()) ? "1" : "0") + ";";
    }
    else if (condition.value(condition.keys().first()).type() == QJsonValue::Double)
    {
        strUpd += " WHERE " + condition.keys().first() + " = " +
                QString::number(condition.value(condition.keys().first()).toInt()) + ";";
    }
    else
    {
        strUpd += " WHERE " + condition.keys().first() + " = " +
                condition.value(condition.keys().first()).toString() + ";";
    }

    qDebug() << strUpd;
    if (!query.exec(strUpd))
    {
        qDebug() << query.lastError().text();

        errorStr = query.lastError().text();
        return false;
    }
    return true;
}

/*!
 * \brief Универсальное удаление элемента из любой таблицы
 * \param condition отбор для удаляемых строк
 * \param table таблица удаляемых строк
 * \return Удалена / не удалена
 */
bool Database::removeTuple(QJsonObject condition, QString table)
{
    QSqlQuery query;
    QString strDel = "DELETE FROM " + table + " "
                                              "WHERE " + condition.keys().first() + " = '" +
            condition.value(condition.keys().first()).toString() + "';";
    //qDebug() << strDel;

    if (!query.exec(strDel))
    {
        qDebug() << query.lastError().text();
        errorStr = query.lastError().text();
        return false;
    }
    return true;
}

/*!
 * \brief Универсальная выборка с одним условием
 * \param condition отбор для выбираемых строк
 * \param countOfParam кол-во столбцов
 * \param table таблица удаляемых строк
 * \return кол-во элементов + список отобранных элементов таблицы
 */
QVariantList Database::selectWithCond(QJsonObject condition,int countOfParam, QString table)
{
    QSqlQuery query;
    QString strSelect = "SELECT * FROM " + table + " "
                                                   "WHERE " + condition.keys().first() + " = '" +
            condition.value(condition.keys().first()).toString() + "';";
    if (table == "commandsList" || table == "imagesList")
    {
        strSelect = "SELECT * FROM " + table + " "
                                               "WHERE " + "instr("+ condition.keys().first() + ",'" +
                condition.value(condition.keys().first()).toString() + "') > 0;";
    }
    else if (table == "catsList")
    {
        strSelect = "SELECT * FROM " + table + " "
                                               "WHERE " + "LOWER("+ condition.keys().first() + ") = LOWER('" +
                condition.value(condition.keys().first()).toString() + "');";
    }
    if (!query.exec(strSelect))
    {
        qDebug() << query.lastError().text();
        return {};
    }

    QVariantList list;
    list << 0;
    int count = 0;
    while (query.next())
    {

        for (int i = 0; i < countOfParam; i++)
        {
            list << query.value(i).toString();
        }
        count += countOfParam;
    }
    list[0] = count;

    return list;
}


/*!
 * \brief Выборка всех элементов любой таблицы
 * \param countOfParam кол-во столбцов
 * \param table имя таблицы
 * \return кол-во элементов + список всех элементов таблицы
 */
QVariantList Database::selectAllTuples(int countOfParam, QString table)
{
    QSqlQuery query;
    //QString strSelect = "SELECT ID, nameOfEvent, date, description, repetition, frequency, period, isViewed FROM " + table + ";";

    QString strSelect;

    if (table == "remindersList")
    {
        QString checkColumn = "SELECT cid FROM pragma_table_info('remindersList') ORDER BY cid DESC LIMIT 1;";
        QString comandsColumn = "ALTER TABLE remindersList ADD commands text;";

        if (!query.exec(checkColumn))
        {
             qDebug() << query.lastError().text();
        }

        query.next();
        if (query.value(0).toInt() < 9){
            if (!query.exec(comandsColumn))
            {
                 qDebug() << query.lastError().text();
            }
        }
        //strSelect = "SELECT * FROM " + table + " ORDER BY date;";
        strSelect = "SELECT ID, nameOfEvent, date, description, repetition, frequency, period, isViewed, commands FROM " + table + " ORDER BY date;";
    }
    else
        strSelect = "SELECT * FROM " + table + ";";
    if (!query.exec(strSelect))
    {
        qDebug() << query.lastError().text();
        return {};
    }

    QVariantList list;
    list << 0;
    int count = 0;
    while (query.next())
    {

        for (int i = 0; i < countOfParam; i++)
        {
            list << query.value(i).toString();
        }
        qDebug() << query.value(0).toString() << " " << query.value(1).toString() << " " << query.value(2).toString() << query.value(7).toString();
        count += countOfParam;
    }
    list[0] = count;

    /*QJsonDocument objectDoc(object);
    QString strJson(objectDoc.toJson(QJsonDocument::Compact));
    qDebug() << strJson << "!";*/

    return list;

}

/*!
 * \brief Назначение текущего изображения (персонажа)
 * \param tag тэг этого персонажа
 * \return Назначен / не назначен
 */
bool Database::makeImageMain(QString tag)
{
    QSqlQuery query;
    QString strUpdUp =   "UPDATE imagesList SET isSelected = TRUE "
                         "WHERE tag = '" + tag + "';";

    QString strUpdDown = "UPDATE imagesList SET isSelected = FALSE "
                         "WHERE isSelected = TRUE;";
    if (!query.exec(strUpdDown))
    {
        qDebug() << query.lastError().text();
        errorStr = query.lastError().text();
        return false;
    }

    if (!query.exec(strUpdUp))
    {
        qDebug() << query.lastError().text();
        errorStr = query.lastError().text();
        return false;
    }
    return true;
}

/*!
 * \brief Получение основного персонажа
 * \return путь до файла данного изображения
 */
QString Database::getImageMainPath()
{
    QSqlQuery query;
    QString strSelect = "SELECT * FROM imagesList "
                        "WHERE isSelected = TRUE;";
    if (!query.exec(strSelect))
    {
        return "";
    }
    while (query.next())
    {
        QString path = query.value(0).toString();
        return ((path != "") ? query.value(0).toString() : "/resources/img/chika.png");
    }
    return "/resources/img/chika.png";
}

/*!
 * \brief Поиск команд в таблице commandsList БД по условию
 * \param json тьюпл из ключ-значений для поиска
 * \param column имя столбца, значения которого нужны
 * \return Список со значениями по данному столбцу
 */
QStringList Database::findCommand(QJsonObject json, QString column)
{
    QSqlQuery query;
    QString strSelect = "SELECT " + column + " FROM commandsList "
                                             "WHERE ";
    for (int i = 0; i < json.keys().count();i++)
    {
        strSelect += json.keys().takeAt(i) + "=" +
                ((json.value(json.keys().takeAt(i)).type() == QJsonValue::String) ? "'": "") +
                ((json.value(json.keys().takeAt(i)).type() == QJsonValue::Bool) ?
                     (json.value(json.keys().takeAt(i)).toBool() ? "TRUE" : "FALSE")  :
                     json.value(json.keys().takeAt(i)).toString() ) +
                ((json.value(json.keys().takeAt(i)).type() == QJsonValue::String) ? "'": "") +
                ((i + 1 == json.keys().count()) ? ";": " AND ");
        //qDebug() << json.value(json.keys().takeAt(i));
    }

    if (!query.exec(strSelect))
    {
        return {};
    }
    QStringList qsl;
    while (query.next())
    {
        qsl.append(query.value(0).toString());
    }
    return qsl;
}

/*!
 * \brief Поиск команд в таблице commandsList БД и их отбор с помощью расстояния Левенштейна
 * \param string строка, введенная пользователем
 * \return Список команд, которые соответствуют строке string
 */
QStringList Database::findCommandWithLevenshteinDistance(QString string)
{
    QSqlQuery query;
    QString strSelect = "SELECT KeysORVoice, action FROM commandsList "
                        "WHERE isVoiceInput = TRUE";


    if (!query.exec(strSelect))
    {
        return {};
    }
    QVariantList qv;
    QStringList qsl;
    int distance = 0; int minDistance = 3;
    while (query.next())
    {

        distance = levenshtein(string, query.value(0).toString(),1,1,1,1);

        //qDebug() << string << " " << query.value(0).toString() << " " << distance;
        if (distance < 3)
        {
            qv << query.value(1).toString();
            qv << distance;
            //qDebug() << distance << " " << query.value(1).toString();
            if (distance < minDistance)
                minDistance = distance;
        }
    }
    if (minDistance == 3)
        return {};
    else
    {
        int i = 1;
        while (i < qv.size())
        {
            if (qv.value(i).toInt() == minDistance)
            {
                qsl << qv.value(i - 1).toString();
            }
            i += 2;
        }
    }
    return qsl;
}

/*!
 * \brief Получение из БД всех уникальных сочетаний клавиш
 * \return Список сочетаний
 */
QStringList Database::receiveShortcuts()
{
    QSqlQuery query;
    QString strSelect = "SELECT DISTINCT KeysORVoice FROM commandsList "
                        "WHERE isVoiceInput = FALSE;";
    if (!query.exec(strSelect))
    {
        return {};
    }
    QStringList qsl;
    while (query.next())
    {
        qsl.append(query.value(0).toString());
    }
    return qsl;
}

/*!
 * \brief Получение текста ошибки
 * \return Текст ошибки
 */
QString Database::getError()
{
    return errorStr;
}

/*!
 * \brief Получение кол-ва строк таблицы
 * \param table имя таблицы
 * \return Кол-во строк
 */
int Database::getCountRows(QString table)
{
    QSqlQuery query;
    QString getCount = "SELECT count(*) FROM " + table + ";";
    int count = 0;
    if (!query.exec(getCount))
    {
        qDebug() << query.lastError().text();
        errorStr = query.lastError().text();
        return 0;
    }
    else
    {
        query.next();
        count = query.value(0).toInt();
    }
    return count;
}


/*!
 * \brief Получение данных из таблицы remindersList
 * \param date дата для отбора данных
 * \param time время для отбора данных
 * \return тьюпл из данных
 */
QVariantList Database::getReminderMessage(QString date, QString time)
{
    QString datePlusOne = incrementReminderDate(1,date);
    QSqlQuery query;
    qDebug() << date;
    QString selectMessage = "SELECT ID, nameOfEvent, description, repetition, frequency, frequencyYet, period, date, commands FROM remindersList WHERE date BETWEEN '" + date + "' AND '" + datePlusOne + "' AND isViewed = false;";
    qDebug() << selectMessage;
    QVariantList list;
    query.exec(selectMessage);
    QTime t;
    QJsonObject v;
    QJsonObject condition;
    QTime fullIncrement;
    while (query.next())
    {
        v = QJsonObject();
        condition = QJsonObject();

        qDebug() << getTime(query.value(7).toString()) << query.value(6).toString();
        t = t.fromString(getTime(query.value(7).toString()),"HH:mm"); // дата из бд
        fullIncrement = fullIncrement.fromString(query.value(6).toString(),"HH:mm"); // дата инкремента

        qDebug() << "date:" << t << fullIncrement;

        // формируем текущее время напоминания
        for (int i = 1; i < query.value(5).toInt() + 1;i++)
        {
            t = t.addSecs(fullIncrement.second() + fullIncrement.minute() * 60 + fullIncrement.hour() * 3600);
            qDebug() << "date after inc:" << t;
        }
        //qDebug() << "date equals:" << t.toString("hh:mm") << time;
        //qDebug() << "frequency: " << query.value(4).toInt() << query.value(5).toInt() + 1;
        // если напоминание должно сейчас сработать
        if (t.toString("hh:mm") == time)
        {

            // если это последняя сработка напоминания на сегодня
            if (query.value(4).toInt() <= query.value(5).toInt() + 1)
            {
                condition.insert("ID", query.value(0).toString());
                if (query.value(3).toInt() == 0) {
                    v.insert("isViewed",true);
                }
                v.insert("frequencyYet",0);
                v.insert("date", incrementReminderDate(query.value(3).toInt(),date) + " " + getTime(query.value(7).toString()));
                editTuple(v,"remindersList",condition);
            }
            else
            {
                condition.insert("ID", query.value(0).toString());
                v.insert("frequencyYet",query.value(5).toInt() + 1);
                editTuple(v,"remindersList",condition);
            }
            list << query.value(0) << query.value(1) <<  query.value(2) << query.value(3) << toList(query.value(8).toString());

        }
        //qDebug() << "frequency: " << query.value(4) << query.value(5);

    }
    return list;
}

/*!
 * \brief Получение данных из таблицы remindersList
 * \param repetition режим повторения напоминания (раз в день, неделю и тд)
 * \param strDate Дата напоминания
 * \return Измененная дата
 */
QString Database::incrementReminderDate(int repetition, QString strDate)
{
    QDate date;
    date = date.fromString(strDate,"yyyy.MM.dd");
    if (repetition == 1)
    {
        date = date.addDays(1);
    }
    if (repetition == 2)
    {
        date = date.addDays(7);
    }
    if (repetition == 3)
    {
        date = date.addMonths(1);
    }
    if (repetition == 4)
    {
        date = date.addYears(1);
    }
    return date.toString("yyyy.MM.dd");
}

/*!
 * \brief Получение данных о пропущенных напоминаниях из таблицы remindersList
 * \param date текущее время
 * \return Данные неотмеченных напоминаний
 */
QVariantList Database::getNotViewedNotifications (QString date)
{
    QSqlQuery query;
    QString getVieweds = "SELECT ID, nameOfEvent, description, repetition, date, frequency, frequencyYet, period FROM remindersList WHERE date <= '" + date + "' AND isViewed = false;";
    QVariantList messages;
    QJsonObject v;
    QJsonObject condition;
    QTime t;
    QTime fullIncrement;
    QDateTime dt1 = dt1.fromString(date,"yyyy.MM.dd HH:mm");
    qDebug() << dt1;
    QDateTime dt2;
    int frequencyYet;

    if (!query.exec(getVieweds))
    {
        qDebug() << query.lastError().text();
    }
    else
    {
        while (query.next())
        {
            v = QJsonObject();
            condition = QJsonObject();

            // если напоминание не требует повторений
            if (query.value(3).toInt() == 0)
            {
                v.insert("isViewed",true);
                condition.insert("ID", query.value(0).toString());
                editTuple(v,"remindersList",condition);

            }
            // если на сегодня должно быть несколько сработок напоминания
            else if (getDate(date) == getDate(query.value(4).toString()) && query.value(5).toInt() != 1) {
                t = t.fromString(getTime(query.value(4).toString()),"HH:mm"); // дата из бд
                fullIncrement = fullIncrement.fromString(query.value(7).toString(),"HH:mm"); // дата инкремента

                qDebug() << "date:" << t << fullIncrement;

                // формируем текущее время напоминания
                for (int i = 0; i < query.value(6).toInt();i++)
                {
                    t = t.addSecs(fullIncrement.second() + fullIncrement.minute() * 60 + fullIncrement.hour() * 3600);
                    qDebug() << "date after inc:" << t;
                }
                // высчитываем, в какой по счету сработке должно сейчас находиться напоминание
                frequencyYet = query.value(6).toInt();
                do {
                    if (t > QTime::fromString(dt1.toString("hh:mm"),"hh:mm"))
                    {
                        break;
                    }
                    else {

                        t = t.addSecs(fullIncrement.second() + fullIncrement.minute() * 60 + fullIncrement.hour() * 3600);
                        frequencyYet++;
                    }
                } while (frequencyYet != query.value(5).toInt());
                // если сработка еще не последняя
                if (frequencyYet != query.value(5).toInt()) {
                    v.insert("frequencyYet",frequencyYet);
                    condition.insert("ID", query.value(0).toString());
                    editTuple(v,"remindersList",condition);
                } else {
                    dt2 = dt2.fromString(incrementReminderDate(query.value(3).toInt(),
                                                               getDate(date)) + " " +
                                         getTime(query.value(4).toString()),"yyyy.MM.dd HH:mm");
                    v.insert("date",dt2.toString("yyyy.MM.dd HH:mm"));
                    v.insert("frequencyYet",0);
                    condition.insert("ID", query.value(0).toString());
                    editTuple(v,"remindersList",condition);
                }
            }
            else {
                dt2 = dt2.fromString(query.value(4).toString(),"yyyy.MM.dd HH:mm");
                qDebug() << dt2;
                do {
                    dt2 = dt2.fromString(incrementReminderDate(query.value(3).toInt(),
                                                               dt2.toString("yyyy.MM.dd")) + " " +
                                         getTime(query.value(4).toString()),"yyyy.MM.dd HH:mm");
                    qDebug() << "<>" << dt2;
                } while (dt2 <= dt1);
                qDebug() << "equals: " << dt2 << dt1 << dt2.toString("yyyy.MM.dd");
                v.insert("date",dt2.toString("yyyy.MM.dd HH:mm"));
                v.insert("frequencyYet",0);
                condition.insert("ID", query.value(0).toString());
                editTuple(v,"remindersList",condition);
            }
            messages << query.value(1).toString() << query.value(2).toString()
                     << getDate(query.value(4).toString()) << getTime(query.value(4).toString());
        }
    }
    return messages;
}

/*!
 * \brief Преобразование строки в дату
 * \param strDate строка
 * \return дата
 */
QDate Database::stringToDate(QString strDate)
{ 
    return QDate::fromString(strDate, "yyyy.MM.dd");
}

/*!
 * \brief Получение только даты из строки
 * \param dbDate строка
 * \return дата
 */
QString Database::getDate(QString dbDate)
{
    QStringList splitSring = dbDate.split(" ");
    return splitSring[0];
}

/*!
 * \brief Получение только времени из строки
 * \param dbDate строка
 * \return время
 */
QString Database::getTime(QString dbDate)
{
    QStringList splitSring = dbDate.split(" ");
    return splitSring[1];
}

/*!
 * \brief Получение только часов из строки
 * \param dbDate строка
 * \return часы
 */
QString Database::getHours(QString dbDate)
{
    QStringList splitSring = dbDate.split(":");
    return splitSring[0];
}

/*!
 * \brief Получение только минут из строки
 * \param dbDate строка
 * \return минуты
 */
QString Database::getMinutes(QString dbDate)
{
    QStringList splitSring = dbDate.split(":");
    return splitSring[1];
}

/*!
 * \brief Получение текстового представления нажатой клавиши
 * \param sym код нажатой клавиши
 * \return Текстовое представление клавиши
 */
QString Database::getSymbol(int sym)
{

    int index = keyboardRu.indexOf(QChar(sym));
    if (index != -1)
    {
        return QString(keyboardEn.at(index)).toUpper();
    }

    //sym = 'W';
    QKeySequence seq = QKeySequence(sym);
    //qDebug() << ">>> " << QString(QChar(sym)).toUpper();
    if (seq.toString().length() > 1)
        return seq.toString();
    return seq.toString().toUpper();
}

/*!
 * \brief Получение расширения исполняемых файлов
 * \return список расширений
 */
QStringList Database::getExpansion()
{
#ifdef Q_OS_WIN
    return {"Execute files (*.exe)", "All files (*)"} ;
#else
    return {"Execute files (*.sh *.out *)", "All files (*)"} ;
#endif
}

/*!
 * \brief Получение строки пути файла без "file:///"
 * \param str путь до файла
 * \return преобразованный путь
 */
QString Database::trimString(QString str)
{
#ifdef Q_OS_WIN
    return str.mid(8);
#else
    return str.mid(7);
#endif
}

/*!
 * \brief Получение списка строк из строки
 * \param str строка
 * \param delimiter разделитель
 * \return список строк
 */
QStringList Database::toList(QString str, QString delimiter)
{

    qDebug() << str;
    return str.split(delimiter,Qt::SkipEmptyParts);
}

/*!
 * \brief Проверка пересечения list1 с list2
 * \param list1 первое множество
 * \param list2 второе множество
 * \return Пересекаются / не пересекаются
 */
bool Database::haveIntersection(QStringList list1, QStringList list2)
{
    qDebug() << ">>>>" << list1 << list2;
    //qDebug() << list1.length() << list2.length();
    if (list1.count() == 0)
        return true;
    bool complete = true;
    for (int i = 0; i < list1.length(); i++)
    {
        complete = false;
        for (int j = 0; j < list2.length(); j++)
        {
            if (list1.value(i) == list2.value(j))
            {
                complete = true;
                break;
            }
        }
        if (!complete)
            return false;
    }
    return true;

}

/*!
 * \brief Преобразоние строки в число
 * \param str строка
 * \return число
 */
int Database::toInt(QString str)
{
    return str.toInt();
}

/*!
 * \brief Получение id
 * \return id
 */
QString Database::getId()
{
    return id;
}

/*!
 * \brief Перевод строки в нижний регистр
 * \param str строка
 * \return строка в нижнем регистре
 */
QString Database::toLower(QString str)
{
    return str.toLower();
}

QString Database::changeAllQuotes(QString string, bool onSingle)
{

    if (onSingle)
    {
        qDebug() << string << "\n" << string.replace(QRegExp("[\"]"),"'");
        return string.replace(QRegExp("[\"]"),"'");
    } else {
        qDebug() << string << "\n" << string.replace(QRegExp("[']"),"\"");
        return string.replace(QRegExp("[']"),"\"");
    }

}

QString Database::changeFontFamily(QString string, QString newFontFamily)
{
    int pos = string.indexOf("font-family");
    if (pos != -1) {
        int pos2 = string.indexOf(";",pos);
        if (pos != -1) {
            qDebug() << "><><" << string.left(pos) + "font-family:\"" + newFontFamily + "\""
                        + string.right(string.length() - pos2);
            return string.left(pos) + "font-family:\"" + newFontFamily + "\""
                    + string.right(string.length() - pos2);
        }
    }
    return string;
}

QString Database::getFontFamily(QString string) {
    int pos = string.indexOf("font-family");
    if (pos != -1) {
        int pos2 = string.indexOf(";",pos);
        if (pos != -1) {
            string = string.mid(pos,pos2 - pos);
            string = string.replace("font-family:","").replace("\"","").replace("'","");
            qDebug() << string;
            return string;
        }
    }
    return "";
}

QString Database::changeBold(QString formattedText, QString text)
{
    QString styleProp = "font-weight:";
    int pos = formattedText.indexOf(styleProp);
    qDebug() << "><><" << formattedText;
    if (pos == -1 ) {
        //styleProp = "p style=\" margin-top:0px;";
        //pos = formattedText.indexOf(styleProp);
        styleProp = "<span style=\"";
        pos = formattedText.indexOf(styleProp);
        if (pos != -1) {
            return formattedText.insert(pos + styleProp.length(),"font-weight:600;");
        } else {
            return "<b>" + text + "</b>";
        }
    }
    while (pos != -1) {
        int pos2 = formattedText.indexOf(";",pos);
        if (pos != -1) {
            qDebug() << "><><" << formattedText.mid(pos + styleProp.length(),pos2-pos - styleProp.length());
            if (formattedText.mid(pos + styleProp.length(),pos2-pos - styleProp.length()) == "400") {
                formattedText = formattedText.replace(pos + styleProp.length(),pos2-pos - styleProp.length(),"600");
            } else {
                formattedText = formattedText.replace(pos + styleProp.length(),pos2-pos - styleProp.length(),"400");
            }
        }
        pos = formattedText.indexOf(styleProp,pos + 1);
    }
    qDebug() << "><><" << formattedText;
    return formattedText;
}

QString Database::changeItalic(QString formattedText, QString text)
{
    QString styleProp = "font-style:";
    int pos = formattedText.indexOf(styleProp);
    qDebug() << "><><" << formattedText;
    if (pos == -1 ) {
        //styleProp = "p style=\" margin-top:0px;";
        //pos = formattedText.indexOf(styleProp);
        styleProp = "<span style=\"";
        pos = formattedText.indexOf(styleProp);
        if (pos != -1) {
            return formattedText.insert(pos + styleProp.length(),"font-style:italic;");
        } else {
            return "<i>" + text + "</i>";
        }
    }
    while (pos != -1) {
        int pos2 = formattedText.indexOf(";",pos);
        if (pos != -1) {
            qDebug() << "><><" << formattedText.mid(pos + styleProp.length(),pos2-pos - styleProp.length());
            if (formattedText.mid(pos + styleProp.length(),pos2-pos - styleProp.length()) == "italic") {
                formattedText = formattedText.replace(pos + styleProp.length(),pos2-pos - styleProp.length(),"normal");
            } else {
                formattedText = formattedText.replace(pos + styleProp.length(),pos2-pos - styleProp.length(),"italic");
            }
        }
        pos = formattedText.indexOf(styleProp,pos + 1);
    }
    qDebug() << "><><" << formattedText;
    return formattedText;
}

QString Database::changeUnderline(QString formattedText, QString text)
{

    QString styleProp = "text-decoration: ";
    int pos = formattedText.indexOf(styleProp);
    qDebug() << "><><" << formattedText;
    if (pos == -1 ) {
        styleProp = "<span style=\"";
        pos = formattedText.indexOf(styleProp);
        if (pos != -1) {
            return formattedText.insert(pos + styleProp.length(),"text-decoration: underline;");
        } else {
            return "<u>" + text + "</u>";
        }
    }
    while (pos != -1) {
        int pos2 = formattedText.indexOf(";",pos);
        if (pos != -1) {
            qDebug() << "><><" << formattedText.mid(pos + styleProp.length(),pos2-pos - styleProp.length());
            if (formattedText.mid(pos + styleProp.length(),pos2-pos - styleProp.length()) == "underline") {
                formattedText = formattedText.replace(pos + styleProp.length(),pos2-pos - styleProp.length(),"none");
            } else {
                formattedText = formattedText.replace(pos + styleProp.length(),pos2-pos - styleProp.length(),"underline");
            }
        }
        pos = formattedText.indexOf(styleProp,pos + 1);
    }
    qDebug() << "><><" << formattedText;
    return formattedText;
}
