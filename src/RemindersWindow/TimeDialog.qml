/**
  * @file
  * @brief Диалог для выбора времени
  *
*/
import QtQuick 2.5
import QtQuick.Controls 2.14
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

/*!
    \class TimeDialog
    \brief Диалог для выбора времени
 */
Dialog {
    id: dialogTime

    width: 300
    height: 300
    /** @brief Время */
    property string establishedTime: (hours_tumbler.currentIndex < 10 ? "0" + hours_tumbler.currentIndex.toString() : hours_tumbler.currentIndex.toString()) + ":" +
                                     (minutes_tumbler.currentIndex * 5 < 10 ? "0" + (minutes_tumbler.currentIndex * 5).toString() : (minutes_tumbler.currentIndex * 5).toString())

    /** @brief Сигнал нажатого "Ок" */
    signal okClicked


    contentItem: Rectangle {
        id: dialogRect
        color: "#343842"

        Label{
            id: hours_label

            anchors.left: parent.left
            anchors.top: parent.top

            anchors.leftMargin: 45
            anchors.topMargin: 30

            text: qsTr("Hours") + settings.emptyString
            color: "white"
            font.pixelSize: 20
        }

        Label{
            id: minutes_label

            anchors.right: parent.right
            anchors.top: parent.top

            anchors.rightMargin: 30
            anchors.topMargin: 30

            text: qsTr("Minutes") + settings.emptyString
            color: "white"
            font.pixelSize: 20
        }

        Tumbler {
            id: hours_tumbler

            anchors.left: parent.left
            anchors.top: hours_label.bottom
            anchors.bottom: row.top

            anchors.topMargin: 10
            anchors.bottomMargin:  30
            anchors.leftMargin: 20

            width: parent.width / 3

            model: 24
            visibleItemCount: 3

            MouseArea{
                anchors.fill: parent
                onWheel: {
                    if (wheel.angleDelta.y > 0)
                    {
                        parent.currentIndex == 23 ? parent.currentIndex = 0 : parent.currentIndex += 1
                    }
                    else
                    {
                        parent.currentIndex == 0 ? parent.currentIndex = 23 : parent.currentIndex -= 1
                    }
                }
            }

            background: Item {
                Rectangle {
                    opacity: hours_tumbler.enabled ? 0.2 : 0.1
                    border.color: "#6136A3"
                    width: parent.width
                    height: 1
                    anchors.top: parent.top
                }

                Rectangle {
                    opacity: hours_tumbler.enabled ? 0.2 : 0.1
                    border.color: "#6136A3"
                    width: parent.width
                    height: 1
                    anchors.bottom: parent.bottom
                }
            }


            delegate: Text {
                id: hours
                text: qsTr(modelData < 10 ? "0" + "%0" : "%0").arg(modelData)
                color: "white"
                font: hours_tumbler.font
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                opacity: 1.0 - Math.abs(Tumbler.displacement) / (hours_tumbler.visibleItemCount / 2)
            }

            Rectangle {
                anchors.horizontalCenter: hours_tumbler.horizontalCenter
                y: hours_tumbler.height * 0.4
                width: 40
                height: 1
                color: "#CE1A33"
            }

            Rectangle {
                anchors.horizontalCenter: hours_tumbler.horizontalCenter
                y: hours_tumbler.height * 0.6
                width: 40
                height: 1
                color: "#CE1A33"
            }
        }

        Tumbler {
            id: minutes_tumbler

            anchors.right: parent.right
            anchors.top: hours_label.bottom
            anchors.bottom: row.top

            anchors.topMargin: 10
            anchors.bottomMargin:  30
            anchors.rightMargin: 20

            width: parent.width / 3


            model: 12
            visibleItemCount: 3

            MouseArea{
                anchors.fill: parent
                onWheel: {
                    if (wheel.angleDelta.y > 0)
                    {
                        parent.currentIndex == 11 ? parent.currentIndex = 0 : parent.currentIndex += 1
                    }
                    else
                    {
                        parent.currentIndex == 0 ? parent.currentIndex = 11 : parent.currentIndex -= 1
                    }
                }
            }

            background: Item {
                Rectangle {
                    opacity: minutes_tumbler.enabled ? 0.2 : 0.1
                    border.color: "#6136A3"
                    width: parent.width
                    height: 1
                    anchors.top: parent.top
                }

                Rectangle {
                    opacity: minutes_tumbler.enabled ? 0.2 : 0.1
                    border.color: "#6136A3"
                    width: parent.width
                    height: 1
                    anchors.bottom: parent.bottom
                }
            }

            delegate: Text {
                id: minutes
                text: qsTr(modelData * 5 < 10 ? "0" + "%0" : "%0").arg(modelData * 5)
                color: "white"
                font: minutes_tumbler.font
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                opacity: 1.0 - Math.abs(Tumbler.displacement) / (minutes_tumbler.visibleItemCount / 2)
            }

            Rectangle {
                anchors.horizontalCenter: minutes_tumbler.horizontalCenter
                y: minutes_tumbler.height * 0.4
                width: 40
                height: 1
                color: "#CE1A33"
            }

            Rectangle {
                anchors.horizontalCenter: minutes_tumbler.horizontalCenter
                y: minutes_tumbler.height * 0.6
                width: 40
                height: 1
                color: "#CE1A33"
            }


        }

        Row {
            id: row
            height: 50
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            anchors.bottomMargin: 2
            anchors.rightMargin: 2
            anchors.leftMargin: 2



            // Кнопка для закрытия диалога
            Button {
                id: dialogButtonCalCancel
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width / 2 - 1


                background: Rectangle {
                    gradient: dialogButtonCalCancel.pressed ? style.getAttentionGradientEntered() : style.getAttentionGradientStandart()
                    border.width: 1
                }


                text: qsTr("Cancel") + settings.emptyString
                font.pixelSize: 14

                // По нажатию на кнопку - просто закрываем диалог
                onClicked: dialogTime.close()
            }

            // Вертикальный разделитель между кнопками
            Rectangle {
                id: dividerVertical
                width: 2
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                color: "#343842"
            }

            // Кнопка подтверждения выбранной даты
            Button {
                id: dialogButtonCalOk
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width / 2 - 1


                background: Rectangle {
                    gradient: dialogButtonCalOk.pressed ? style.getGradientEntered() : style.getGradientStandart()
                    border.width: 1
                }

                text: qsTr("Ok") + settings.emptyString
                font.pixelSize: 14

                /* По клику по кнопке сохраняем выбранную дату во временную переменную
                 * и помещаем эту дату на кнопку в главном окне,
                 * после чего закрываем диалог
                 */
                 onClicked: dialogTime.okClicked()
            }
        }
    }
}
