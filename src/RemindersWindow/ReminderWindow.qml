/**
  * @file
  * @brief Страница напоминаний
  *
  * Можно удалять/сохранять/изменять/добавлять напоминания
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.3
import "../Other" as OtherModules
import "../Style" as StyleModules
import "../CommandsWindow" as CommandsModules

/*!
    \class ReminderWindow
    \brief Страница напоминаний
 */
Page {
    id: reminder_window

    /** @brief Название таблицы в БД */
    property string table: "remindersList"
    /** @brief Флаг сохранения */
    property bool save_clicked: false

    /** @brief Текущая дата */
    property var current_date: Qt.formatDate(new Date(), "yyyy.MM.dd")
    /** @brief Текущее время */
    property var current_time: Qt.formatTime(new Date(), "hh:mm")

    /** @brief Видимость окна ошибок */
    property var visibleErrorDialog : false
    /** @brief Лог ошибки*/
    property var log : ""
    /** @brief Текст ошибки*/
    property var errName: ""

    function clearFields() {
        time_field.clear()
        date_field.clear()
        task_plase.clear()
        details.clear()
        countOfRepeat.text = "1"
        time_field1.text = "00:00"
    }

    background: LinearGradient {
        id: lg
        //z:0
        anchors.fill: parent
        //anchors.top: parent.top
        // anchors.topMargin: -30
        start: Qt.point(0,0)
        end: Qt.point(parent.width, parent.height)

        gradient: style.getBackgroundGradient()

    }

    z: 5

    Column  {
        id: scrcol
        width: parent.width
        spacing: 30
        anchors.fill: parent
        anchors.topMargin: 20

        /*
        Dialog {
            id: accept_dialog
            width: 400
            height: 150
            contentItem: Rectangle {
                width: 400
                height: 200
                gradient: style.getBackgroundGradient()
                Text {
                    anchors{
                        top: parent.top
                        left: parent.left
                        right: parent.right
                        bottom: ok_button.top
                        topMargin: 30
                        bottomMargin: 10
                        rightMargin: 10
                        leftMargin: 10
                    }

                    text: qsTr("There is unsaved data, would you like to continue?") + settings.emptyString
                    color: style.getTextColor()

                    font.pointSize: 11
                    wrapMode: Text.WordWrap

                }

                StylishButton {
                    id: cancel_button
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    anchors.bottomMargin: 10
                    anchors.rightMargin: 20
                    button_name: qsTr("Cancel") + settings.emptyString
                    background_gradient: style.getAttentionGradientStandart()
                    entered_gradient: style.getAttentionGradientEntered()
                    width: 100
                    onMouseAreaClicked: {accept_dialog.close()}
                }
                StylishButton {
                    id: ok_button
                    anchors.bottom: parent.bottom
                    anchors.right: cancel_button.left
                    anchors.bottomMargin: 10
                    anchors.rightMargin: 20
                    button_name: "OK"
                    background_gradient: style.getGradientStandart()
                    entered_gradient: style.getGradientEntered()
                    width: 100
                    onMouseAreaClicked: {accept_dialog.close(); rem_table.enabled_value = false;}
                }
            }
        }
        */

        Rectangle{
            //anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            y: -50
            z:4

            gradient: style.getAttentionGradientStandart()
            radius: 5
            width: 200
            height: 30

            Text{
                //anchors.fill: parent
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Saved!") + settings.emptyString
                color: style.getTextColor()
            }

            SequentialAnimation on y {
                id: saveAnimation
                running: false
                loops: 1
                PropertyAnimation { to: -50 }
                PropertyAnimation { to: -20}
                PropertyAnimation { to: -20; duration: 1000}
                PropertyAnimation { to: -20}
                PropertyAnimation { to: -50}
            }
        }


        ReminderDateTable {
            id: date_view
            //anchors.top: parent.bottom
            //anchors.horizontalCenter: parent.horizontalCenter
            height: 300
            width: parent.width
            color: "transparent"
            //anchors.left: parent.left
            //anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: -120
            textColor: style.getTextColor()
            //anchors.leftMargin: 50
            //anchors.rightMargin: 50
            //width: rem_table.width
            z:2
            objectName: "date_table"

            onDateTimeChanged: {
                rem_table.reload_table()
            }


        }


        ReminderTable {
            id: rem_table

            objectName: "reminders_table"
            z:3
            anchors.top: date_view.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: -100
            anchors.leftMargin: 50
            anchors.rightMargin: 50
            textColor: style.getTextColor()
            //width: parent.width / 9 * 8
            height: reminder_window.height * 5 / 7
            //height: 150
            //gradientStandart: style.getGradientStandart()
            //gradientEntered: style.getGradientEntered()


        }

        // кнопки слева сверху
        Column {
            id: buttonsReminderRow
            objectName: "buttonsReminderRow"
            anchors.right: rem_table.left
            anchors.top: rem_table.top
            anchors.rightMargin: 5

            width: 30
            spacing: 10
            Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }

            OtherModules.StylishButton {
                id: addButton

                property bool enableValue: true

                objectName: "addButton"

                width: parent.width
                //height: width
                image_src: "/resources/img/add.svg"//button_name: qsTr("Add") + settings.emptyString
                background_gradient: style.getGradientStandart()
                entered_gradient: style.getGradientEntered()
                onMouseAreaClicked: {
                    date_field.text = date_view.get_reminder_date()
                    add_button.isAdd = true
                    addReminder.open()

                }
            }


            OtherModules.StylishButton {
                id: delete_button

                objectName: "deleting_reminder"

                width: parent.width
                //height: width
                image_src: "/resources/img/remove.svg"//button_name: qsTr("Remove") + settings.emptyString
                background_gradient: style.getGradientStandart()
                entered_gradient: style.getGradientEntered()
                onMouseAreaClicked: {
                    if (db.removeTuple({ ID : rem_table.getID()}, table))
                    {
                        rem_table.removeReminder()
                        //rem_table.deleteAnimation()
                        //deleteTimer.running = true
                        //rem_table.enabled_value = false
                    }
                    else
                    {
                        visibleErrorDialog = true
                        log = qsTr("Error deleting database row") + settings.emptyString
                        errName = qsTr("Error deleting database row: ") + settings.emptyString + db.getError();
                        errorLoader.sourceComponent = errorComponent
                    }
                }
            }

        }


        //Получение выбранных команд из списка
        function getCommands(){
            comandsList.currentIndex = 0;
            var commands = ""
            for (var i = 0; i < comandsList.count; i++) {
                if (comandsList.currentItem.getGradient() === style.getAttentionGradientStandart()){
                    commands += comandsList.currentItem.commandName + ","
                }

                comandsList.currentIndex += 1;
            }
            return commands
        }

        Popup {

            id: addReminder
            x: parent.width / 2 - width / 2
            y: parent.height / 2 - height / 2
            width: (reminder_window.width * 0.35 <= Screen.width * 0.28) ? Screen.width * 0.28 : reminder_window.width * 0.35
            height: (reminder_window.height * 0.35 <= Screen.height * 0.28) ? Screen.height * 0.28 : reminder_window.height * 0.35 //rem_table.height / 1.7//

            signal comandsRectangleStateChange

            modal: true
            focus: true
            background:  Rectangle {
                gradient: style.getBackgroundGradient()
                radius: 10
                z: 0
            }

            onClosed: {
                clearFields()
            }

            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

            onComandsRectangleStateChange: {
                if (comandsRectangle.state === "hide"){
                    hideAnimation.start()
                }
                else{
                    showAnimation.start()
                }
            }

            onVisibleChanged: {
                if (visible == false){
                    comandsRectangle.state = "hide"
                    comandsList.resetGradients()
                }
            }

            PropertyAnimation {
                id: hideAnimation
                target: addReminder
                property: "x"
                to: scrcol.width / 2 - addReminder.width / 2
                duration: 500
            }

            PropertyAnimation {
                id: showAnimation
                target: addReminder
                property: "x"
                to: scrcol.width / 2 - addReminder.width / 2 - comandsRectangle.width / 2
                duration: 500
            }


            contentItem: Item {
                id: addCategoryItem
                anchors.fill: parent

                Rectangle {
                    anchors.fill: parent
                    gradient: style.getBackgroundGradient()
                    radius: 10
                }

                //Меню выбора команд
                Rectangle {
                    id: comandsRectangle
                    width: addReminder.width / 2
                    height: addReminder.height
                    //gradient: style.getDrawerGradient()
                    color: "transparent"
                    z: -1

                    onStateChanged: {
                        addReminder.comandsRectangleStateChange();
                    }

                    Rectangle {
                        anchors.fill: parent
                        color: "black"
                        opacity: 0.5
                        radius: 10
                    }

                    states: [ State {
                            name: "hide"
                            AnchorChanges {
                                target: comandsRectangle
                                anchors.right: addCategoryItem.right
                                anchors.left: undefined
                            }
                        }, State {
                            name: "show"
                            AnchorChanges {
                                target: comandsRectangle
                                anchors.right: undefined
                                anchors.left: addCategoryItem.right
                            }
                        }
                    ]

                    state: "hide"

                    transitions: Transition {
                        AnchorAnimation {duration: 500;easing.type: Easing.InOutQuad;}
                    }

                    TextField {
                        id: search
                        anchors.top: parent.top
                        anchors.topMargin: 10
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: comandsList.width//parent.width * 2 / 3
                        placeholderText: qsTr("Search by command name") + settings.emptyString
                        placeholderTextColor: style.getPlaceholderTextColor()
                        color: style.getTextColor()
                        background: Rectangle {
                            implicitWidth: parent.width
                            implicitHeight: parent.height
                            color: "transparent"
                            border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                            border.width: 3
                            radius: 5
                        }
                        z: 0

                        onTextChanged: {
                            parent.findByName(text)
                        }
                    }

                    //Поиск команд в бд
                    function findByName(cn)
                    {
                        var list
                        if (cn === ""){
                            list = db.selectAllTuples(4, "commandsList")
                        }
                        else
                            list = db.selectWithCond({commandName : cn},4, "commandsList")

                        commandList.clear()

                        var i = 1
                        if (list[0] > 1)
                        {
                            for (; i < list[0];) {
                                commandList.append({name : list[i]})
                                i = i + 4
                            }
                        }

                        comandsList.setGradients(comandsList.selectCommands);

                    }



                    ListView {
                        id: comandsList
                        z: 0
                        anchors{
                            top: search.bottom
                            left: parent.left
                            right: parent.right
                            //bottom: parent.bottom
                            margins: 10
                        }


                        // Устанавливаем градиенты на выбранные элементы
                        function setGradients(commands){
                            comandsList.currentIndex = 0
                            for (var i = 0; i < commandList.count; i++){
                                comandsList.currentItem.commandGradient = style.getGradientStandart()
                                for (var j = 0; j < commands.length; j++){
                                    if (comandsList.currentItem.commandName === commands[j]){
                                        comandsList.currentItem.commandGradient = style.getAttentionGradientStandart()
                                    }
                                }
                                comandsList.currentIndex += 1
                            }
                        }

                        //Установка стандартного градиента на все элементы (вызывается при создании нового напоминания)
                        function resetGradients(){
                            comandsList.currentIndex = 0
                            for (var i = 0; i < commandList.count; i++){
                                comandsList.currentItem.commandGradient = style.getGradientStandart()
                                comandsList.currentIndex += 1
                            }
                        }

                        height: addReminder.height / 6 * 4 + 15
                        clip: true
                        spacing: 5
                        model: ListModel {

                            id: commandList

                            Component.onCompleted: {

                                console.log(comandsList.commList + "!-!-!-!")

                                var list = db.selectAllTuples(4, "commandsList")

                                var i = 1
                                if (list[0] > 1)
                                {
                                    for (; i < list[0];) {
                                        append({name : list[i],
                                                vis: true})
                                                   //shortCuts : list[i + 1],
                                                   //command : list[i + 2],
                                                   //isVoiceInput: (list[i + 3] === "0") ? false : true})
                                        i = i + 4
                                    }
                                }
                            }
                        }

                        property var selectCommands: []

                        delegate: Row {

                            id: commandDelegate

                            function getGradient(){
                                return commandRect.gradient
                            }

                            property string commandName: commandText.text
                            property var commandGradient: style.getGradientStandart()

                            //visible: vis

                            Rectangle{
                                id: commandRect
                                width: comandsList.width
                                height: addReminder.height / 9.5
                                gradient: parent.commandGradient

                                radius: 5

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        comandsList.currentIndex = model.index;
                                        if (commandRect.gradient === style.getGradientStandart()){
                                            commandDelegate.commandGradient = style.getAttentionGradientStandart();
                                            var command = commandList.get(comandsList.currentIndex).name;
                                            var flag = true;
                                            for (var i of comandsList.selectCommands) {
                                                if (command === i){
                                                    flag = false;
                                                }
                                            }
                                            if (flag)
                                                comandsList.selectCommands.push(command);
                                            console.log(comandsList.selectCommands);
                                        }
                                        else{
                                            commandDelegate.commandGradient = style.getGradientStandart();
                                            var index = comandsList.selectCommands.indexOf(commandList.get(comandsList.currentIndex).name);
                                            console.log(index);
                                            comandsList.selectCommands.splice(index, 1);
                                            console.log(comandsList.selectCommands);
                                        }
                                    }
                                }

                                Text {
                                    id: commandText
                                    anchors.fill: parent
                                    text: name
                                    color: "#ffffff"
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                    font.pixelSize: parent.height / 2
                                    elide: Text.ElideRight
                                }
                            }
                        }

                        ScrollBar.vertical: ScrollBar {
                                id: vbar
                                hoverEnabled: true
                                active: hovered || pressed
                                orientation: Qt.Vertical
                                //size: comandsList.height // content.height
                                anchors.top: parent.top
                                anchors.right: parent.right
                                anchors.bottom: parent.bottom
                            }
                    }
                }


                Column {
                    anchors.fill: parent
                    anchors.topMargin: addReminder.width * 0.025
                    anchors.leftMargin: addReminder.width * 0.05
                    anchors.rightMargin: addReminder.width * 0.05
                    Rectangle {

                        //gradient: style.getGradientStandart()
                        //onColorChanged: {
                        //    gradient = style.getGradientStandart()
                        //}

                        anchors.fill: addCategoryItem
                        id: task

                        /** @brief Текущее время */
                        property var tempDate: new Date();

                        states:[
                            State {
                                name: "BUTTON_ENTERED"
                                PropertyChanges { target: task; gradient: style.getGradientEntered();} //implicitWidth: 300; implicitHeight: 300}
                            },
                            State {
                                name: "BUTTON_EXITED"
                                PropertyChanges { target: task; gradient: style.getGradientStandart()}
                            }
                        ]

                        TimeDialog {
                            id: time
                            onOkClicked: {
                                time_field.text = establishedTime
                                time.close()
                            }
                        }

                        Rectangle {
                            id: timeItem
                            anchors.left: parent.left
                            width: parent.width
                            height: addReminder.height * 0.13

                            Label {
                                id: time_label

                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter

                                padding: 5

                                text: qsTr("Time/Date") + settings.emptyString
                                color: style.getTextColor()
                                font.pixelSize: 15
                                height: parent.height//addReminder.height * 0.088
                            }

                            TextField {
                                id: time_field

                                objectName: "datePlase"

                                anchors.left: time_label.right
                                anchors.leftMargin: 5

                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter

                                width: addReminder.width * 0.157
                                height: parent.height
                                color: style.getTextColor()

                                //enabled: dele.enable
                                selectByMouse: true

                                background: Rectangle {
                                    implicitWidth: parent.width
                                    implicitHeight: parent.height
                                    color: "transparent"
                                    border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                    border.width: 3
                                    radius: 5
                                }

                                //inputMethodHints: Qt.ImhDate
                                inputMask: "99:99"
                                validator: RegExpValidator { regExp: /^([0-1\s]?[0-9\s]|2[0-3\s]):([0-5\s][0-9\s])$ / }

                            }

                            OtherModules.StylishButton {
                                id: time_button
                                anchors.left: time_field.right
                                anchors.leftMargin: 5
                                width: time_field.height
                                height: width

                                //enabled: dele.enable

                                background_gradient: style.getGradientStandart()
                                entered_gradient: style.getGradientEntered()
                                Image {
                                    anchors.fill: parent
                                    anchors.margins: 4
                                    source: "/resources/img/clock.svg"
                                }
                                onMouseAreaClicked: time.open()
                            }

                            TextField {
                                id: date_field

                                anchors.left: time_button.right
                                anchors.leftMargin: addReminder.width /36*25 - time_button.width -
                                                    time_button.anchors.leftMargin - time_field.width -
                                                    calendar_button.width - calendar_button.anchors.leftMargin - date_field.width
                                objectName: "datePlase"

                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter

                                width: addReminder.width * 0.3
                                height: parent.height
                                color: style.getTextColor()
                                selectByMouse: true

                                background: Rectangle {
                                    implicitWidth: parent.width
                                    implicitHeight: parent.height
                                    color: "transparent"
                                    border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                    border.width: 3
                                    radius: 5
                                }

                                inputMethodHints: Qt.ImhDate
                                inputMask: "9999.99.99"
                                //text: datePlase == "" ? Qt.formatDate(task.tempDate, "yyyy.MM.dd") : datePlase
                                validator: RegExpValidator { regExp: /^([2-9\s][0-9\s][0-9\s][0-9\s]).([0\s]?[0-9\s]|1[0-2\s]).([0-2\s]?[0-9\s]|3[0-1\s])$ / }


                                onActiveFocusChanged: {
                                    task.tempDate = db.stringToDate(date_field.text)
                                    console.debug(task.tempDate)
                                }

                            }

                            OtherModules.StylishButton {
                                id: calendar_button

                                anchors.left: date_field.right
                                anchors.leftMargin: 5
                                width: date_field.height
                                height: width

                                //visible: false

                                //enabled: dele.enable

                                background_gradient: style.getGradientStandart()
                                entered_gradient: style.getGradientEntered()
                                Image {
                                    anchors.fill: parent
                                    anchors.margins: 4
                                    source: "/resources/img/calendrier.svg"
                                }
                                onMouseAreaClicked: calendar.show(task.tempDate)
                            }
                        }

                        CalendarDialog {
                            id:calendar

                            onOkClicked: {
                                task.tempDate = selectDate
                                date_field.text = Qt.formatDate(task.tempDate, "yyyy.MM.dd");
                                calendar.close();
                            }
                        }

                        //Rectangle {
                        //    id: delimiter
                        //    anchors.top: timeItem.bottom
                        //    width: parent.width
                        //    anchors.topMargin: 10
                        //
                        //    height: 2
                        //    color: style.getTextColor()
                        //    radius: 5
                        //}

                        Row {
                            id: rowTitle
                            anchors.top: timeItem.bottom
                            anchors.topMargin: 10
                            width: parent.width
                            height: addReminder.height * 0.13

                            Label {
                                id: task_label

                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter

                                padding: 5
                                width: details_label.width
                                text: qsTr("Title") + settings.emptyString
                                color: style.getTextColor()
                                font.pixelSize: 15
                                height: parent.height
                            }

                            TextField {
                                id: task_plase

                                objectName: "taskText"

                                width: addReminder.width /36*25
                                height: parent.height
                                color: style.getTextColor()

                                //enabled: dele.enable

                                selectByMouse: true
                                placeholderText: qsTr("Enter a few words") + settings.emptyString
                                placeholderTextColor: "#cfcfcf"//style.getPlaceholderTextColor()
                                background: Rectangle {
                                    implicitWidth: parent.width
                                    implicitHeight: parent.height
                                    color: "transparent"
                                    border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                    border.width: 3
                                    radius: 5
                                }
                            }

                        }
                        Row {
                            id: rowDetails
                            anchors.top: rowTitle.bottom
                            anchors.topMargin: 10
                            width: parent.width
                            height: task_plase.height * 1.542


                            Label {
                                id: details_label
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter



                                padding: 5
                                text: qsTr("Description") + settings.emptyString
                                color: style.getTextColor()
                                font.pixelSize: 15
                                height: parent.height
                            }

                            Rectangle {
                                id: details_plase
                                width: task_plase.width
                                height: parent.height
                                color: "transparent"
                                border.color: details.activeFocus ? "#B175C5" : "#6C669D"
                                border.width: 3
                                radius: 5

                                Flickable {
                                    anchors.fill: parent
                                    flickableDirection: Flickable.VerticalFlick
                                    TextArea.flickable: TextArea {
                                        id: details
                                        objectName:"detailsEdit"
                                        //text: detailsText
                                        //readOnly: true
                                        //rightInset: 10
                                        background: Rectangle{
                                            color: "transparent"
                                            border.color: "gray"
                                            border.width: 1
                                            opacity: 0.5
                                        }
                                        color: style.getTextColor()
                                        selectByMouse: true
                                        placeholderText: qsTr("Enter details") + settings.emptyString
                                        placeholderTextColor: "#cfcfcf"//style.getPlaceholderTextColor()
                                        //color: "black"//style.getTextColor()
                                        //font.pixelSize: 20
                                        wrapMode: TextInput.WrapAnywhere
                                    }
                                    clip: true

                                    ScrollBar.vertical: ScrollBar {}
                                    ScrollBar.horizontal: null
                                }
                            }

                        }

                        Row {
                            id: rowRepeat
                            anchors.top: rowDetails.bottom
                            anchors.topMargin: 10
                            width: addReminder.width /36*25
                            height: 40

                            Label {
                                id: repeat_label
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter

                                padding: 5

                                //visible: false
                                width: details_label.width
                                font.pixelSize: 15
                                text: qsTr("Repeat") + settings.emptyString
                                color: style.getTextColor()
                                height: parent.height
                            }

                            ComboBox {
                                id: repeat_box
                                z: 6
                                anchors.left: repeat_label.right
                                //   anchors.bottom: parent.bottom
                                //    anchors.left: repeat_label.right
                                //    anchors.bottomMargin: 30
                                //    anchors.leftMargin: 10

                                //visible: false

                                //currentIndex: repeatIndex

                                leftPadding: 10

                                width: task_plase.width * 0.5

                                //enabled: dele.enable

                                //onHighlighted: repeat_box.width = 200
                                /*onCurrentIndexChanged: {
                                    if (currentIndex !== 0)
                                    {
                                        timeItem1.visible = true
                                        countOfRepeat.visible = true
                                    }
                                    else
                                    {
                                        timeItem1.visible = false
                                        countOfRepeat.visible = false
                                    }
                                }*/

                                model: [qsTr("Never") + settings.emptyString,
                                    qsTr("Every day") + settings.emptyString,qsTr("Every week") + settings.emptyString,
                                    qsTr("Every month") + settings.emptyString,qsTr("Every year") + settings.emptyString]

                                delegate: ItemDelegate {
                                    width: repeat_box.width

                                    Rectangle {
                                        anchors.fill: parent
                                        color: "#6534AC"
                                        opacity: 0.9
                                    }

                                    contentItem: Text {
                                        text: modelData
                                        font: repeat_box.font
                                        elide: Text.ElideRight
                                        verticalAlignment: Text.AlignVCenter
                                        color: "white"
                                    }

                                    highlighted: repeat_box.highlightedIndex === index
                                }

                                contentItem: Text {
                                    leftPadding: 0
                                    rightPadding: repeat_box.indicator.width + repeat_box.spacing

                                    text: repeat_box.displayText
                                    font: repeat_box.font
                                    color: repeat_box.pressed ? "gray" : style.getTextColor()
                                    verticalAlignment: Text.AlignVCenter
                                    elide: Text.ElideRight
                                }

                                background: Rectangle {
                                    implicitWidth: 120
                                    implicitHeight: 40
                                    color: "black"
                                    opacity: 0.3
                                    border.color: repeat_box.pressed ? "#B175C5" : "#6C669D"
                                    border.width: repeat_box.pressed ? 3 : 2
                                    radius: 5
                                }

                                popup: Popup {
                                    z:7
                                    y: repeat_box.height - 1
                                    width: repeat_box.width
                                    implicitHeight: contentItem.implicitHeight
                                    padding: 1

                                    contentItem: ListView {

                                        clip: true

                                        //implicitHeight: contentHeight
                                        implicitHeight: 120
                                        model: repeat_box.popup.visible ? repeat_box.delegateModel : null
                                        currentIndex: repeat_box.highlightedIndex
                                        ScrollIndicator.vertical: ScrollIndicator { }
                                    }

                                    background: Rectangle {
                                        color: "#6534AC"
                                        opacity: 0.7
                                        border.color: "#B175C5"
                                        radius: 2
                                    }
                                }

                            }

                            TextField {
                                id: countOfRepeat

                                objectName: "countOfRepeat"

                                anchors.left: repeat_box.right
                                anchors.leftMargin: 5
                                //anchors.left: time_label.right
                                //anchors.rightMargin: 3
                                //anchors.topMargin: 10
                                //text: timePlase
                                width: task_plase.width * 1/4 - time_button1.width - 5 //addReminder.width * 0.1
                                height: addReminder.height * 0.13//repeat_box.height//repeat_box.height/5*4
                                anchors.verticalCenter: repeat_box.verticalCenter
                                visible: (repeat_box.currentIndex === 0) ? false : true
                                color: style.getTextColor()

                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter

                                text: "1"
                                //enabled: dele.enable
                                selectByMouse: true

                                background: Rectangle {
                                    implicitWidth: parent.width
                                    implicitHeight: parent.height
                                    color: "transparent"
                                    border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                    border.width: 3
                                    radius: 5
                                }

                                //inputMethodHints: Qt.ImhDate
                                inputMask: "9"
                                validator: RegExpValidator { regExp: /^([1-9\s])$ / }

                            }


                            TextField {
                                id: time_field1

                                objectName: "datePlase"

                                visible: (repeat_box.currentIndex === 0) ? false : true

                                anchors.left: countOfRepeat.right
                                anchors.verticalCenter: repeat_box.verticalCenter
                                anchors.leftMargin: 5
                                width: task_plase.width / 4 - 10 //addReminder.width * 0.2 //rowRepeat.width / 2
                                height: countOfRepeat.height//repeat_box.height
                                color: style.getTextColor()

                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter

                                //enabled: dele.enable
                                selectByMouse: true

                                background: Rectangle {
                                    implicitWidth: parent.width
                                    implicitHeight: parent.height
                                    color: "transparent"
                                    border.color: parent.activeFocus ? "#B175C5" : "#6C669D"
                                    border.width: 3
                                    radius: 5
                                }
                                text: "00:00"
                                //inputMethodHints: Qt.ImhDate
                                inputMask: "99:99"
                                validator: RegExpValidator { regExp: /^([0-1\s]?[0-9\s]|2[0-3\s]):([0-5\s][0-9\s])$ / }

                            }

                            OtherModules.StylishButton {
                                id: time_button1
                                anchors.left: time_field1.right
                                anchors.leftMargin: 5
                                anchors.verticalCenter: repeat_box.verticalCenter
                                visible: (repeat_box.currentIndex === 0) ? false : true
                                width: time_field1.height
                                height: width

                                //enabled: dele.enable

                                background_gradient: style.getGradientStandart()
                                entered_gradient: style.getGradientEntered()
                                Image {
                                    anchors.fill: parent
                                    anchors.margins: 4
                                    source: "/resources/img/clock.svg"
                                }
                                onMouseAreaClicked: time1.open()
                            }
                            TimeDialog {
                                id: time1
                                onOkClicked: {
                                    time_field1.text = establishedTime
                                    time1.close()
                                }
                            }
                        }

                        /*
                        StylishButton {
                            id: edit_button
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            anchors.bottomMargin: 5
                            anchors.rightMargin: 5
                            width: 20
                            height: 20

                            background_gradient: style.getGradientStandart()
                            entered_gradient: style.getGradientEntered()
                            Image {
                                anchors.fill: parent
                                anchors.margins: 2
                                source: "/resources/img/edit.png"
                            }
                            onMouseAreaClicked: {
                                if (reminder_table.enabled_value === true){
                                    error.err_name = "First save your previous changes!"
                                    error.open()
                                }
                                else{
                                    if (reminder_window.height < 600){
                                        reminder_window.height += 200
                                    }
                                    // startExpandAnimationHeight()
                                    // startExpandAnimationWidth()
                                    setVisible(true)
                                    reminder_table.enabled_value = true
                                    //dele.enable = true;
                                }
                            }
                        }

                        */

                        OtherModules.ErrorDialog {
                            id: error
                        }

                        //implicitWidth: (reminder_table.width - 150) / 4
                        //implicitHeight: 150
                        //opacity: enabled ? 1 : 0.3
                        border.width: 1
                        radius: 4

                    }

                }

                Row {
                    id: rowInputBtns
                    objectName: "rowInputCenterBtns"
                    //anchors.top: rowRepeat.bottom
                    anchors.right: parent.right
                    anchors.rightMargin: 7

                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 7
                    //width: charactersTable.width / 14
                    height: 30

                    spacing: 5
                    OtherModules.StylishButton {
                        id: addComand
                        button_name: qsTr("Commands") + settings.emptyString
                        background_gradient: style.getGradientStandart()
                        entered_gradient: style.getGradientEntered()
                        width: 100

                        onMouseAreaClicked: {
                            if (comandsRectangle.state === "show")
                                comandsRectangle.state = "hide"
                            else
                                comandsRectangle.state = "show"

                        }
                    }

                    OtherModules.StylishButton {
                        id: add_button
                        objectName: "createButton"
                        property bool isAdd: true
                        //image_src: "/resources/img/add.png"

                        width: 80
                        Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                        button_name: (isAdd) ? qsTr("Create") + settings.emptyString :
                                               qsTr("Edit") + settings.emptyString
                        background_gradient: style.getGradientStandart()
                        entered_gradient: style.getGradientEntered()

                        onMouseAreaClicked: {
                            if (time_field1.text !== ':') {
                                var parts1 = time_field1.text.split(":")

                                var length1 = 2 - parts1[0].length
                                while (length1 > 0)
                                {
                                    parts1[0] = "0" + parts1[0]
                                    length1--
                                }
                                length1 = 2 - parts1[1].length
                                while (length1 > 0)
                                {
                                    parts1[1] = "0" + parts1[1]
                                    length1--
                                }
                                time_field1.text = parts1[0] + ":" + parts1[1]
                            }

                            if (date_field.text !== '..') {
                                var parts = date_field.text.split(".")

                                var length = parts[0].length
                                for (var i = 0; i < (4-length);i++)
                                {
                                    parts[0] = "0" + parts[0]
                                }
                                length = parts[1].length
                                for (i = 0; i < (2-length);i++)
                                {
                                    parts[1] = "0" + parts[1]
                                }
                                if (parts[1] === "00")
                                    parts[1] = "01"
                                length = parts[2].length
                                for (i = 0; i < (2-length);i++)
                                {
                                    parts[2] = "0" + parts[2]
                                }
                                if (parts[2] === "00")
                                    parts[2] = "01"
                                date_field.text = parts[0] + "." + parts[1] + "." + parts[2]
                            }
                            if (time_field.text !== ':') {
                                parts = time_field.text.split(":")

                                length = 2 - parts[0].length
                                while (length > 0)
                                {
                                    parts[0] = "0" + parts[0]
                                    length--
                                }
                                length = 2 - parts[1].length
                                while (length > 0)
                                {
                                    parts[1] = "0" + parts[1]
                                    length--
                                }
                                time_field.text = parts[0] + ":" + parts[1]
                            }

                            if ((date_field.text === '..') ||
                                    (time_field.text === ':') ||
                                    (task_plase.text === "") ||
                                    (details.text === "") ||
                                    (time_field1.text === ":") ||
                                    (countOfRepeat.text === ""))
                            {
                                visibleErrorDialog = true
                                errName = qsTr("Fill in all the fields!") + settings.emptyString;
                                errorLoader.sourceComponent = errorComponent
                            }
                            else if (time_field1.text === "00:00" && countOfRepeat.text !== "1") {
                                visibleErrorDialog = true
                                errName = qsTr("A notification cannot have multiple retries at one time!") + settings.emptyString;
                                errorLoader.sourceComponent = errorComponent
                            }
                            else
                            {

                                if (date_field.text < current_date)
                                {

                                    visibleErrorDialog = true
                                    errName = qsTr("Date cannot be less than the current!");
                                    errorLoader.sourceComponent = errorComponent

                                }
                                else
                                {
                                    if ((time_field.text < current_time) && (date_field.text === current_date.toString()))
                                    {
                                        visibleErrorDialog = true
                                        errName = qsTr("Time cannot be less than the current!");
                                        errorLoader.sourceComponent = errorComponent
                                    }
                                    else
                                    {

                                        if (isAdd) {

                                            /////////////////////////////////////

                                            console.log(scrcol.getCommands())

                                            if (db.addTuple([task_plase.text,
                                                             date_field.text + ' ' + time_field.text,
                                                             details.text,
                                                             repeat_box.currentIndex,
                                                             db.toInt(countOfRepeat.text),
                                                             0,
                                                             time_field1.text,
                                                             false,
                                                             scrcol.getCommands()], table))
                                            {
                                                console.log("date: ",date_field.text,date_view.get_reminder_date())
                                                if (date_field.text === date_view.get_reminder_date())
                                                    rem_table.addingReminder()

                                                clearFields()
                                                addReminder.close()
                                                saveAnimation.start()

                                            }
                                            else
                                            {
                                                visibleErrorDialog = true
                                                log = qsTr("Error adding database row") + settings.emptyString
                                                errName = qsTr("Error adding database row: ") + settings.emptyString + db.getError();
                                                errorLoader.sourceComponent = errorComponent
                                            }
                                        } else {

                                            /////////////////////////////////

                                            console.log(scrcol.getCommands())

                                            if (db.editTuple({nameOfEvent: task_plase.text,
                                                                 date : date_field.text + " " + time_field.text,
                                                                 description : details.text,
                                                                 repetition : repeat_box.currentIndex,
                                                                 frequency : db.toInt(countOfRepeat.text),
                                                                 frequencyYet : 0,
                                                                 period : time_field1.text,
                                                                 isViewed : false,
                                                                 commands : scrcol.getCommands()}, table,{ID: rem_table.getID()}))
                                            {
                                                rem_table.reload_table()//editReminder()

                                                clearFields()
                                                addReminder.close()
                                                saveAnimation.start()

                                            }
                                            else
                                            {
                                                visibleErrorDialog = true
                                                log = qsTr("Error adding database row") + settings.emptyString
                                                errName = qsTr("Error adding database row: ") + settings.emptyString + db.getError();
                                                errorLoader.sourceComponent = errorComponent
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }

                    OtherModules.StylishButton {
                        id: cancelButton
                        objectName: "cancelButton"
                        width: 80
                        Behavior on width { SpringAnimation { spring: 3; damping: 0.5 } }
                        button_name: qsTr("Cancel") + settings.emptyString
                        background_gradient: style.getGrayGradientStandart()
                        entered_gradient: style.getGrayGradientEntered()

                        onMouseAreaClicked: {
                            clearFields()
                            addReminder.close()
                        }
                    }

                }
            }
        }

        Loader {
            id: errorLoader
        }

        Component {
            id: errorComponent
            OtherModules.ErrorDialog {
                id: error
                visible: visibleErrorDialog
                log: log
                err_name: errName
            }
        }


        Repeater{
            id: repeater
            model: 4
            delegate: OtherModules.GuideRectangle{
                id: guide_ism
                z: 10
                onPromptClicked: {
                    root.theWindow.setRemWinVisible(model.index)
                }

                states: [
                    State {
                        name: "parentAddReminder"
                        ParentChange { target: guide_ism; parent: addCategoryItem; x: 0; y: addCategoryItem.height/2}
                    },
                    State {
                        name: "parentAddReminder2"
                        ParentChange { target: guide_ism; parent: addCategoryItem; x: addCategoryItem.width/5*3; y: addCategoryItem.height/3}
                    }
                ]
            }
            Component.onCompleted: {
                for (var i = 0; i < repeater.count; ++i){

                    if (i === 0){
                        repeater.itemAt(i).width = Screen.width / 6
                        repeater.itemAt(i).promptText = qsTr("To switch to a month or a year,  hold down the mouse button; to go back, click once") + settings.emptyString
                        repeater.itemAt(i).anchors.bottom = rem_table.top
                        repeater.itemAt(i).anchors.bottomMargin = 20
                        repeater.itemAt(i).anchors.horizontalCenter = parent.horizontalCenter
                    }
                    else if (i === 1){
                        repeater.itemAt(i).promptText = qsTr("Below are the buttons for adding and removing reminders") + settings.emptyString
                        repeater.itemAt(i).parent = addButton
                        repeater.itemAt(i).anchors.bottom = addButton.top
                        repeater.itemAt(i).anchors.horizontalCenter = addButton.horizontalCenter
                    }
                    else if (i === 2){
                        repeater.itemAt(i).state = "parentAddReminder"
                        repeater.itemAt(i).promptText = qsTr("This is a menu for creating reminders. Here you need to set a time date and write a reminder text") + settings.emptyString
                    }
                    else if (i === 3){
                        repeater.itemAt(i).state = "parentAddReminder2"
                        repeater.itemAt(i).promptText = qsTr("This is the number of repetitions setting. You can customize the number and repeat period of reminders") + settings.emptyString
                    }
                    changeGuideVisible()
                }
            }

            function changeGuideVisible(){
                for (var i = 0; i < repeater.count; ++i){
                    if (settings.getPromptMode()) {
                        repeater.itemAt(i).doUpdate(root.theWindow.getRemWinVisible(i))
                    } else {
                        repeater.itemAt(i).doUpdate(false)
                    }
                }
            }
        }

        Item
        {
            id: root
            property Window theWindow: Window.window
            Connections
            {
                target: root.theWindow
                onShowPromptChanged: {
                    repeater.changeGuideVisible()
                }
                onSettingsChanged: {
                    date_view.updLanguage()
                }
            }
        }

        StyleModules.Style {
            id: style
        }
    }
    /** @brief Видимость подсказок */
    property bool guide_visible: guide.visible
}
