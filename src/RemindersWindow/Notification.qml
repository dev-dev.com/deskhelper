import QtQuick 2.12
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import "../Other" as OtherModules

MouseArea {

    id: notificationWindow
    width: Screen.width / 4
    height: width / 7 //+ width/18

   // flags: Qt.FramelessWindowHint | Qt.Window
    property string textColor: style.getTextColor()

    property var prevX
    property var prevY

    /*SequentialAnimation on y {
        id: aminationMessage
        loops: SequentialAnimation.Infinite
        running: true

        PropertyAnimation { to: Screen.height - Screen.height / 6;duration: 100}
        PropertyAnimation { to: Screen.height - Screen.height / 6.3;duration: 100}
    }*/

    //MouseArea {
        //anchors.fill: parent
        hoverEnabled: true
        /*onEntered: {
            closeNotification.width = closeNotification.height
            closeNotification.state = "show"
        }
        onExited: {
            closeNotification.state = "hide"
        }*/
        propagateComposedEvents: true

       /* Rectangle {
            color: "gray"
            z: -1
            anchors.fill: parent
        }*/

   // color: "gray"    

    Rectangle {

        id: windowRectangle
        anchors {
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        height: parent.height// - notificationWindow.width/18

        color: "transparent"

        /*MouseArea {
            anchors.fill: parent
            onPressed: {
                prevX = mouseX
                prevY = mouseY
            }
            onMouseXChanged: {
                var dx = mouseX - prevX
                notificationWindow.setX(notificationWindow.x + dx)
            }
            onMouseYChanged: {
                var dy = mouseY - prevY
                notificationWindow.setY(notificationWindow.y + dy)
            }
            onClicked: {
                //console.log(leftArrow.state,rightArrow.state)
                if (leftArrow.state !== "left")
                {
                    animationLeft.running = true
                    animationRight.running = true
                }

            }
        }*/
        Component.onCompleted: {
            //animationLeft.running = true
            //animationRight.running = true
            logo.state = "flip"
            messageRectangle.state = "stop"
            logo.flipped = !logo.flipped
        }

        Rectangle {
            id: centralRectangle
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: 0
            height: parent.height
            color: "transparent"
        }

        Rectangle {
            id: messageRectangle
            //width: parent.width / 6
            height: parent.height//parent.width / 9
            //anchors.left: parent.left//leftArrow.right
            anchors.right: parent.right//rightArrow.left
            //anchors.leftMargin: 5
            //anchors.rightMargin: 5
            anchors.verticalCenter: parent.verticalCenter
            radius: 10
            //property var previousY1: y
            color: "transparent"
            clip: true
            border.color: style.getTextColor()
            border.width: 1
            onYChanged: {
                console.log(y)
            }

            state: "start"
            states: [ State {
                    name: "start"
                    PropertyChanges {
                        target: messageRectangle
                        //anchors.left: parent.right
                        //anchors.right: parent.right
                        width: logo.height + logo.anchors.leftMargin * 2
                    }

                    PropertyChanges {
                        target: notificationText
                        opacity: 0
                    }

                    PropertyChanges {
                        target: date_text
                        opacity: 0
                    }

                    PropertyChanges {
                        target: time_text
                        opacity: 0
                    }


                }, State {
                    name: "stop"
                    PropertyChanges {
                        target: messageRectangle
                        //anchors.right: parent.right
                        //anchors.left: parent.left
                        width: parent.width
                    }

                    PropertyChanges {
                        target: notificationText
                        opacity: 1
                    }

                    PropertyChanges {
                        target: date_text
                        opacity: 1
                    }

                    PropertyChanges {
                        target: time_text
                        opacity: 1
                    }
                }

            ]
            transitions: Transition {

                PropertyAnimation {
                    property: "width"
                    duration: 1300
                    //easing.type: Easing.InOutQuad;
                }
            }

            Rectangle{
                id: notification_background
                anchors.fill: parent
                color: style.getNotificationColor()
                //gradient: ""//style.getDrawerGradient()
                opacity: 0.7
                radius: 10
            }

        /*    MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                propagateComposedEvents: true
                onEntered: {
                    closeNotification.width = closeNotification.height
                    closeNotification.state = "show"
                }
                onExited: {
                    closeNotification.state = "hide"
                }
                onClicked: {
                    console.log("clicked")
                }
            }
        */
            Rectangle {
                id: logo
                z:10
                anchors.left: parent.left
                anchors.leftMargin: width / 2
                anchors.verticalCenter: parent.verticalCenter
                width: parent.height / 2
                height: parent.height / 2
                radius: 50
                border.width: 1
                property bool flipped: false

                transform: Rotation { // способ трансформации изображения
                    id: rotation // поворот
                    origin.x: logo.height / 2; origin.y: logo.height / 2
                    axis {x: 0; y: 0; z:1} // как поворачиваем
                    angle: 0 // угол
                }
                states: State { // задаем состояние
                    name: "flip" // название состояния
                    PropertyChanges { target: rotation; angle: 1080} // изменяемые параметры
                    when: logo.flipped // когда переменная указывает на то, что был совершен переворот
                }

                transitions: Transition {
                    NumberAnimation { target: rotation; property: "angle"; duration: 1300;} // задаем время анимации
                }

                color: generateColor()


                function generateColor() {
                  return '#' + Math.floor(Math.random()*16*16*16*16*16*16).toString(16)
                }

                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "DH"
                    font.family: "Cipitillo"
                    font.pixelSize: 18
                    color: "white"
                }

                MouseArea{
                    hoverEnabled: true

                    anchors.fill: parent
                    onClicked: {
                        logo.state = "flip"
                        logo.flipped = !logo.flipped
                        messageRectangle.state = "start"
                        removeElementTimer.start()
                    }

                    onEntered: {
                        maximize.start()
                    }

                    onExited: {
                        minimize.start()
                    }
                }

                ParallelAnimation {
                    id: maximize
                    running: false
                    PropertyAnimation {target: logo; property: "width";  to: messageRectangle.height / 2 + 1; duration: 200}
                    PropertyAnimation {target: logo; property: "height";  to: messageRectangle.height / 2 + 1; duration: 200}
                }

                ParallelAnimation {
                    id: minimize
                    running: false
                    PropertyAnimation {target: logo; property: "width";  to: messageRectangle.height / 2 - 1; duration: 200}
                    PropertyAnimation {target: logo; property: "height";  to: messageRectangle.height / 2 - 1; duration: 200}
                }
            }

            Flickable{
                id:flick

                anchors.left: logo.left
                anchors.right: date_text.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom

                flickableDirection: Flickable.VerticalFlick
                clip: true
                TextArea.flickable: TextArea {
                    id: notificationText
                    property bool isTitle: true
                    verticalAlignment: TextField.AlignVCenter
                    horizontalAlignment: TextField.AlignHCenter
                    text: (isTitle) ? title : desc
                    color: textColor
                    clip: true
                    readOnly: true
                    background: Rectangle { color: "transparent" }
                }

                states: [ State {
                        name: "hide"
                        AnchorChanges {
                            target: flick
                            anchors.bottom: undefined
                            anchors.top: parent.bottom
                        }
                    }, State {
                        name: "show"
                        AnchorChanges {
                            target: flick
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                        }
                    }

                ]
                transitions: Transition {
                    AnchorAnimation {duration: 250;easing.type: Easing.InOutQuad;}
                }

                state: "show"
            }

            Text {
                id: date_text
                //anchors.left: notificationText.right
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.topMargin: 5
                width: 70
                text: date_t//"23.06.2020"
                color: textColor
            }

            Text {
                id: time_text
                //anchors.left: notificationText.right
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.rightMargin: 5
                anchors.bottomMargin: 5
                width: 40
                text: time_t//"13:12"
                color: textColor
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    flick.state = (flick.state === "show") ? "hide" : "show"
                    changeNotificationTextTimer.start()
                }
            }
            Timer {
                id: changeNotificationTextTimer
                running: false
                interval: 250
                onTriggered: {
                    notificationText.isTitle = !notificationText.isTitle
                    flick.state = (flick.state === "hide") ? "show" : "hide"
                    running = false
                }
            }

}

        /*Rectangle {
            id: closeNotification
            z: -1
            anchors.right: parent.right
            anchors.rightMargin: rightArrow.width + 5
            //anchors.verticalCenter: parent.verticalCenter
            //anchors.topMargin: 3
            height: parent.height/2
            Behavior on opacity { NumberAnimation{duration: 400}}
            width: 0
            opacity: 0.6
            scale: 0.7
            state: "hide"
            gradient: style.getGrayGradientStandart()//"#e06971"//"#30313A"//gradient: style.getGradientStandart()
            states: [ State {
                    name: "hide"
                    AnchorChanges {
                        target: closeNotification
                        anchors.bottom: undefined
                        anchors.top: parent.top
                    }
                }, State {
                    name: "show"
                    AnchorChanges {
                        target: closeNotification
                        anchors.top: undefined
                        anchors.bottom: parent.top
                    }
                }

            ]
            transitions: Transition {
                AnchorAnimation {duration: 400}
            }

            Image {
                anchors.fill: parent
                anchors.margins: 2

                id: close_img
                source: "/resources/img/close.png"
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                propagateComposedEvents: true
                onEntered: {
                    closeNotification.gradient = style.getAttentionGradientStandart()
                }
                onExited:  {
                    closeNotification.gradient = style.getGrayGradientStandart()
                }
                onClicked: {
                    //closeNotification.state = (closeNotification.state === "hide") ? "show" : "hide"
                    timerCloseNotification.running = true
                    leftArrow.state = "central"
                    rightArrow.state = "central"
                    removeElementTimer.start()
                    //running = false
                   // rightGrid.removeChecked(listModel.get(index).cat)
                   // listModel.remove(index)
                }
            }
        }

        /*Timer {
            id: timerCloseNotification
            interval: 800
            running: false
            onTriggered: {
                closeNotification.width = closeNotification.height
                closeNotification.state = (closeNotification.state === "hide") ? "show" : "hide"
                running = false
            }
        }*/
        /*Timer {
            id: timerCloseNotification
            interval: 1
            running: false
            onTriggered: {
                closeNotification.opacity = 0
                running = false
                //closeNotification.state = (closeNotification.state === "hide") ? "show" : "hide"
                //running = false
            }
        }*/


        Timer {
            id: removeElementTimer
            running: false
            interval: 1500
            onTriggered: {
                notification_model.remove(model.index)
                running = false
            }
        }


        /*OtherModules.Arrow {
            id: leftArrow
            //source: "resources/img/expand.png"
            width: parent.width / 8
            arrowColor: "#3C4653"
            height: width / 2
            anchors.verticalCenter: parent.verticalCenter
            rotation: 90
            anchors.right: centralRectangle.left
            SequentialAnimation on rotation {
                id: animationLeft
                running: false
                RotationAnimation {from:90; to: 0;duration: 400; easing.type: Easing.InQuad}
            }
            SequentialAnimation on rotation {
                id: animationLeftToRight
                running: false
                RotationAnimation {from:0; to: -180;duration: 400; easing.type: Easing.InQuad}
            }
            state: "central"
            states: [ State {
                    name: "central"
                    AnchorChanges {
                        target: leftArrow
                        anchors.left: undefined
                        anchors.right: centralRectangle.left
                    }
                }, State {
                    name: "left"
                    AnchorChanges {
                        target: leftArrow
                        anchors.right: undefined
                        anchors.left: windowRectangle.left
                    }
                }

            ]
            transitions: Transition {
                AnchorAnimation {
                    duration: 800
                }
            }
            onStateChanged: {
                if (state === "left") {
                    //messageRectangle.color = "black"
                    //messageRectangle.gradient = //style.getBackgroundGradient()
                    notification_background.color = "#D7DFE3"
                    notification_background.opacity = 0.8
                }
            }
        }

        /*Timer {
            id: closeElementTimer
            running: model.index === notification_list.currentIndex
            interval: 5000
            onTriggered: {
                leftArrow.state = "central"
                rightArrow.state = "central"
                removeElementTimer.start()
                running = false
                //animationLeftToRight.start()
                //animationRightToLeft.start()
            }
        }

        Timer {
            id: removeElementTimer
            running: false
            interval: 850
            onTriggered: {
                notification_model.remove(model.index)
                running = false
            }
        }*/

        /*OtherModules.Arrow {
            id: rightArrow
            //source: "resources/img/expand.png"
            width: leftArrow.width
            height: leftArrow.height
            arrowColor: leftArrow.arrowColor
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: -2
            rotation: 270
            SequentialAnimation on rotation {
                id: animationRight
                running: false
                RotationAnimation {from:270; to: 180;duration: 400; easing.type: Easing.InQuad}

                onStopped: {
                    leftArrow.state = "left"
                    rightArrow.state = "right"
                    //timerCloseNotification.running = true
                }
            }
            SequentialAnimation on rotation {
                id: animationRightToLeft
                running: false
                RotationAnimation {from:180; to: 0;duration: 400; easing.type: Easing.InQuad}
                onStopped: {

                }
            }


            state: "central"
            anchors.left: centralRectangle.right
            states: [ State {
                    name: "central"
                    AnchorChanges {
                        target: rightArrow
                        anchors.right: undefined
                        anchors.left: centralRectangle.right
                    }
                }, State {
                    name: "right"
                    AnchorChanges {
                        target: rightArrow
                        anchors.left: undefined
                        anchors.right: windowRectangle.right
                    }
                }

            ]
            transitions: Transition {
                AnchorAnimation {duration: 800}
            }
        }*/

    }
}
