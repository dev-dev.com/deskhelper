/**
  * @file
  * @brief Календарь
  *
  * Диалог, в котором можно выбрать дату
*/

import QtQuick 2.5
import QtQuick.Controls 2.14
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import Qt.labs.calendar 1.0
/*!
    \class CalendarDialog
    \brief Диалог, в котором можно выбрать дату
 */
Dialog {
    id: dialogLineCalendar
    // Задаём размеры диалогового окна
    width: 400
    height: 60

    /** @brief Сигнал о том, что нажата кнопка "Ok"*/
    signal okClicked
    /** @brief Выбранная дата */
    property date selectDate: calendar.selectedDate
    /** @brief Текущая дата */
    property var currentDate: new Date();

    // Создаем контент диалогового окна
    //contentItem:
    property var currentMonth: 0;
    property var currentYear: 2020;
    Rectangle {
        //rotation: 90
        anchors.top: dialogLineCalendar.top
        anchors.topMargin: 10
        anchors.horizontalCenter: dialogLineCalendar.horizontalCenter
        Text {
            text: new Date(currentYear,
                           currentMonth,
                           daysModel.get(hours_tumbler.currentIndex).day).toLocaleDateString(Qt.locale("de_DE"))
        }
    }
    Tumbler {
        id: hours_tumbler

        rotation: -90
        //anchors.fill: parent
        width: parent.height
        height: parent.width
        //width: parent.width / 3

        model: ListModel {
            id: daysModel
            Component.onCompleted:{
                for (var i = 1; i < 32; i++)
                {
                    append({day: i})
                }
            }
        }

        visibleItemCount: 7

        function getMonth(month,back) {
            if (back) {
                if (month === 0)
                {
                    currentYear--
                    return 11
                }
                else
                    return month - 1
            } else {
                if (month === 11)
                {
                    currentYear++
                    return 0
                }
                else
                    return month + 1
            }
        }

        MouseArea{
            anchors.fill: parent
            onWheel: {
                if (wheel.angleDelta.y > 0)
                {
                    var lastDate = new Date(currentYear,currentMonth + 1,0).getDate() - 1
                    if ( parent.currentIndex === lastDate)
                    {
                        currentMonth = parent.getMonth(currentMonth,false)
                        parent.currentIndex = 0
                    } else {

                        parent.currentIndex += 1
                    }
                }
                else
                {

                    lastDate = new Date(currentYear,currentMonth,0).getDate() - 1
                    if (parent.currentIndex === 0)
                    {
                        currentMonth = parent.getMonth(currentMonth,true)
                        parent.currentIndex = lastDate
                    } else {

                        parent.currentIndex -= 1
                    }
                }
            }
        }

        background: Item {
            rotation: 90
            Rectangle {
                opacity: hours_tumbler.enabled ? 0.2 : 0.1
                border.color: "#6136A3"
                width: parent.width
                height: 1
                anchors.top: parent.top
            }

            Rectangle {
                opacity: hours_tumbler.enabled ? 0.2 : 0.1
                border.color: "#6136A3"
                width: parent.width
                height: 1
                anchors.bottom: parent.bottom
            }
        }


        delegate: Text {
            rotation: 90
            id: hours
            text: qsTr(day < 10 ? "0" + "%0" : "%0").arg(day)
            color: "black"
            font: hours_tumbler.font
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            opacity: 1.0 - Math.abs(Tumbler.displacement) / (hours_tumbler.visibleItemCount / 2)
        }

        Rectangle {
            anchors.horizontalCenter: hours_tumbler.horizontalCenter
            y: hours_tumbler.height * 0.4
            width: 40
            height: 1
            color: "#CE1A33"
        }

        Rectangle {
            anchors.horizontalCenter: hours_tumbler.horizontalCenter
            y: hours_tumbler.height * 0.6
            width: 40
            height: 1
            color: "#CE1A33"
        }
    }


    /** @brief Данная функция необходима для того, чтобы
     * установить дату с кнопки в календарь,
     * иначе календарь откроется с текущей датой
     */
    function show(x){
        calendar.selectedDate = x
        dialogCalendar.open()
    }
}
