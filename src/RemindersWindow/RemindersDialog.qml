/**
  * @file
  * @brief Окно для вывода уведомлений
  *
*/

import QtQuick 2.0
import QtQuick 2.14
import QtQuick.Dialogs 1.3
import QtMultimedia 5.14
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

/*!
    \class RemindersDialog
    \brief Окно для вывода уведомлений
 */
Window {
    id: reminder_dialog

    width: Screen.width / 4
    height: (Screen.width / 24) * notification_model.count

    flags: Qt.FramelessWindowHint | Qt.Window | Qt.WindowStaysOnTopHint
    color: "transparent"

    /** @brief Добавление напоминания */
    function addingNotification(tText, dText, dateText, timeText)
    {
        notification_model.append({title: tText, desc: dText, date_t: dateText, time_t: timeText})
    }

    /** @brief Координата Х до перемещение */
    property var prevX
    /** @brief Координата Y до перемещения */
    property var prevY
/*
    Rectangle{

        id: labels_area

        //border.color: "purple"
        //border.width: 1

        anchors.horizontalCenter: parent.horizontalCenter

        implicitWidth: parent.width
        implicitHeight: parent.height / 8
        radius: parent.width / 30
        gradient: backgroundGradient

        Rectangle{
            anchors.bottom: parent.bottom
            gradient: backgroundGradient
            width: parent.width
            height: parent.height / 2
            z: -1
        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                prevX = mouseX
                prevY = mouseY
            }
            onMouseXChanged: {
                var dx = mouseX - prevX
                reminder_dialog.setX(reminder_dialog.x + dx)
            }
            onMouseYChanged: {
                var dy = mouseY - prevY
                reminder_dialog.setY(reminder_dialog.y + dy)
            }
        }


        Rectangle {
            id: notification_count_place

            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: reminder_dialog.height / 53.33

            width: 0
            height: parent.height / 1.33
            gradient: attentionGradientStandart

            radius: 5

            Label{
                id: notification_count
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                anchors.topMargin: reminder_dialog.height / 53.33
                text: qsTr("You have " + notification_list.count + " notification!") + settings.emptyString
                font.pointSize: reminder_dialog.height / 26.7
                color: "white"
            }
            SequentialAnimation on width {
                id: animation1
                running: false
                loops: 1
                PropertyAnimation { to: reminder_dialog.width / 5.625; duration: 100 }
                PropertyAnimation { to: reminder_dialog.width / 2.8125; duration: 200 }
                PropertyAnimation { to: reminder_dialog.width / 1.875; duration: 300 }
                PropertyAnimation { to: reminder_dialog.width / 1.40625; duration: 400 }
                PropertyAnimation { to: reminder_dialog.width / 1.125; duration: 400 }
            }
        }

        StylishButton{
            id: ok_button
            anchors.right: parent.right
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: reminder_dialog.height / 20
            width: parent.width / 30
            height: parent.width / 30
            background_gradient: attentionGradientStandart
            entered_gradient: attentionGradientEntered
            button_name: "X"
            font_size: reminder_dialog.height / 22.857
            radius: 10
            onMouseAreaClicked: {
                var i = 0
                while (i <= notification_list.count){
                    notification_model.remove(0)
                    i += 1
                }
                reminders_dialog.visible = false;
                //reminders_dialog.flags -= Qt.WindowStaysOnTopHint;
                //mainWindow.flags += Qt.WindowStaysOnBottomHint;
                //mainWindow.flags -= Qt.WindowStaysOnBottomHint;
            }
        }

    }
*/

    ListModel{
        id: notification_model
        /*ListElement {
            title: "dsg"
            desc:"sdgs dsg sdg sdg s"
        }
        ListElement {
            title: "dsg"
            desc:"sdgs dsg sdg sdg s"
        }
        ListElement {
            title: "dsg"
            desc:"sdgs dsg sdg sdg s"
        }*/

        onCountChanged: {
            if (count === 0)
                reminder_dialog.visible = false
        }
    }


    ListView{
        id: notification_list


        //snapMode: ListView.SnapOneItem
        verticalLayoutDirection: ListView.BottomToTop
        //highlightRangeMode: ListView.StrictlyEnforceRange

        clip: true

        height: parent.height - 10
        width: parent.width

        spacing: 5//width / 10

        model: notification_model
        delegate: Notification {

        }

        remove: Transition {
                NumberAnimation { property: "opacity"; to: 0; duration: 700 }
        }
        displaced: Transition {
            NumberAnimation {property: "y";easing.type: Easing.InOutQuad;duration: 500 }
        }
    }

}
