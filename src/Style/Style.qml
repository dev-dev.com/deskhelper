/**
  * @file
  * @brief Стили проекта
  *
*/
import QtQuick 2.0

/*!
    \class Style
    \brief Стили проекта
 */
Item {

    /* Функции получения градиентов*/

    function getGradientStandart() {
        return gradientStandart
    }
    function getGradientEntered() {
        return gradientEntered
    }
    function getAttentionGradientStandart() {
        return attentionGradientStandart
    }
    function getAttentionGradientEntered() {
        return attentionGradientEntered
    }
    function getStartingGradientStandart() {
        return startingGradientStandart
    }
    function getStartingGradientEntered() {
        return startingGradientEntered
    }
    function getGrayGradientStandart() {
        return grayGradientStandart
    }
    function getGrayGradientEntered() {
        return grayGradientEntered
    }
    function getBackgroundGradient() {
        return backgroundGradient
    }

    function getDrawerGradient() {
        return drawerGradient
    }

    function getTextColor() {
        return ident.txtColor
    }

    function getTextButtonColor() {
        return ident.textButtonColor
    }
    function getPlaceholderTextColor() {
        return ident.placeholderTextColor
    }

    function getNotificationColor() {
        return ident.notificationColor
    }

    function getBorderColor() {
        return ident.borderColor
    }

    function getSdTextColorDown() {
        return ident.sdTextColorDown
    }
    function getSdTextColor() {
        return ident.sdTextColor
    }
    function getSdIndicatorColor() {
        return ident.sdIndicatorColor
    }
    function getSdIndicatorColorChecked() {
        return ident.sdIndicatorColorChecked
    }
    function getSdIndicatorBorderColor() {
        return ident.sdIndicatorBorderColor
    }
    function getSdIndicatorBorderColorChecked() {
        return ident.sdIndicatorBorderColorChecked
    }
    function getSdIndicatorLadenColorDown() {
        return ident.sdIndicatorLadenColorDown
    }
    function getSdIndicatorLadenColor() {
        return ident.sdIndicatorLadenColor
    }
    function getSdIndicatorLadenBorderColor() {
        return ident.sdIndicatorLadenBorderColor
    }
    function getSdBackgroundBorderColor() {
        return ident.sdBackgroundBorderColor
    }
    function getSdBackgroundBorderColorPressed() {
        return ident.sdBackgroundBorderColorPressed
    }
    function getSdBackgroundColor() {
        return ident.sdBackgroundColor
    }

    Gradient {
        id: gradientStandart
        GradientStop {
            position: 0
            color: ident.gradientStandartStart
        }
        GradientStop {
            position: 1
            color: ident.gradientStandartStop
        }
    }

    Gradient {
        id: gradientEntered
        GradientStop {
            position: 0
            color: ident.gradientEnteredStart
        }
        GradientStop {
            position: 1
            color: ident.gradientEnteredStop
        }
    }

    Gradient {
        id: attentionGradientStandart
        GradientStop {
            position: 0
            color: ident.attentionGradientStandartStart
        }
        GradientStop {
            position: 1
            color: ident.attentionGradientStandartStop
        }
    }


    Gradient {
        id: attentionGradientEntered
        GradientStop {
            position: 0
            color: ident.attentionGradientEnteredStart
        }
        GradientStop {
            position: 1
            color: ident.attentionGradientEnteredStop
        }
    }

    Gradient {
        id: startingGradientStandart
        GradientStop {
            position: 0
            color: ident.startingGradientStandartStart
        }
        GradientStop {
            position: 1
            color: ident.startingGradientStandartStop
        }
    }

    Gradient {
        id: grayGradientStandart
        GradientStop {
            position: 0
            color: "#868f96"
        }
        GradientStop {
            position: 1
            color: "#596164"
        }
    }

    Gradient {
        id: grayGradientEntered
        GradientStop {
            position: 0
            color: "#596164"
        }
        GradientStop {
            position: 1
            color: "#868f96"
        }
    }

    Gradient {
        id: startingGradientEntered
        GradientStop {
            position: 0
            color: ident.startingGradientEnteredStart
        }
        GradientStop {
            position: 1
            color: ident.startingGradientEnteredStop
        }
    }

    Gradient{
        id: backgroundGradient
        GradientStop {
            position: 0
            color: ident.backgroundGradientStart
            Behavior on color {
                ColorAnimation {duration: 700; easing.type: Easing.Linear}
            }
        }
        GradientStop {
            position: 1
            color: ident.backgroundGradientStop
            Behavior on color {
                ColorAnimation {duration: 700; easing.type: Easing.Linear}
            }
        }
    }
    Gradient{
        id: drawerGradient
        GradientStop {
            position: 0
            color: ident.drawerGradientStart
            Behavior on color {
                ColorAnimation {duration: 700; easing.type: Easing.Linear}
            }
        }
        GradientStop {
            position: 1
            color: ident.drawerGradientStop
            Behavior on color {
                ColorAnimation {duration: 700; easing.type: Easing.Linear}
            }
        }
    }

    function getBackgroundGradientStart() {
        return ident.backgroundGradientStart
    }
    function getBackgroundGradientStop() {
        return ident.backgroundGradientStop
    }

     /* Идентификатор текущей темы */
    property var ident: (settings.getDarkMode()) ? dark_theme : light_theme
    Theme {
        id:light_theme
    }
    Theme {
        id: dark_theme
        gradientStandartStart: "#6C669D"
        gradientStandartStop:  "#6534AC"
        gradientEnteredStart:  "#777EC4"
        gradientEnteredStop: "#7652AC"
        attentionGradientStandartStart: "#F5203E"
        attentionGradientStandartStop: "#B2172C"
        attentionGradientEnteredStart: "#F5566B"
        attentionGradientEnteredStop: "#B2515E"
        startingGradientStandartStart: "#10da1e"
        startingGradientStandartStop: "#12942f"
        startingGradientEnteredStart: "#13b548"
        startingGradientEnteredStop: "#126d13"
        backgroundGradientStart: "#3C4653"
        backgroundGradientStop: "#30313A"
        txtColor: "#ffffff"
        placeholderTextColor: "#C7C7C7"

        notificationColor: "#1F2023"

        drawerGradientStart: "#404042"
        drawerGradientStop: "#090a0d"
        /* SwitchDelegate */

        sdTextColorDown: "gray"
        sdTextColor: "white"
        sdIndicatorColor: "transparent"
        sdIndicatorColorChecked: "#6534AC"
        sdIndicatorBorderColor: "#B175C5"
        sdIndicatorBorderColorChecked: "#6534AC"
        sdIndicatorLadenColorDown: "#B175C5"
        sdIndicatorLadenColor: "#ffffff"
        sdIndicatorLadenBorderColor: "#999999"
        sdBackgroundBorderColor: "#6C669D"
        sdBackgroundBorderColorPressed: "#B175C5"
        sdBackgroundColor: "black"

    }

    Connections {
        target: settings
        onThemeChanged: {
            if (settings.getDarkMode() && ident === light_theme
                    || !settings.getDarkMode() && ident === dark_theme)
            ident = (ident === dark_theme) ? light_theme : dark_theme
        }
    }
}

