/**
  * @file
  * @brief Параметры цвета в проекте
  *
*/
import QtQuick 2.0

 // светлая тема по умолчанию
/*!
    \class Theme
    \brief Параметры цвета в проекте
 */
Item {

    /* Все цвета проекта*/

    property string gradientStandartStart: "#2b5876"
    property string gradientStandartStop:  "#4e4376"
    property string gradientEnteredStart:  "#1e3c72"
    property string gradientEnteredStop: "#2a5298"
    property string attentionGradientStandartStart: "#f56060"
    property string attentionGradientStandartStop: "#de3950"
    property string attentionGradientEnteredStart: "#ee1e4a"
    property string attentionGradientEnteredStop: "#ee1e4a"
    property string startingGradientStandartStart: "#bbbbbe"
    property string startingGradientStandartStop: "#bbbbbe"
    property string startingGradientEnteredStart: "#bbbbbe"
    property string startingGradientEnteredStop: "#bbbbbe"
    property string backgroundGradientStart: "#fdfbfb"
    property string backgroundGradientStop: "#ebedee"
    property string textButtonColor: "white"
    property string txtColor: "#3d4245"
    property string placeholderTextColor: "#495464"
    property string borderColor: "#dfe9f3"

    property string notificationColor: "#DFE4E2"

    property string drawerGradientStart: "#fdfbfb"
    property string drawerGradientStop: "#ebedee"
    /* SwitchDelegate */

    property string sdTextColorDown: "#515763"
    property string sdTextColor: "#2d3138"
    property string sdIndicatorColor: "transparent"
    property string sdIndicatorColorChecked: "#f56060"
    property string sdIndicatorBorderColor: "#B175C5"
    property string sdIndicatorBorderColorChecked: "#6534AC"
    property string sdIndicatorLadenColorDown: "#B175C5"
    property string sdIndicatorLadenColor: "#ffffff"
    property string sdIndicatorLadenBorderColor: "#999999"
    property string sdBackgroundBorderColor: "#6C669D"
    property string sdBackgroundBorderColorPressed: "#f56060"
    property string sdBackgroundColor: "#bcc1cc"
}
