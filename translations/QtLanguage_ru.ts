<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CalendarDialog</name>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Хорошо</translation>
    </message>
</context>
<context>
    <name>CharactersTable</name>
    <message>
        <source>Couldn&apos;t open image.</source>
        <translation>Не удалось открыть изображение.</translation>
    </message>
    <message>
        <source>Here is empty</source>
        <translation>Пусто</translation>
    </message>
</context>
<context>
    <name>CommandWindow</name>
    <message>
        <source>Enter command name</source>
        <translation>Введите название команды</translation>
    </message>
    <message>
        <source>Enter command</source>
        <translation>Введите команду</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation>Клавиатура</translation>
    </message>
    <message>
        <source>Voice</source>
        <translation>Звук</translation>
    </message>
    <message>
        <source>Press to enter</source>
        <translation>Нажмите для ввода</translation>
    </message>
    <message>
        <source>Voice input field</source>
        <translation>Поле голосового ввода</translation>
    </message>
    <message>
        <source>No internet connection</source>
        <translation>Нет подключения к интернету</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <source>Fill in all the fields!</source>
        <translation>Заполните все поля!</translation>
    </message>
    <message>
        <source>Error adding database row</source>
        <translation>Ошибка добавления строки базы данных</translation>
    </message>
    <message>
        <source>Error adding database row: </source>
        <translation>Ошибка добавления строки базы данных: </translation>
    </message>
    <message>
        <source>Error deleting database row</source>
        <translation>Ошибка удаления строки базы данных</translation>
    </message>
    <message>
        <source>Error deleting database row: </source>
        <translation>Ошибка удаления строки базы данных: </translation>
    </message>
    <message>
        <source>Error changing database row</source>
        <translation>Ошибка изменения строки базы данных</translation>
    </message>
    <message>
        <source>Error changing database row: </source>
        <translation>Ошибка изменения строки базы данных: </translation>
    </message>
    <message>
        <source>Search by file name</source>
        <translation>Искать по имени команды</translation>
    </message>
    <message>
        <source>Enter cmd command, or select the application to open</source>
        <translation type="vanished">Введите терминальную команду или выберите исполняемый файл</translation>
    </message>
    <message>
        <source>Enter shortcut or voice command</source>
        <translation type="vanished">Введите сочетание клавиш или голосовую команду</translation>
    </message>
    <message>
        <source>Now you can add the command</source>
        <translation type="vanished">Сейчас вы можете добавить команду</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <source>Congratulations! You added a command, now you can call it using the specified shortcut or voice command. (Ctrl+Alt+S on the main window - call voice input). Close the window</source>
        <translation type="vanished">Поздравляем! Вы добавили команду, сейчас вы можете вызвать ее, используя сочетание клавиш или голосовую команду (нажмите Ctrl+Alt+S для голосового ввода). Вы можете закрыть это окно.</translation>
    </message>
    <message>
        <source>Press the button &apos;+&apos;</source>
        <translation type="vanished">Нажмите кнопку &apos;+&apos;</translation>
    </message>
    <message>
        <source>Below are the buttons for adding and removing comands</source>
        <translation>Ниже расположены кнопки добавления и удаления команд</translation>
    </message>
    <message>
        <source>Change the appearance of the table (grid, list, table)</source>
        <translation>Измените вид ваших данных (есть сетка, список, таблица)</translation>
    </message>
    <message>
        <source>Command name input field</source>
        <translation>Поле ввода имени команды</translation>
    </message>
    <message>
        <source>Command input field cmd (also you can select the program to open)</source>
        <translation>Поле ввода самой команды (также можете нажать на кнопку справа и выбрать исполняемый файл)</translation>
    </message>
    <message>
        <source>Select voice input and shortcuts</source>
        <translation>Выберите голосовой ввод или ввод сочетанием клавиш</translation>
    </message>
    <message>
        <source>Shortcut or voice input field</source>
        <translation>Поле ввода сочетания клавиш или голоса</translation>
    </message>
    <message>
        <source>Adding a command (all fields must be filled)</source>
        <translation>Добавление команды (все поля должны быть заполнены)</translation>
    </message>
</context>
<context>
    <name>CreateCategory</name>
    <message>
        <source>Enter note</source>
        <translation type="vanished">Изменить заметку</translation>
    </message>
</context>
<context>
    <name>DialogWindow</name>
    <message>
        <source>Speak</source>
        <translation>Говорите</translation>
    </message>
    <message>
        <source>Execute..</source>
        <translation>Выполняю..</translation>
    </message>
    <message>
        <source>Don&apos;t understand command</source>
        <translation>Не удалось найти команду для запуска</translation>
    </message>
</context>
<context>
    <name>ErrorDialog</name>
    <message>
        <source>Enter shortcuts</source>
        <translation>Введите сочетания клавиш</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>Ошибка!</translation>
    </message>
</context>
<context>
    <name>GeneralWindow</name>
    <message>
        <source>Silent mode</source>
        <translation>Режим &quot;Без звука&quot;</translation>
    </message>
    <message>
        <source>Minimize to tray</source>
        <translation>Сворачивать в трей</translation>
    </message>
    <message>
        <source>Application activation: </source>
        <translation>Активация приложения: </translation>
    </message>
    <message>
        <source>Enter shortcut</source>
        <translation>Введите сочетание клавиш</translation>
    </message>
    <message>
        <source>Language: </source>
        <translation>Язык: </translation>
    </message>
    <message>
        <source>Saved!</source>
        <translation>Сохранено!</translation>
    </message>
    <message>
        <source>Dark mode</source>
        <translation>Темная тема</translation>
    </message>
    <message>
        <source>Additionally</source>
        <translation>Дополнительно</translation>
    </message>
    <message>
        <source>Home</source>
        <translation>Домой</translation>
    </message>
    <message>
        <source>Command window</source>
        <translation>Команды</translation>
    </message>
    <message>
        <source>Reminders window</source>
        <translation>Напоминания</translation>
    </message>
    <message>
        <source>Notes window</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>Show prompts</source>
        <translation>Показать подсказки</translation>
    </message>
    <message>
        <source>There is a menu on the left, you can open it and choose what you need. I know it.</source>
        <translation>Слева Вы можете обратиться к меню и выбрать один из подходящих пунктов</translation>
    </message>
    <message>
        <source>This is the navigation menu. Here you can select the required module, as well as change application settings</source>
        <translation>Это навигационное меню. Здесь Вы можете выбрать нужный модуль, а также изменить настройки программы</translation>
    </message>
    <message>
        <source>Click here to open the settings</source>
        <translation>Кликните здесь для открытия настроек</translation>
    </message>
    <message>
        <source>This is the settings here. You can configure the program as you please</source>
        <translation>Это настройки. Вы можете настроить программу под себя.</translation>
    </message>
    <message>
        <source>On the left is the navigation menu (Shift+Tab) and character library (Ctrl+Tab)</source>
        <translation>Вы можете открыть навигационное меню нажатием Shift+Tab (или кликнуть по логотипу в верхнем левом углу) или открыть библиотеку персонажей (Ctrl+Tab или нажатием по человечку на верхней панели)</translation>
    </message>
</context>
<context>
    <name>GridCommandTable</name>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Error deleting database row</source>
        <translation>Ошибка удаления строки базы данных</translation>
    </message>
    <message>
        <source>Error deleting database row: </source>
        <translation>Ошибка удаления строки базы данных: </translation>
    </message>
    <message>
        <source>Command name</source>
        <translation>Название команды</translation>
    </message>
    <message>
        <source>Shortcuts / text</source>
        <translation>Сочетания клавиш / текст</translation>
    </message>
    <message>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Новый</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation>Имя: </translation>
    </message>
    <message>
        <source>Voice: </source>
        <translation>Голос: </translation>
    </message>
    <message>
        <source>Shortcut: </source>
        <translation></translation>
    </message>
    <message>
        <source>Command: </source>
        <translation>Команда: </translation>
    </message>
    <message>
        <source>Here is empty</source>
        <translation>Пусто</translation>
    </message>
</context>
<context>
    <name>ImageSelectionMenu</name>
    <message>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <source>Enter tag and path!</source>
        <translation>Введите тег и путь!</translation>
    </message>
    <message>
        <source>Error adding database row</source>
        <translation>Ошибка добавления строки базы данных</translation>
    </message>
    <message>
        <source>Error adding database row: </source>
        <translation>Ошибка добавления строки базы данных: </translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Error deleting database row</source>
        <translation>Ошибка удаления строки базы данных</translation>
    </message>
    <message>
        <source>Error deleting database row: </source>
        <translation>Ошибка удаления строки базы данных: </translation>
    </message>
    <message>
        <source>Make main</source>
        <translation>Назначить</translation>
    </message>
    <message>
        <source>Error changing current character in database</source>
        <translation>Ошибка изменения текущего персонажа в базе данных</translation>
    </message>
    <message>
        <source>Error changing current character in database: </source>
        <translation>Ошибка изменения текущего персонажа в базе данных: </translation>
    </message>
    <message>
        <source>Enter tag</source>
        <translation>Введите тег</translation>
    </message>
    <message>
        <source>Enter file path</source>
        <translation>Введите путь до файла</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Search by file name</source>
        <translation>Искать по имени файла</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Новый</translation>
    </message>
    <message>
        <source>Congratulations! You put the image on the main window. You can close this window.</source>
        <translation>Поздравляем! Вы чего-то достигли!</translation>
    </message>
    <message>
        <source>Click to add image.</source>
        <translation>Нажмите для добавления изображения></translation>
    </message>
    <message>
        <source>Delete image from library</source>
        <translation>Удалите изображение из библиотеки нажатием кнопки ниже</translation>
    </message>
    <message>
        <source>Setting the image on the main window</source>
        <translation>Назначьте персонажа на главное окно нажатием кнопки ниже</translation>
    </message>
    <message>
        <source>Enter the name of the image (the tag should not be repeated with the existing one) (required field)</source>
        <translation>Введите имя персонажа (тэг не должен повторяться в библиотеке)</translation>
    </message>
    <message>
        <source>File path input field (File may have extensions .png, .jpg, .svg, .gif) (required field)</source>
        <translation>Поле ввода пути файла (файл может иметь расширения .png, .jpg, .svg, .gif)</translation>
    </message>
    <message>
        <source>Click to select picture</source>
        <translation>Нажмите для выбора изображения</translation>
    </message>
    <message>
        <source>Click to add an image to the library</source>
        <translation>Нажмите для добавления изображения в библиотеку</translation>
    </message>
    <message>
        <source>Click to cancel upload</source>
        <translation>Нажмите для отмены добавления</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <source>Desk Helper</source>
        <translation>Десктопный помощник</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Create a reminder</source>
        <translation>Создать напоминание</translation>
    </message>
    <message>
        <source>Maximize window</source>
        <translation>Развернуть окно</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>No internet connection</source>
        <translation>Нет подключения к интернету</translation>
    </message>
    <message>
        <source>Want to go through the program guide?</source>
        <translation>Желаете пройти обучение?</translation>
    </message>
    <message>
        <source>Add a commands</source>
        <translation>Добавить команды</translation>
    </message>
    <message>
        <source>Add a node</source>
        <translation>Создать заметку</translation>
    </message>
    <message>
        <source>Press the right button on the character and choose menu item. If you created a voice command, then to enter it, press Ctrl+Alt+S</source>
        <translation>Нажмите правую кнопку мыши на персонаже для открытия меню. После создания голосовых команд Вы сможете вызывать персонажа комбинацией клавиш Ctrl+Alt+S</translation>
    </message>
</context>
<context>
    <name>MainContextMenu</name>
    <message>
        <source>Choose character</source>
        <translation type="vanished">Выбрать персонажа</translation>
    </message>
    <message>
        <source>Add a commands</source>
        <translation>Добавить команды</translation>
    </message>
    <message>
        <source>Create a reminder</source>
        <translation>Создать напоминание</translation>
    </message>
    <message>
        <source>Minimize to tray</source>
        <translation>Сворачивать в трей</translation>
    </message>
    <message>
        <source>Add a node</source>
        <translation>Добавить заметку</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Выйти</translation>
    </message>
</context>
<context>
    <name>Nodes</name>
    <message>
        <source>Here is empty</source>
        <translation type="vanished">Пусто</translation>
    </message>
</context>
<context>
    <name>Notes</name>
    <message>
        <source>Here is empty</source>
        <translation>Пусто</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Новый</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Error deleting database row</source>
        <translation>Ошибка удаления строки базы данных</translation>
    </message>
    <message>
        <source>Error deleting database row: </source>
        <translation>Ошибка удаления строки базы данных: </translation>
    </message>
    <message>
        <source>current</source>
        <translation>текущий</translation>
    </message>
</context>
<context>
    <name>NotesWindow</name>
    <message>
        <source>Color: </source>
        <translation>Палитра: </translation>
    </message>
    <message>
        <source>Error deleting database row</source>
        <translation>Ошибка удаления строки базы данных</translation>
    </message>
    <message>
        <source>Error deleting database row: </source>
        <translation>Ошибка удаления строки базы данных: </translation>
    </message>
    <message>
        <source>Error adding database row</source>
        <translation>Ошибка добавления строки базы данных</translation>
    </message>
    <message>
        <source>Error adding database row: </source>
        <translation>Ошибка добавления строки базы данных: </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Enter category (or categories througth &apos;;&apos;)</source>
        <translation>Введите категорию (или категории через &apos;;&apos;)</translation>
    </message>
    <message>
        <source>+ Create</source>
        <translation>+ Новая категория</translation>
    </message>
    <message>
        <source>Enter category name</source>
        <translation type="vanished">Введите имя категории</translation>
    </message>
    <message>
        <source>&gt; More</source>
        <translation>&gt; Показать категории</translation>
    </message>
    <message>
        <source>Enter name</source>
        <translation>Введите имя</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="vanished">Выберите цвет</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <source>Enter category!</source>
        <translation>Введите категорию!</translation>
    </message>
    <message>
        <source>This category already exists!</source>
        <translation>Такая категория уже существует!</translation>
    </message>
    <message>
        <source>Write...</source>
        <translation type="vanished">Заметить..</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <source>Enter description!</source>
        <translation>Введите описание!</translation>
    </message>
    <message>
        <source>Error editing database row</source>
        <translation>Ошибка изменения строки базы данных</translation>
    </message>
    <message>
        <source>Error editing database row: </source>
        <translation>Ошибка изменения строки базы данных: </translation>
    </message>
    <message>
        <source>Category have dependent elements. Delete?</source>
        <translation>Категория имеет зависимости. Все равно удалить?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <source>Category: </source>
        <translation>Категории:</translation>
    </message>
    <message>
        <source>http://example.com</source>
        <translation>http://example.com</translation>
    </message>
    <message>
        <source>example</source>
        <translation>example</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <source>To create a note press &apos;+&apos;, below the delete button</source>
        <translation>Для добавления заметки нажмите &apos;+&apos; или нажмите кнопку ниже для удаления</translation>
    </message>
    <message>
        <source>To create a category, click &apos;create&apos;</source>
        <translation>Для создания категории нажмите &apos;создать&apos;</translation>
    </message>
    <message>
        <source>Browse the list of categories, click &apos;more&apos;</source>
        <translation>Для просмотра списка категорий нажмите &apos;показать категории&apos;</translation>
    </message>
    <message>
        <source>All created categories will appear here. To remove a category, hold down the left mouse button.</source>
        <translation>Все созданные категории появляются здесь. Для удаления категории кажмите и удерживайте левую кнопку мыши на категории</translation>
    </message>
    <message>
        <source>This is a category creation menu. Enter a name and choose a color</source>
        <translation>Это меню создания категорий. Введите имя и выберите цвет категории</translation>
    </message>
    <message>
        <source>Here you can select the created category, below you can write a note</source>
        <translation>Здесь Вы можете выбрать созданную категорию, ниже Вы можете ввести и отформатировать данные заметки</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Выберите файл</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>Failed to open the database. Shutdown.</source>
        <translation>Не удалось открыть базу данных. Завершение работы.</translation>
    </message>
    <message>
        <source>Failed to create database. Shutdown.</source>
        <translation>Не удалось создать базу данных. Завершение работы.</translation>
    </message>
    <message>
        <source>Failed to load settings: configuration file is not readable; default settings selected.</source>
        <translation>Не удалось загрузить настройки: файл конфигурации не доступен для чтения; выбраны настройки по умолчанию.</translation>
    </message>
    <message>
        <source>Failed to load custom settings: default settings selected.</source>
        <translation>Не удалось загрузить кастомные настройки: выбраны настройки по умолчанию.</translation>
    </message>
    <message>
        <source>Failed to save settings: configuration file is not writable</source>
        <translation>Не удалось сохранить настройки: файл конфигурации не доступен для записи</translation>
    </message>
</context>
<context>
    <name>ReminderTable</name>
    <message>
        <source>Here is empty</source>
        <translation>Пусто</translation>
    </message>
    <message>
        <source>current</source>
        <translation>текущий</translation>
    </message>
</context>
<context>
    <name>ReminderWindow</name>
    <message>
        <source>Saved!</source>
        <translation>Сохранено!</translation>
    </message>
    <message>
        <source>Error adding database row</source>
        <translation>Ошибка добавления строки базы данных</translation>
    </message>
    <message>
        <source>Error adding database row: </source>
        <translation>Ошибка добавления строки базы данных: </translation>
    </message>
    <message>
        <source>Error deleting database row</source>
        <translation>Ошибка удаления строки базы данных</translation>
    </message>
    <message>
        <source>Error deleting database row: </source>
        <translation>Ошибка удаления строки базы данных: </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Time cannot be less than the current!</source>
        <translation>Время не может быть меньше текущего!</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <source>Repeat: </source>
        <translation>Повтор: </translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Никогда</translation>
    </message>
    <message>
        <source>Every day</source>
        <translation>Каждый день</translation>
    </message>
    <message>
        <source>Every week</source>
        <translation>Каждую неделю</translation>
    </message>
    <message>
        <source>Every month</source>
        <translation>Каждый месяц</translation>
    </message>
    <message>
        <source>Every year</source>
        <translation>Каждый год</translation>
    </message>
    <message>
        <source>Fill in all the fields!</source>
        <translation>Заполните все поля!</translation>
    </message>
    <message>
        <source>Time: </source>
        <translation>Время: </translation>
    </message>
    <message>
        <source>Date: </source>
        <translation>Дата: </translation>
    </message>
    <message>
        <source>Enter a few words</source>
        <translation>Введите пару слов</translation>
    </message>
    <message>
        <source>Enter details</source>
        <translation>Введите детали напоминания</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <source>A notification cannot have multiple retries at one time!</source>
        <translation>Напоминание не может иметь несколько повторов в один момент времени!</translation>
    </message>
    <message>
        <source>Date cannot be less than the current!</source>
        <translation>Дата не может быть меньше текущей!</translation>
    </message>
    <message>
        <source>To switch to a month or a year,  hold down the mouse button; to go back, click once</source>
        <translation>Для переключения на месяцы или года удерживаете ЛКМ; для выбора месяца или года нажмите на него</translation>
    </message>
    <message>
        <source>Below are the buttons for adding and removing reminders</source>
        <translation>Ниже находятся кнопки добавления и удаления напоминаний</translation>
    </message>
    <message>
        <source>This is a menu for creating reminders. Here you need to set a time date and write a reminder text</source>
        <translation>Это меню создания напоминаний. Здесь Вы можете выбрать дату и время напоминаний, периодичность и ввести текст напоминания</translation>
    </message>
    <message>
        <source>This is the number of repetitions setting. You can customize the number and repeat period of reminders</source>
        <translation>Здесь располагается число и интервал повторений. Вы можете настроить это на Ваш вкус.</translation>
    </message>
</context>
<context>
    <name>RightNavCategory</name>
    <message>
        <source>Error deleting database row</source>
        <translation>Ошибка удаления строки базы данных</translation>
    </message>
    <message>
        <source>Error deleting database row: </source>
        <translation>Ошибка удаления строки базы данных: </translation>
    </message>
    <message>
        <source>Here is empty</source>
        <translation>Пусто</translation>
    </message>
</context>
<context>
    <name>StartWindow</name>
    <message>
        <source>Upload and put your character on the main window</source>
        <translation>Загружайте и помещайте вашего персонажа в главное окно</translation>
    </message>
    <message>
        <source>Set reminders for any day so you don’t forget about important events</source>
        <translation>Установите напоминания на любой день, чтобы не забыть о важных событиях</translation>
    </message>
    <message>
        <source>Create your own comands and get quick access to them using shortcuts or voice input</source>
        <translation>Создайте свои собственные команды и получите быстрый доступ к ним, используя сочетания клавиш или голосовой ввод</translation>
    </message>
    <message>
        <source>Create notes and categorize them</source>
        <translation>Создавайте заметки и категории для них</translation>
    </message>
    <message>
        <source>Accept agreement</source>
        <translation>С правилами согласен</translation>
    </message>
    <message>
        <source>Begin</source>
        <translation>Начать</translation>
    </message>
</context>
<context>
    <name>TimeDialog</name>
    <message>
        <source>Hours</source>
        <translation>Часы</translation>
    </message>
    <message>
        <source>Minutes</source>
        <translation>Минуты</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Хорошо</translation>
    </message>
</context>
</TS>
