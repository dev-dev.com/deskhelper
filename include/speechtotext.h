
#ifndef SPEECHTOTEXT_H
#define SPEECHTOTEXT_H

#include <QObject>
#include <QtNetwork/QNetworkReply>
#include <QAudioRecorder>
#include <QSettings>
#include <QPixmap>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDir>
#include <QProcess>
#include "qxtglobalshortcut.h"
#include "../deskhelper/include/settings.h"
#include "../deskhelper/include/action.h"

/*!
    \class SpeechToText
    \brief Класс для реализации конвертации звуковой информации в текстовую
 */

class SpeechToText : public QObject
{
    Q_OBJECT

public:
    SpeechToText(QString shortcutAppActivation, QString shortcutFastCommandsActivation);
    ~SpeechToText();
signals:
     void sendTexttoMainClass();
     void secondClick();
     void processGlobalShortcut();
     void changeRecording();
     void recordError();
     void postError();
     void showApp();
     void showFastCommands();
     void workFinished();
public slots:
    void setGlobalShortcuts(QStringList list);
    QString getGlobalShortcut();
    void setShortcutAppActivation(QString shortcut);
    void setShortcutFastCommandsActivation(QString shortcut);
    void connectShortcuts(int j);
    void onSend();
    void onResponse (QNetworkReply* reply );
    void toggleRecord();
    void setRunning(bool flag);
    bool getRunning();
    void displayErrorMessage();
    bool isInternetAccess();
    bool getEvenClick();
    QString getRezult();
    QString getErrorText();
    void setErrorText(QString text);
    bool evecuteAction(QString action);
    bool isWin();


private:
    /*! \brief Указатель на экземпляр класса QNetworkAccessManager для отправки POST-запроса
               и приема ответа*/
    QNetworkAccessManager* m_manager;
    /*! \brief Указатель на экземпляр класса QAudioRecorder для записи аудио */
    QAudioRecorder* m_audioRecorder = nullptr;
    /*! \brief Экземпляр класса QAudioEncoderSettings для хранения данных об аудио-формате */
    QAudioEncoderSettings settings;
    /*! \brief Аудиоформат */
    QString container;
    /*! \brief Путь до аудиофайла */
    QString pathAudioFile;
    /*! \brief Результат приема ответа от сервера */
    QString rezultText;
    /*! \brief Текст ошибки */
    QString errorText;

    /* Работа с глобальными ShortCuts*/

    /*! \brief Полученное сочетание клавиш */
    QString rezultShortcut;
    /*! \brief Динамический массив указателей на глобальные ShortCut*/
    QxtGlobalShortcut** qxtshortcuts = nullptr;
    /*! \brief Указатель на глобальный ShortCut для активации приложения*/
    QxtGlobalShortcut* pshortcutAppActivation;
    /*! \brief Указатель на глобальный ShortCut для активации окна быстрого доступа к командам*/
    QxtGlobalShortcut* pshortcutFastCommandsActivation;
    /*! \brief Кол-во сочетаний клавиш */
    int shortcutsCount = 0;
    /*! \brief Счетчик */
    int i;
    /*! \brief Флаг, указывающий на то, происходит ли запись голоса */
    bool isRunning;
    /*! \brief Флаг, указывающий, была ли нажата кнопка записи голоса */
    bool evenClick;

    QList<QThread*> threads;
    QList<Action*> actions;

};

#endif // SPEECHTOTEXT_H
