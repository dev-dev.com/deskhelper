#ifndef __PERF_LEVENSHTEIN_H
#define __PERF_LEVENSHTEIN_H


#include <QString>

int levenshtein(QString string1, QString string2,
    int swap_penalty, int substition_penalty,
    int insertion_penalty, int deletion_penalty);

#endif /* __PERF_LEVENSHTEIN_H */
